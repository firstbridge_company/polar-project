/*
 * Copyright © 2013-2016 The Nxt Core Developers.
 * Copyright © 2016-2018 Jelurida IP B.V.
 *
 * See the LICENSE.txt file at the top-level directory of this distribution
 * for licensing information.
 *
 * Unless otherwise agreed in a custom licensing agreement with Jelurida B.V.,
 * no part of the Nxt software, including this file, may be copied, modified,
 * propagated, or distributed except according to the terms contained in the
 * LICENSE.txt file.
 *
 * Removal or modification of this copyright notice is prohibited.
 *
 */

package io.firstbridge.fbc;

import io.firstbridge.fbc.util.Constants;
import io.firstbridge.fbc.core.FbcCore;
import io.firstbridge.fbc.util.DirProvider;
import io.firstbridge.fbc.util.env.RuntimeEnvironment;
import io.firstbridge.fbc.util.env.RuntimeMode;
import io.firstbridge.fbc.util.env.ServerStatus;
import io.firstbridge.fbc.http.API;
import io.firstbridge.fbc.env.RunMode;

import java.io.IOException;
import java.io.PrintStream;
import java.lang.management.ManagementFactory;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.AccessControlException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class Fbc {
    final static Logger logger = LoggerFactory.getLogger(Fbc.class);

    private static final RuntimeMode runtimeMode;
    private static final DirProvider dirProvider;
         
    static {
        redirectSystemStreams("out");
        redirectSystemStreams("err");
        System.out.println("Initializing " + Constants.APPLICATION
                + " server version " + Constants.VERSION);
        printCommandLineArguments();
        runtimeMode = RunMode.getRuntimeMode();
        logger.info("Runtime mode %s\n", runtimeMode.getClass().getName());
        dirProvider = RuntimeEnvironment.getDirProvider();
        logger.info("User home folder " + dirProvider.getUserHomeDir());

    }

    private static void redirectSystemStreams(String streamName) {
        String isStandardRedirect = System.getProperty("nxt.redirect.system." + streamName);
        Path path = null;
        if (isStandardRedirect != null) {
            try {
                path = Files.createTempFile("nxt.system." + streamName + ".", ".log");
            } catch (IOException e) {
                logger.warn("Redirectrinf streams filed", e);
                return;
            }
        } else {
            String explicitFileName = System.getProperty("nxt.system." + streamName);
            if (explicitFileName != null) {
                path = Paths.get(explicitFileName);
            }
        }
        if (path != null) {
            try {
                PrintStream stream = new PrintStream(Files.newOutputStream(path));
                if (streamName.equals("out")) {
                    System.setOut(new PrintStream(stream));
                } else {
                    System.setErr(new PrintStream(stream));
                }
            } catch (IOException e) {
                logger.warn("Redirectrinf streams filed", e);
            }
        }
    }


    private static void printCommandLineArguments() {
        try {
            List<String> inputArguments = ManagementFactory.getRuntimeMXBean().getInputArguments();
            if (inputArguments != null && inputArguments.size() > 0) {
                System.out.println("Command line arguments");
            } else {
                return;
            }
            inputArguments.forEach(System.out::println);
        } catch (AccessControlException e) {
            System.out.println("Cannot read input arguments " + e.getMessage());
        }
    }


    public static void main(String[] args) {
        try {
            Runtime.getRuntime().addShutdownHook(new Thread(Fbc::shutdown));
            init();
        } catch (Throwable t) {
            logger.error("Fatal error: " + t.toString());
        }
    }

    public static void init() {
        try {
            runtimeMode.init();
            setServerStatus(ServerStatus.BEFORE_DATABASE, null);
            FbcCore.init();
            setServerStatus(ServerStatus.AFTER_DATABASE, null);

            setServerStatus(ServerStatus.STARTED, API.getWelcomePageUri());
            if (RuntimeEnvironment.isDesktopApplicationEnabled()) {
                launchDesktopApplication();
            }
            logger.info("FBC blockhain started");
            if (Constants.isTestnet) {
                logger.info("RUNNING ON TESTNET - DO NOT USE REAL ACCOUNTS!");
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            runtimeMode.alert(e.getMessage() + "\n"
                    + "See additional information in " + dirProvider.getLogFileDir() + System.getProperty("file.separator") + "nxt.log");
            System.exit(1);
        }
    }

    public static void shutdown() {
        FbcCore.shutdown();
        runtimeMode.shutdown();
    }


    private static void setServerStatus(ServerStatus status, URI wallet) {
        runtimeMode.setServerStatus(status, wallet, dirProvider.getLogFileDir());
    }



    private static void launchDesktopApplication() {
        runtimeMode.launchDesktopApplication();
    }

    private Fbc() {} // never

}
