
package io.firstbridge.fbc.env;

import io.firstbridge.fbc.util.env.CommandLineMode;
import io.firstbridge.fbc.util.env.RuntimeEnvironment;
import io.firstbridge.fbc.util.env.RuntimeMode;
import io.firstbridge.fbc.util.env.WindowsServiceMode;

/**
 *
 * @author al
 */
public class RunMode {
    public static RuntimeMode getRuntimeMode() {
        System.out.println("isHeadless=" + RuntimeEnvironment.isHeadless());
        if (RuntimeEnvironment.isDesktopEnabled()) {
            return new DesktopMode();
        } else if (RuntimeEnvironment.isWindowsService()) {
            return new WindowsServiceMode();
        } else {
            return new CommandLineMode();
        }
    }
    
}
