/*
 * Copyright © 2013-2016 The Nxt Core Developers.
 * Copyright © 2016-2018 Jelurida IP B.V.
 *
 * See the LICENSE.txt file at the top-level directory of this distribution
 * for licensing information.
 *
 * Unless otherwise agreed in a custom licensing agreement with Jelurida B.V.,
 * no part of the Nxt software, including this file, may be copied, modified,
 * propagated, or distributed except according to the terms contained in the
 * LICENSE.txt file.
 *
 * Removal or modification of this copyright notice is prohibited.
 *
 */

package io.firstbridge.fbc.env;
import io.firstbridge.fbc.util.env.LookAndFeel;
import io.firstbridge.fbc.util.env.RuntimeMode;
import io.firstbridge.fbc.util.env.ServerStatus;

import javax.swing.*;
import java.io.File;
import java.net.URI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DesktopMode implements RuntimeMode {
   static final Logger logger = LoggerFactory.getLogger(DesktopMode.class);

    private DesktopSystemTray desktopSystemTray;
    private Class desktopApplication;
    public final static String DESKTOP_APP="io.firstbridge.fbc.desktop.DesktopApplication";
    @Override
    public void init() {
        LookAndFeel.init();
        desktopSystemTray = new DesktopSystemTray();
        SwingUtilities.invokeLater(desktopSystemTray::createAndShowGUI);
    }

    @Override
    public void setServerStatus(ServerStatus status, URI wallet, File logFileDir) {
        desktopSystemTray.setToolTip(new SystemTrayDataProvider(status.getMessage(), wallet, logFileDir));
    }

    @Override
    public void launchDesktopApplication() {
        logger.info("Launching desktop wallet");
        try {
            desktopApplication = Class.forName(DESKTOP_APP);
            desktopApplication.getMethod("launch").invoke(null);
        } catch (ReflectiveOperationException e) {
            logger.info(DESKTOP_APP+" failed to launch", e);
        }
    }

    @Override
    public void shutdown() {
        desktopSystemTray.shutdown();
        if (desktopApplication == null) {
            return;
        }
        try {
            desktopApplication.getMethod("shutdown").invoke(null);
        } catch (ReflectiveOperationException e) {
            logger.info("nxtdesktop.DesktopApplication failed to shutdown", e);
        }
    }

    @Override
    public void alert(String message) {
        desktopSystemTray.alert(message);
    }
}
