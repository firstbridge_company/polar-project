package io.firstbridge.fbc.util;

import java.io.File;

/**
 * This is holder class for HTTP API properties that being accessed from all
 * over the code. IT is not goo but is better than all this crap in API class
 *
 * @author al
 */
public class ApiProperties {
    private static String API_RESOURCE_SEARCH_PATH="./;../;../../;../../../";

    public static String API_PATH = "/nxt";
    public static final int TESTNET_API_PORT = 6876;
    public static final int TESTNET_API_SSLPORT = 6877;
    public static final String[] DISABLED_HTTP_METHODS = {"TRACE", "OPTIONS", "HEAD"};

    public static final int port = Constants.isTestnet ? TESTNET_API_PORT : PropertyHelper.getIntProperty("nxt.apiServerPort");
    public static final int sslPort = Constants.isTestnet ? TESTNET_API_SSLPORT : PropertyHelper.getIntProperty("nxt.apiServerSSLPort");
    public static final String host = PropertyHelper.getStringProperty("nxt.apiServerHost");
    public static final int maxRecords = PropertyHelper.getIntProperty("nxt.maxAPIRecords");
    public static final boolean enableAPIUPnP = PropertyHelper.getBooleanProperty("nxt.enableAPIUPnP");
    public static final int apiServerIdleTimeout = PropertyHelper.getIntProperty("nxt.apiServerIdleTimeout");
    public static final boolean apiServerCORS = PropertyHelper.getBooleanProperty("nxt.apiServerCORS");
    public static final String forwardedForHeader = PropertyHelper.getStringProperty("nxt.forwardedForHeader");
    public static final String adminPassword = PropertyHelper.getStringProperty("nxt.adminPassword", "", true);
    public static  boolean disableAdminPassword = PropertyHelper.getBooleanProperty("nxt.disableAdminPassword")
            || ("127.0.0.1".equals(host) && adminPassword.isEmpty());
    public static final boolean enableAPIServer = PropertyHelper.getBooleanProperty("nxt.enableAPIServer");
    public static final  boolean enableSSL = PropertyHelper.getBooleanProperty("nxt.apiSSL");
    public static final String apiResourceBase = PropertyHelper.findReource(API_RESOURCE_SEARCH_PATH,
            PropertyHelper.getStringProperty("nxt.apiResourceBase"));
    
}
