/*
 * Copyright © 2013-2016 The Nxt Core Developers.
 * Copyright © 2016-2018 Jelurida IP B.V.
 *
 * See the LICENSE.txt file at the top-level directory of this distribution
 * for licensing information.
 *
 * Unless otherwise agreed in a custom licensing agreement with Jelurida B.V.,
 * no part of the Nxt software, including this file, may be copied, modified,
 * propagated, or distributed except according to the terms contained in the
 * LICENSE.txt file.
 *
 * Removal or modification of this copyright notice is prohibited.
 *
 */

package io.firstbridge.fbc.util;

import org.bitlet.weupnp.GatewayDevice;
import org.bitlet.weupnp.GatewayDiscover;

import java.net.InetAddress;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Forward ports using the UPnP protocol.
 */
public class UPnP {
    static final Logger logger = LoggerFactory.getLogger(UPnP.class);

    /** Initialization done */
    private static boolean initDone = false;

    /** UPnP gateway device */
    private static GatewayDevice gateway = null;

    /** Local address */
    private static InetAddress localAddress;

    /** External address */
    private static InetAddress externalAddress;

    /**
     * Add a port to the UPnP mapping
     *
     * @param   port                Port to add
     */
    public static synchronized void addPort(int port, String description) {
        if (!initDone)
            init();
        //
        // Ignore the request if we didn't find a gateway device
        //
        if (gateway == null)
            return;
        //
        // Forward the port
        //
        try {
            if (gateway.addPortMapping(port, port, localAddress.getHostAddress(), "TCP",
                   description))//                    Nxt.APPLICATION + " " + Nxt.VERSION)) 
            {
                logger.debug("Mapped port [" + externalAddress.getHostAddress() + "]:" + port);
            } else {
                logger.debug("Unable to map port " + port);
            }
        } catch (Exception exc) {
            logger.error("Unable to map port " + port + ": " + exc.toString());
        }
    }

    /**
     * Delete a port from the UPnP mapping
     *
     * @param   port                Port to delete
     */
    public static synchronized void deletePort(int port) {
        if (!initDone || gateway == null)
            return;
        //
        // Delete the port
        //
        try {
            if (gateway.deletePortMapping(port, "TCP")) {
                logger.debug("Mapping deleted for port " + port);
            } else {
                logger.debug("Unable to delete mapping for port " + port);
            }
        } catch (Exception exc) {
            logger.error("Unable to delete mapping for port " + port + ": " + exc.toString());
        }
    }

    /**
     * Return the local address
     *
     * @return                      Local address or null if the address is not available
     */
    public static synchronized InetAddress getLocalAddress() {
        if (!initDone)
            init();
        return localAddress;
    }

    /**
     * Return the external address
     *
     * @return                      External address or null if the address is not available
     */
    public static synchronized InetAddress getExternalAddress() {
        if (!initDone)
            init();
        return externalAddress;
    }

    /**
     * Initialize the UPnP support
     */
    private static void init() {
        initDone = true;
        //
        // Discover the gateway devices on the local network
        //
        try {
            logger.info("Looking for UPnP gateway device...");
            GatewayDevice.setHttpReadTimeout(PropertyHelper.getIntProperty("nxt.upnpGatewayTimeout", GatewayDevice.getHttpReadTimeout()));
            GatewayDiscover discover = new GatewayDiscover();
            discover.setTimeout(PropertyHelper.getIntProperty("nxt.upnpDiscoverTimeout", discover.getTimeout()));
            Map<InetAddress, GatewayDevice> gatewayMap = discover.discover();
            if (gatewayMap == null || gatewayMap.isEmpty()) {
                logger.debug("There are no UPnP gateway devices");
            } else {
                gatewayMap.forEach((addr, device) ->
                        logger.debug("UPnP gateway device found on " + addr.getHostAddress()));
                gateway = discover.getValidGateway();
                if (gateway == null) {
                    logger.debug("There is no connected UPnP gateway device");
                } else {
                    localAddress = gateway.getLocalAddress();
                    externalAddress = InetAddress.getByName(gateway.getExternalIPAddress());
                    logger.debug("Using UPnP gateway device on " + localAddress.getHostAddress());
                    logger.info("External IP address is " + externalAddress.getHostAddress());
                }
            }
        } catch (Exception exc) {
            logger.error("Unable to discover UPnP gateway devices: " + exc.toString());
        }
    }
}
