/*
 * Copyright © 2013-2016 The Nxt Core Developers.
 * Copyright © 2016-2018 Jelurida IP B.V.
 *
 * See the LICENSE.txt file at the top-level directory of this distribution
 * for licensing information.
 *
 * Unless otherwise agreed in a custom licensing agreement with Jelurida B.V.,
 * no part of the Nxt software, including this file, may be copied, modified,
 * propagated, or distributed except according to the terms contained in the
 * LICENSE.txt file.
 *
 * Removal or modification of this copyright notice is prohibited.
 *
 */

package io.firstbridge.fbc.util.env;

import io.firstbridge.fbc.util.Constants;
import java.nio.file.Paths;

public class UnixUserDirProvider extends DesktopUserDirProvider {

    private static final String FBC_USER_HOME = Paths.get(System.getProperty("user.home"),
            "." + Constants.APPLICATION.toLowerCase()).toString();

    @Override
    public String getUserHomeDir() {
        return FBC_USER_HOME;
    }
}
