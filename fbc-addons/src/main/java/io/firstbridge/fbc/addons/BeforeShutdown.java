/*
 * Copyright © 2013-2016 The Nxt Core Developers.
 * Copyright © 2016-2018 Jelurida IP B.V.
 *
 * See the LICENSE.txt file at the top-level directory of this distribution
 * for licensing information.
 *
 * Unless otherwise agreed in a custom licensing agreement with Jelurida B.V.,
 * no part of the Nxt software, including this file, may be copied, modified,
 * propagated, or distributed except according to the terms contained in the
 * LICENSE.txt file.
 *
 * Removal or modification of this copyright notice is prohibited.
 *
 */

package io.firstbridge.fbc.addons;

import io.firstbridge.fbc.http.addon.AddOn;
import io.firstbridge.fbc.util.PropertyHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class BeforeShutdown implements AddOn {
    static final Logger logger = LoggerFactory.getLogger(BeforeShutdown.class);
    private final String beforeShutdownScript = PropertyHelper.getStringProperty("nxt.beforeShutdownScript");

    @Override
    public void shutdown() {
        if (beforeShutdownScript != null) {
            try {
                Runtime.getRuntime().exec(beforeShutdownScript);
            } catch (Exception e) {
                logger.warn("Failed to run after start script: " + beforeShutdownScript, e);
            }
        }
    }

}
