/*
 * Copyright © 2013-2016 The Nxt Core Developers.
 * Copyright © 2016-2018 Jelurida IP B.V.
 *
 * See the LICENSE.txt file at the top-level directory of this distribution
 * for licensing information.
 *
 * Unless otherwise agreed in a custom licensing agreement with Jelurida B.V.,
 * no part of the Nxt software, including this file, may be copied, modified,
 * propagated, or distributed except according to the terms contained in the
 * LICENSE.txt file.
 *
 * Removal or modification of this copyright notice is prohibited.
 *
 */

package io.firstbridge.fbc.addons;

import io.firstbridge.fbc.http.addon.AddOn;
import io.firstbridge.fbc.util.PropertyHelper;
import io.firstbridge.fbc.util.ThreadPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class AfterStart implements AddOn {
    static final Logger logger = LoggerFactory.getLogger(AfterStart.class);
    @Override
    public void init() {
        String afterStartScript = PropertyHelper.getStringProperty("nxt.afterStartScript");
        if (afterStartScript != null) {
            ThreadPool.runAfterStart(() -> {
                try {
                    Runtime.getRuntime().exec(afterStartScript);
                } catch (Exception e) {
                    logger.error("Failed to run after start script: " + afterStartScript, e);
                }
            });
        }
    }

}
