/*
 * Copyright © 2013-2016 The Nxt Core Developers.
 * Copyright © 2016-2018 Jelurida IP B.V.
 *
 * See the LICENSE.txt file at the top-level directory of this distribution
 * for licensing information.
 *
 * Unless otherwise agreed in a custom licensing agreement with Jelurida B.V.,
 * no part of the Nxt software, including this file, may be copied, modified,
 * propagated, or distributed except according to the terms contained in the
 * LICENSE.txt file.
 *
 * Removal or modification of this copyright notice is prohibited.
 *
 */

package io.firstbridge.fbc.addons;

import io.firstbridge.fbc.http.addon.AddOn;
import io.firstbridge.fbc.core.account.Account;
import io.firstbridge.fbc.core.interfaces.BlockchainProcessor;
import io.firstbridge.fbc.core.Convert2;
import io.firstbridge.fbc.core.DiH;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class Demo implements AddOn {
    static final Logger logger = LoggerFactory.getLogger(Demo.class);
    @Override
    public void init() {
        DiH.getBlockchainProcessor().addListener(block -> logger.info("Block " + block.getStringId()
                + " has been forged by account " + Convert2.rsAccount(block.getGeneratorId()) + " having effective balance of "
                + Account.getAccount(block.getGeneratorId()).getEffectiveBalanceNXT()),
                BlockchainProcessor.Event.BEFORE_BLOCK_APPLY);
    }

    @Override
    public void shutdown() {
        logger.info("Goodbye!");
    }

}
