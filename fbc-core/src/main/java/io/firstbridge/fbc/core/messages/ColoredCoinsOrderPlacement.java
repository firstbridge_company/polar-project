/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.crypto.util.Convert;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public abstract class ColoredCoinsOrderPlacement extends AbstractAttachment {
    
    final long assetId;
    final long quantityQNT;
    final long priceNQT;

    public ColoredCoinsOrderPlacement(ByteBuffer buffer) {
        super(buffer);
        this.assetId = buffer.getLong();
        this.quantityQNT = buffer.getLong();
        this.priceNQT = buffer.getLong();
    }

    public ColoredCoinsOrderPlacement(JSONObject attachmentData) {
        super(attachmentData);
        this.assetId = Convert.parseUnsignedLong((String) attachmentData.get("asset"));
        this.quantityQNT = Convert.parseLong(attachmentData.get("quantityQNT"));
        this.priceNQT = Convert.parseLong(attachmentData.get("priceNQT"));
    }

    public ColoredCoinsOrderPlacement(long assetId, long quantityQNT, long priceNQT) {
        this.assetId = assetId;
        this.quantityQNT = quantityQNT;
        this.priceNQT = priceNQT;
    }

    @Override
    public int getMySize() {
        return 8 + 8 + 8;
    }

    @Override
    public void putMyBytes(ByteBuffer buffer) {
        buffer.putLong(assetId);
        buffer.putLong(quantityQNT);
        buffer.putLong(priceNQT);
    }

    @Override
    public void putMyJSON(JSONObject attachment) {
        attachment.put("asset", Long.toUnsignedString(assetId));
        attachment.put("quantityQNT", quantityQNT);
        attachment.put("priceNQT", priceNQT);
    }

    public long getAssetId() {
        return assetId;
    }

    public long getQuantityQNT() {
        return quantityQNT;
    }

    public long getPriceNQT() {
        return priceNQT;
    }
    
}
