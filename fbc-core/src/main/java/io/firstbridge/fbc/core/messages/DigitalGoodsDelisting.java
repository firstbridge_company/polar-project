/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.transaction.DigitalGoodsTr;
import io.firstbridge.fbc.transaction.TransactionType;
import io.firstbridge.fbc.crypto.util.Convert;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public final class DigitalGoodsDelisting extends AbstractAttachment {
    
    final long goodsId;

    public DigitalGoodsDelisting(ByteBuffer buffer) {
        super(buffer);
        this.goodsId = buffer.getLong();
    }

    public DigitalGoodsDelisting(JSONObject attachmentData) {
        super(attachmentData);
        this.goodsId = Convert.parseUnsignedLong((String) attachmentData.get("goods"));
    }

    public DigitalGoodsDelisting(long goodsId) {
        this.goodsId = goodsId;
    }

    @Override
    public int getMySize() {
        return 8;
    }

    @Override
    public void putMyBytes(ByteBuffer buffer) {
        buffer.putLong(goodsId);
    }

    @Override
    public void putMyJSON(JSONObject attachment) {
        attachment.put("goods", Long.toUnsignedString(goodsId));
    }

    @Override
    public TransactionType getTransactionType() {
        return DigitalGoodsTr.DELISTING;
    }

    public long getGoodsId() {
        return goodsId;
    }
    
}
