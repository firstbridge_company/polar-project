/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core;

import io.firstbridge.fbc.util.Constants;
import io.firstbridge.fbc.crypto.Crypto;

/**
 *
 * @author al
 */
public class Convert2 {
 
    public static final char[] hexChars = { '0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f' };
    public static final long[] multipliers = {1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000};
   
    public static String rsAccount(long accountId) {
        return Constants.ACCOUNT_PREFIX + "-" + Crypto.rsEncode(accountId);
    }
    
    public static long parseNXT(String nxt) {
        return parseStringFraction(nxt, 8, Constants.MAX_BALANCE_NXT);
    }

    private static long parseStringFraction(String value, int decimals, long maxValue) {
        String[] s = value.trim().split("\\.");
        if (s.length == 0 || s.length > 2) {
            throw new NumberFormatException("Invalid number: " + value);
        }
        long wholePart = Long.parseLong(s[0]);
        if (wholePart > maxValue) {
            throw new IllegalArgumentException("Whole part of value exceeds maximum possible");
        }
        if (s.length == 1) {
            return wholePart * multipliers[decimals];
        }
        long fractionalPart = Long.parseLong(s[1]);
        if (fractionalPart >= multipliers[decimals] || s[1].length() > decimals) {
            throw new IllegalArgumentException("Fractional part exceeds maximum allowed divisibility");
        }
        for (int i = s[1].length(); i < decimals; i++) {
            fractionalPart *= 10;
        }
        return wholePart * multipliers[decimals] + fractionalPart;
    }
     public static long fromEpochTime(int epochTime) {
        return epochTime * 1000L + Genesis.EPOCH_BEGINNING - 500L;
    }

    public static int toEpochTime(long currentTime) {
        return (int)((currentTime - Genesis.EPOCH_BEGINNING + 500) / 1000);
    }   
}
