/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.transaction.DigitalGoodsTr;
import io.firstbridge.fbc.transaction.TransactionType;
import io.firstbridge.fbc.crypto.util.Convert;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public final class DigitalGoodsPurchase extends AbstractAttachment {
    
    final long goodsId;
    final int quantity;
    final long priceNQT;
    final int deliveryDeadlineTimestamp;

    public DigitalGoodsPurchase(ByteBuffer buffer) {
        super(buffer);
        this.goodsId = buffer.getLong();
        this.quantity = buffer.getInt();
        this.priceNQT = buffer.getLong();
        this.deliveryDeadlineTimestamp = buffer.getInt();
    }

    public DigitalGoodsPurchase(JSONObject attachmentData) {
        super(attachmentData);
        this.goodsId = Convert.parseUnsignedLong((String) attachmentData.get("goods"));
        this.quantity = ((Long) attachmentData.get("quantity")).intValue();
        this.priceNQT = Convert.parseLong(attachmentData.get("priceNQT"));
        this.deliveryDeadlineTimestamp = ((Long) attachmentData.get("deliveryDeadlineTimestamp")).intValue();
    }

    public DigitalGoodsPurchase(long goodsId, int quantity, long priceNQT, int deliveryDeadlineTimestamp) {
        this.goodsId = goodsId;
        this.quantity = quantity;
        this.priceNQT = priceNQT;
        this.deliveryDeadlineTimestamp = deliveryDeadlineTimestamp;
    }

    @Override
    public int getMySize() {
        return 8 + 4 + 8 + 4;
    }

    @Override
    public void putMyBytes(ByteBuffer buffer) {
        buffer.putLong(goodsId);
        buffer.putInt(quantity);
        buffer.putLong(priceNQT);
        buffer.putInt(deliveryDeadlineTimestamp);
    }

    @Override
    public void putMyJSON(JSONObject attachment) {
        attachment.put("goods", Long.toUnsignedString(goodsId));
        attachment.put("quantity", quantity);
        attachment.put("priceNQT", priceNQT);
        attachment.put("deliveryDeadlineTimestamp", deliveryDeadlineTimestamp);
    }

    @Override
    public TransactionType getTransactionType() {
        return DigitalGoodsTr.PURCHASE;
    }

    public long getGoodsId() {
        return goodsId;
    }

    public int getQuantity() {
        return quantity;
    }

    public long getPriceNQT() {
        return priceNQT;
    }

    public int getDeliveryDeadlineTimestamp() {
        return deliveryDeadlineTimestamp;
    }
    
}
