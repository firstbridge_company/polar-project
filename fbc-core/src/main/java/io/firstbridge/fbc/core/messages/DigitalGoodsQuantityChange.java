/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.transaction.DigitalGoodsTr;
import io.firstbridge.fbc.transaction.TransactionType;
import io.firstbridge.fbc.crypto.util.Convert;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public final class DigitalGoodsQuantityChange extends AbstractAttachment {
    
    final long goodsId;
    final int deltaQuantity;

    public DigitalGoodsQuantityChange(ByteBuffer buffer) {
        super(buffer);
        this.goodsId = buffer.getLong();
        this.deltaQuantity = buffer.getInt();
    }

    public DigitalGoodsQuantityChange(JSONObject attachmentData) {
        super(attachmentData);
        this.goodsId = Convert.parseUnsignedLong((String) attachmentData.get("goods"));
        this.deltaQuantity = ((Long) attachmentData.get("deltaQuantity")).intValue();
    }

    public DigitalGoodsQuantityChange(long goodsId, int deltaQuantity) {
        this.goodsId = goodsId;
        this.deltaQuantity = deltaQuantity;
    }

    @Override
    public int getMySize() {
        return 8 + 4;
    }

    @Override
    public void putMyBytes(ByteBuffer buffer) {
        buffer.putLong(goodsId);
        buffer.putInt(deltaQuantity);
    }

    @Override
    public void putMyJSON(JSONObject attachment) {
        attachment.put("goods", Long.toUnsignedString(goodsId));
        attachment.put("deltaQuantity", deltaQuantity);
    }

    @Override
    public TransactionType getTransactionType() {
        return DigitalGoodsTr.QUANTITY_CHANGE;
    }

    public long getGoodsId() {
        return goodsId;
    }

    public int getDeltaQuantity() {
        return deltaQuantity;
    }
    
}
