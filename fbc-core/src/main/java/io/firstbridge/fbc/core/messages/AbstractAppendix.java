package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.core.account.Account;
import io.firstbridge.fbc.core.interfaces.Fee;
import io.firstbridge.fbc.core.interfaces.Transaction;
import io.firstbridge.fbc.util.FbcException;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
    public abstract class AbstractAppendix implements Appendix {

        private final byte version;

        public AbstractAppendix(JSONObject attachmentData) {
            version = ((Long) attachmentData.get("version." + getAppendixName())).byteValue();
        }

        public AbstractAppendix(ByteBuffer buffer) {
            version = buffer.get();
        }

        public AbstractAppendix(int version) {
            this.version = (byte) version;
        }

        public AbstractAppendix() {
            this.version = 1;
        }

        public abstract String getAppendixName();

        @Override
        public final int getSize() {
            return getMySize() + (version > 0 ? 1 : 0);
        }

        @Override
        public final int getFullSize() {
            return getMyFullSize() + (version > 0 ? 1 : 0);
        }

        public abstract int getMySize();

        public int getMyFullSize() {
            return getMySize();
        }

        @Override
        public final void putBytes(ByteBuffer buffer) {
            if (version > 0) {
                buffer.put(version);
            }
            putMyBytes(buffer);
        }

        public abstract void putMyBytes(ByteBuffer buffer);

        @Override
        public final JSONObject getJSONObject() {
            JSONObject json = new JSONObject();
            json.put("version." + getAppendixName(), version);
            putMyJSON(json);
            return json;
        }

        public abstract void putMyJSON(JSONObject json);

        @Override
        public final byte getVersion() {
            return version;
        }

        public boolean verifyVersion() {
            return version == 1;
        }

        @Override
        public int getBaselineFeeHeight() {
            return 1;
        }

        @Override
        public Fee getBaselineFee(Transaction transaction) {
            return Fee.NONE;
        }

        @Override
        public int getNextFeeHeight() {
            return Integer.MAX_VALUE;
        }

        @Override
        public Fee getNextFee(Transaction transaction) {
            return getBaselineFee(transaction);
        }

        public abstract void validate(Transaction transaction) throws FbcException.ValidationException;

        public void validateAtFinish(Transaction transaction) throws FbcException.ValidationException {
            if (!isPhased(transaction)) {
                return;
            }
            validate(transaction);
        }

        public abstract void apply(Transaction transaction, Account senderAccount, Account recipientAccount);

        public final void loadPrunable(Transaction transaction) {
            loadPrunable(transaction, false);
        }

        public void loadPrunable(Transaction transaction, boolean includeExpiredPrunable) {}

        public abstract boolean isPhasable();

        @Override
        public final boolean isPhased(Transaction transaction) {
            return isPhasable() && transaction.getPhasing() != null;
        }

    }


