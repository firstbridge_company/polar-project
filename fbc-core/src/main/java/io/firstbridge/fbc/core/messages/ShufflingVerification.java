
package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.transaction.ShufflingTransaction;
import io.firstbridge.fbc.transaction.TransactionType;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public final class ShufflingVerification extends AbstractShufflingAttachment {
    
    public ShufflingVerification(ByteBuffer buffer) {
        super(buffer);
    }

    public ShufflingVerification(JSONObject attachmentData) {
        super(attachmentData);
    }

    public ShufflingVerification(long shufflingId, byte[] shufflingStateHash) {
        super(shufflingId, shufflingStateHash);
    }

    @Override
    public TransactionType getTransactionType() {
        return ShufflingTransaction.SHUFFLING_VERIFICATION;
    }
    
}
