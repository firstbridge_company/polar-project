/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.transaction.AccountControlTr;
import io.firstbridge.fbc.transaction.TransactionType;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public final class AccountControlEffectiveBalanceLeasing extends AbstractAttachment {
    
    final int period;

    public AccountControlEffectiveBalanceLeasing(ByteBuffer buffer) {
        super(buffer);
        this.period = Short.toUnsignedInt(buffer.getShort());
    }

    public AccountControlEffectiveBalanceLeasing(JSONObject attachmentData) {
        super(attachmentData);
        this.period = ((Long) attachmentData.get("period")).intValue();
    }

    public AccountControlEffectiveBalanceLeasing(int period) {
        this.period = period;
    }

    @Override
    public int getMySize() {
        return 2;
    }

    @Override
    public void putMyBytes(ByteBuffer buffer) {
        buffer.putShort((short) period);
    }

    @Override
    public void putMyJSON(JSONObject attachment) {
        attachment.put("period", period);
    }

    @Override
    public TransactionType getTransactionType() {
        return AccountControlTr.EFFECTIVE_BALANCE_LEASING;
    }

    public int getPeriod() {
        return period;
    }
    
}
