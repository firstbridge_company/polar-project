/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.interfaces;

import java.util.Iterator;

/**
 *
 * @author al
 */
public interface DbIterator<T> extends AutoCloseable, Iterable<T>, Iterator<T> {

    void close();

    boolean hasNext();

    Iterator<T> iterator();

    T next();

    void remove();
    
}
