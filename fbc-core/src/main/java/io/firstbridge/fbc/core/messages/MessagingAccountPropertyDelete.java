/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.transaction.MessagingTr;
import io.firstbridge.fbc.transaction.TransactionType;
import io.firstbridge.fbc.crypto.util.Convert;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public final class MessagingAccountPropertyDelete extends AbstractAttachment {
    
    final long propertyId;

    public MessagingAccountPropertyDelete(ByteBuffer buffer) {
        super(buffer);
        this.propertyId = buffer.getLong();
    }

    public MessagingAccountPropertyDelete(JSONObject attachmentData) {
        super(attachmentData);
        this.propertyId = Convert.parseUnsignedLong((String) attachmentData.get("property"));
    }

    public MessagingAccountPropertyDelete(long propertyId) {
        this.propertyId = propertyId;
    }

    @Override
    public int getMySize() {
        return 8;
    }

    @Override
    public void putMyBytes(ByteBuffer buffer) {
        buffer.putLong(propertyId);
    }

    @Override
    public void putMyJSON(JSONObject attachment) {
        attachment.put("property", Long.toUnsignedString(propertyId));
    }

    @Override
    public TransactionType getTransactionType() {
        return MessagingTr.ACCOUNT_PROPERTY_DELETE;
    }

    public long getPropertyId() {
        return propertyId;
    }
    
}
