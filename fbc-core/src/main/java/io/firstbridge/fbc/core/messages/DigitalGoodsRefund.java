/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.transaction.DigitalGoodsTr;
import io.firstbridge.fbc.transaction.TransactionType;
import io.firstbridge.fbc.crypto.util.Convert;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public final class DigitalGoodsRefund extends AbstractAttachment {
    
    final long purchaseId;
    final long refundNQT;

    public DigitalGoodsRefund(ByteBuffer buffer) {
        super(buffer);
        this.purchaseId = buffer.getLong();
        this.refundNQT = buffer.getLong();
    }

    public DigitalGoodsRefund(JSONObject attachmentData) {
        super(attachmentData);
        this.purchaseId = Convert.parseUnsignedLong((String) attachmentData.get("purchase"));
        this.refundNQT = Convert.parseLong(attachmentData.get("refundNQT"));
    }

    public DigitalGoodsRefund(long purchaseId, long refundNQT) {
        this.purchaseId = purchaseId;
        this.refundNQT = refundNQT;
    }

    @Override
    public int getMySize() {
        return 8 + 8;
    }

    @Override
    public void putMyBytes(ByteBuffer buffer) {
        buffer.putLong(purchaseId);
        buffer.putLong(refundNQT);
    }

    @Override
    public void putMyJSON(JSONObject attachment) {
        attachment.put("purchase", Long.toUnsignedString(purchaseId));
        attachment.put("refundNQT", refundNQT);
    }

    @Override
    public TransactionType getTransactionType() {
        return DigitalGoodsTr.REFUND;
    }

    public long getPurchaseId() {
        return purchaseId;
    }

    public long getRefundNQT() {
        return refundNQT;
    }
    
}
