/*
 * Copyright © 2013-2016 The Nxt Core Developers.
 * Copyright © 2016-2018 Jelurida IP B.V.
 *
 * See the LICENSE.txt file at the top-level directory of this distribution
 * for licensing information.
 *
 * Unless otherwise agreed in a custom licensing agreement with Jelurida B.V.,
 * no part of the Nxt software, including this file, may be copied, modified,
 * propagated, or distributed except according to the terms contained in the
 * LICENSE.txt file.
 *
 * Removal or modification of this copyright notice is prohibited.
 *
 */

package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.core.DiH;
import io.firstbridge.fbc.util.Constants;
import io.firstbridge.fbc.core.interfaces.Fee;
import io.firstbridge.fbc.core.interfaces.Transaction;
import org.json.simple.JSONObject;

import java.nio.ByteBuffer;

public interface Appendix {

    public int getSize();
    public int getFullSize();
    public void putBytes(ByteBuffer buffer);
    public JSONObject getJSONObject();
    public byte getVersion();
    public int getBaselineFeeHeight();
    public Fee getBaselineFee(Transaction transaction);
    public int getNextFeeHeight();
    public Fee getNextFee(Transaction transaction);
    public boolean isPhased(Transaction transaction);

    public interface Prunable {
        public byte[] getHash();
        public boolean hasPrunableData();
        public void restorePrunableData(Transaction transaction, int blockTimestamp, int height);
        public default boolean shouldLoadPrunable(Transaction transaction, boolean includeExpiredPrunable) {
            return DiH.getEpochTime() - transaction.getTimestamp() <
                    (includeExpiredPrunable && Constants.INCLUDE_EXPIRED_PRUNABLE ?
                            Constants.MAX_PRUNABLE_LIFETIME : Constants.MIN_PRUNABLE_LIFETIME);
        }
    }

    public interface Encryptable {
        void encrypt(String secretPhrase);
    }


    public static boolean hasAppendix(String appendixName, JSONObject attachmentData) {
        return attachmentData.get("version." + appendixName) != null;
    }
  
}
