/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.transaction.MessagingTr;
import io.firstbridge.fbc.transaction.TransactionType;
import io.firstbridge.fbc.util.Constants;
import io.firstbridge.fbc.crypto.util.Convert;
import io.firstbridge.fbc.util.FbcException;
import java.nio.ByteBuffer;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public final class MessagingVoteCasting extends AbstractAttachment {
    
    final long pollId;
    final byte[] pollVote;

    public MessagingVoteCasting(ByteBuffer buffer) throws FbcException.NotValidException {
        super(buffer);
        pollId = buffer.getLong();
        int numberOfOptions = buffer.get();
        if (numberOfOptions > Constants.MAX_POLL_OPTION_COUNT) {
            throw new FbcException.NotValidException("More than " + Constants.MAX_POLL_OPTION_COUNT + " options in a vote");
        }
        pollVote = new byte[numberOfOptions];
        buffer.get(pollVote);
    }

    public MessagingVoteCasting(JSONObject attachmentData) {
        super(attachmentData);
        pollId = Convert.parseUnsignedLong((String) attachmentData.get("poll"));
        JSONArray vote = (JSONArray) attachmentData.get("vote");
        pollVote = new byte[vote.size()];
        for (int i = 0; i < pollVote.length; i++) {
            pollVote[i] = ((Long) vote.get(i)).byteValue();
        }
    }

    public MessagingVoteCasting(long pollId, byte[] pollVote) {
        this.pollId = pollId;
        this.pollVote = pollVote;
    }

    @Override
    public int getMySize() {
        return 8 + 1 + this.pollVote.length;
    }

    @Override
    public void putMyBytes(ByteBuffer buffer) {
        buffer.putLong(this.pollId);
        buffer.put((byte) this.pollVote.length);
        buffer.put(this.pollVote);
    }

    @Override
    public void putMyJSON(JSONObject attachment) {
        attachment.put("poll", Long.toUnsignedString(this.pollId));
        JSONArray vote = new JSONArray();
        if (this.pollVote != null) {
            for (byte aPollVote : this.pollVote) {
                vote.add(aPollVote);
            }
        }
        attachment.put("vote", vote);
    }

    @Override
    public TransactionType getTransactionType() {
        return MessagingTr.VOTE_CASTING;
    }

    public long getPollId() {
        return pollId;
    }

    public byte[] getPollVote() {
        return pollVote;
    }
    
}
