/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.core.TaggedData;
import io.firstbridge.fbc.transaction.TransactionType;
import io.firstbridge.fbc.core.interfaces.Transaction;
import io.firstbridge.fbc.transaction.DataTr;
import io.firstbridge.fbc.crypto.util.Convert;
import io.firstbridge.fbc.util.FbcException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public final class TaggedDataUpload extends TaggedDataAttachment {
    
    public static TaggedDataUpload parse(JSONObject attachmentData) {
        if (!Appendix.hasAppendix(DataTr.TAGGED_DATA_UPLOAD.getName(), attachmentData)) {
            return null;
        }
        return new TaggedDataUpload(attachmentData);
    }
    final byte[] hash;

    public TaggedDataUpload(ByteBuffer buffer) {
        super(buffer);
        this.hash = new byte[32];
        buffer.get(hash);
    }

    public TaggedDataUpload(JSONObject attachmentData) {
        super(attachmentData);
        String dataJSON = (String) attachmentData.get("data");
        if (dataJSON == null) {
            this.hash = Convert.parseHexString(Convert.emptyToNull((String) attachmentData.get("hash")));
        } else {
            this.hash = null;
        }
    }

    public TaggedDataUpload(String name, String description, String tags, String type, String channel, boolean isText, String filename, byte[] data) throws FbcException.NotValidException {
        super(name, description, tags, type, channel, isText, filename, data);
        this.hash = null;
        if (isText && !Arrays.equals(data, Convert.toBytes(Convert.toString(data)))) {
            throw new FbcException.NotValidException("Data is not UTF-8 text");
        }
    }

    @Override
    public int getMySize() {
        return 32;
    }

    @Override
    public void putMyBytes(ByteBuffer buffer) {
        buffer.put(getHash());
    }

    @Override
    public void putMyJSON(JSONObject attachment) {
        super.putMyJSON(attachment);
        attachment.put("hash", Convert.toHexString(getHash()));
    }

    @Override
    public TransactionType getTransactionType() {
        return DataTr.TAGGED_DATA_UPLOAD;
    }

    @Override
    public byte[] getHash() {
        if (hash != null) {
            return hash;
        }
        return super.getHash();
    }

    @Override
    public long getTaggedDataId(Transaction transaction) {
        return transaction.getId();
    }

    @Override
    public void restorePrunableData(Transaction transaction, int blockTimestamp, int height) {
        TaggedData.restore(transaction, this, blockTimestamp, height);
    }
    
}
