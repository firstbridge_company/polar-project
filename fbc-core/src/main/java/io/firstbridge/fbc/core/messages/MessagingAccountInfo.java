/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.messages;

import io.firstbridge.cryptolib.CryptoNotValidException;
import io.firstbridge.fbc.transaction.MessagingTr;
import io.firstbridge.fbc.transaction.TransactionType;
import io.firstbridge.fbc.util.Constants;
import io.firstbridge.fbc.crypto.util.Convert;
import io.firstbridge.fbc.util.FbcException;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public final class MessagingAccountInfo extends AbstractAttachment {
    
    final String name;
    final String description;

    public MessagingAccountInfo(ByteBuffer buffer) throws FbcException.NotValidException {
        super(buffer);
        try {
            this.name = Convert.readString(buffer, buffer.get(), Constants.MAX_ACCOUNT_NAME_LENGTH);
            this.description = Convert.readString(buffer, buffer.getShort(), Constants.MAX_ACCOUNT_DESCRIPTION_LENGTH);
        } catch (CryptoNotValidException ex) {
            throw new FbcException.NotValidException(ex.getMessage());
        }
    }

    public MessagingAccountInfo(JSONObject attachmentData) {
        super(attachmentData);
        this.name = Convert.nullToEmpty((String) attachmentData.get("name"));
        this.description = Convert.nullToEmpty((String) attachmentData.get("description"));
    }

    public MessagingAccountInfo(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public int getMySize() {
        return 1 + Convert.toBytes(name).length + 2 + Convert.toBytes(description).length;
    }

    @Override
    public void putMyBytes(ByteBuffer buffer) {
        byte[] name = Convert.toBytes(this.name);
        byte[] description = Convert.toBytes(this.description);
        buffer.put((byte) name.length);
        buffer.put(name);
        buffer.putShort((short) description.length);
        buffer.put(description);
    }

    @Override
    public void putMyJSON(JSONObject attachment) {
        attachment.put("name", name);
        attachment.put("description", description);
    }

    @Override
    public TransactionType getTransactionType() {
        return MessagingTr.ACCOUNT_INFO;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
    
}
