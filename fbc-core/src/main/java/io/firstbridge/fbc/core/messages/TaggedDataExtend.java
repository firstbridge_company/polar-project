package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.core.TaggedData;
import io.firstbridge.fbc.core.TransactionDb;
import io.firstbridge.fbc.transaction.TransactionType;
import io.firstbridge.fbc.core.interfaces.Transaction;
import io.firstbridge.fbc.transaction.DataTr;
import io.firstbridge.fbc.crypto.util.Convert;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public final class TaggedDataExtend extends TaggedDataAttachment {
    
    public static TaggedDataExtend parse(JSONObject attachmentData) {
        if (!Appendix.hasAppendix(DataTr.TAGGED_DATA_EXTEND.getName(), attachmentData)) {
            return null;
        }
        return new TaggedDataExtend(attachmentData);
    }
    private volatile byte[] hash;
    final long taggedDataId;
    final boolean jsonIsPruned;

    public TaggedDataExtend(ByteBuffer buffer) {
        super(buffer);
        this.taggedDataId = buffer.getLong();
        this.jsonIsPruned = false;
    }

    public TaggedDataExtend(JSONObject attachmentData) {
        super(attachmentData);
        this.taggedDataId = Convert.parseUnsignedLong((String) attachmentData.get("taggedData"));
        this.jsonIsPruned = attachmentData.get("data") == null;
    }

    public TaggedDataExtend(TaggedData taggedData) {
        super(taggedData.getName(), taggedData.getDescription(), taggedData.getTags(), taggedData.getType(), taggedData.getChannel(), taggedData.isText(), taggedData.getFilename(), taggedData.getData());
        this.taggedDataId = taggedData.getId();
        this.jsonIsPruned = false;
    }

    @Override
    public int getMySize() {
        return 8;
    }

    @Override
    public void putMyBytes(ByteBuffer buffer) {
        buffer.putLong(taggedDataId);
    }

    @Override
    public void putMyJSON(JSONObject attachment) {
        super.putMyJSON(attachment);
        attachment.put("taggedData", Long.toUnsignedString(taggedDataId));
    }

    @Override
    public TransactionType getTransactionType() {
        return DataTr.TAGGED_DATA_EXTEND;
    }

    public long getTaggedDataId() {
        return taggedDataId;
    }

    @Override
    public byte[] getHash() {
        if (hash == null) {
            hash = super.getHash();
        }
        if (hash == null) {
            TaggedDataUpload taggedDataUpload = (TaggedDataUpload) TransactionDb.findTransaction(taggedDataId).getAttachment();
            hash = taggedDataUpload.getHash();
        }
        return hash;
    }

    @Override
    public long getTaggedDataId(Transaction transaction) {
        return taggedDataId;
    }

    public boolean jsonIsPruned() {
        return jsonIsPruned;
    }

    @Override
    public void restorePrunableData(Transaction transaction, int blockTimestamp, int height) {
    }
    
}
