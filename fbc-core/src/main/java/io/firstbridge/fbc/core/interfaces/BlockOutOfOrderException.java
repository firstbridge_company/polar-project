package io.firstbridge.fbc.core.interfaces;

/**
 *
 * @author al
 */
public class BlockOutOfOrderException extends BlockNotAcceptedException {

    public BlockOutOfOrderException(String message, Block block) {
        super(message, block);
    }

}
