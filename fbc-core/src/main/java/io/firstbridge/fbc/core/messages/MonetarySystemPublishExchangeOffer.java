/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.core.MonetarySystem;
import io.firstbridge.fbc.transaction.TransactionType;
import io.firstbridge.fbc.crypto.util.Convert;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public final class MonetarySystemPublishExchangeOffer extends AbstractAttachment implements Attachment.MonetarySystemAttachment {
    
    final long currencyId;
    final long buyRateNQT;
    final long sellRateNQT;
    final long totalBuyLimit;
    final long totalSellLimit;
    final long initialBuySupply;
    final long initialSellSupply;
    final int expirationHeight;

    public MonetarySystemPublishExchangeOffer(ByteBuffer buffer) {
        super(buffer);
        this.currencyId = buffer.getLong();
        this.buyRateNQT = buffer.getLong();
        this.sellRateNQT = buffer.getLong();
        this.totalBuyLimit = buffer.getLong();
        this.totalSellLimit = buffer.getLong();
        this.initialBuySupply = buffer.getLong();
        this.initialSellSupply = buffer.getLong();
        this.expirationHeight = buffer.getInt();
    }

    public MonetarySystemPublishExchangeOffer(JSONObject attachmentData) {
        super(attachmentData);
        this.currencyId = Convert.parseUnsignedLong((String) attachmentData.get("currency"));
        this.buyRateNQT = Convert.parseLong(attachmentData.get("buyRateNQT"));
        this.sellRateNQT = Convert.parseLong(attachmentData.get("sellRateNQT"));
        this.totalBuyLimit = Convert.parseLong(attachmentData.get("totalBuyLimit"));
        this.totalSellLimit = Convert.parseLong(attachmentData.get("totalSellLimit"));
        this.initialBuySupply = Convert.parseLong(attachmentData.get("initialBuySupply"));
        this.initialSellSupply = Convert.parseLong(attachmentData.get("initialSellSupply"));
        this.expirationHeight = ((Long) attachmentData.get("expirationHeight")).intValue();
    }

    public MonetarySystemPublishExchangeOffer(long currencyId, long buyRateNQT, long sellRateNQT, long totalBuyLimit, long totalSellLimit, long initialBuySupply, long initialSellSupply, int expirationHeight) {
        this.currencyId = currencyId;
        this.buyRateNQT = buyRateNQT;
        this.sellRateNQT = sellRateNQT;
        this.totalBuyLimit = totalBuyLimit;
        this.totalSellLimit = totalSellLimit;
        this.initialBuySupply = initialBuySupply;
        this.initialSellSupply = initialSellSupply;
        this.expirationHeight = expirationHeight;
    }

    @Override
    public int getMySize() {
        return 8 + 8 + 8 + 8 + 8 + 8 + 8 + 4;
    }

    @Override
    public void putMyBytes(ByteBuffer buffer) {
        buffer.putLong(currencyId);
        buffer.putLong(buyRateNQT);
        buffer.putLong(sellRateNQT);
        buffer.putLong(totalBuyLimit);
        buffer.putLong(totalSellLimit);
        buffer.putLong(initialBuySupply);
        buffer.putLong(initialSellSupply);
        buffer.putInt(expirationHeight);
    }

    @Override
    public void putMyJSON(JSONObject attachment) {
        attachment.put("currency", Long.toUnsignedString(currencyId));
        attachment.put("buyRateNQT", buyRateNQT);
        attachment.put("sellRateNQT", sellRateNQT);
        attachment.put("totalBuyLimit", totalBuyLimit);
        attachment.put("totalSellLimit", totalSellLimit);
        attachment.put("initialBuySupply", initialBuySupply);
        attachment.put("initialSellSupply", initialSellSupply);
        attachment.put("expirationHeight", expirationHeight);
    }

    @Override
    public TransactionType getTransactionType() {
        return MonetarySystem.PUBLISH_EXCHANGE_OFFER;
    }

    @Override
    public long getCurrencyId() {
        return currencyId;
    }

    public long getBuyRateNQT() {
        return buyRateNQT;
    }

    public long getSellRateNQT() {
        return sellRateNQT;
    }

    public long getTotalBuyLimit() {
        return totalBuyLimit;
    }

    public long getTotalSellLimit() {
        return totalSellLimit;
    }

    public long getInitialBuySupply() {
        return initialBuySupply;
    }

    public long getInitialSellSupply() {
        return initialSellSupply;
    }

    public int getExpirationHeight() {
        return expirationHeight;
    }
    
}
