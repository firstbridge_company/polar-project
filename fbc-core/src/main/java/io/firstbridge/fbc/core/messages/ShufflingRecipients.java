
package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.transaction.ShufflingTransaction;
import io.firstbridge.fbc.transaction.TransactionType;
import io.firstbridge.fbc.util.Constants;
import io.firstbridge.fbc.crypto.util.Convert;
import io.firstbridge.fbc.util.FbcException;
import java.nio.ByteBuffer;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public final class ShufflingRecipients extends AbstractShufflingAttachment {
    
    final byte[][] recipientPublicKeys;

    public ShufflingRecipients(ByteBuffer buffer) throws FbcException.NotValidException {
        super(buffer);
        int count = buffer.get();
        if (count > Constants.MAX_NUMBER_OF_SHUFFLING_PARTICIPANTS || count < 0) {
            throw new FbcException.NotValidException("Invalid data count " + count);
        }
        this.recipientPublicKeys = new byte[count][];
        for (int i = 0; i < count; i++) {
            this.recipientPublicKeys[i] = new byte[32];
            buffer.get(this.recipientPublicKeys[i]);
        }
    }

    public ShufflingRecipients(JSONObject attachmentData) {
        super(attachmentData);
        JSONArray jsonArray = (JSONArray) attachmentData.get("recipientPublicKeys");
        this.recipientPublicKeys = new byte[jsonArray.size()][];
        for (int i = 0; i < this.recipientPublicKeys.length; i++) {
            this.recipientPublicKeys[i] = Convert.parseHexString((String) jsonArray.get(i));
        }
    }

    public ShufflingRecipients(long shufflingId, byte[][] recipientPublicKeys, byte[] shufflingStateHash) {
        super(shufflingId, shufflingStateHash);
        this.recipientPublicKeys = recipientPublicKeys;
    }

    @Override
    public int getMySize() {
        int size = super.getMySize();
        size += 1;
        size += 32 * recipientPublicKeys.length;
        return size;
    }

    @Override
    public void putMyBytes(ByteBuffer buffer) {
        super.putMyBytes(buffer);
        buffer.put((byte) recipientPublicKeys.length);
        for (byte[] bytes : recipientPublicKeys) {
            buffer.put(bytes);
        }
    }

    @Override
    public void putMyJSON(JSONObject attachment) {
        super.putMyJSON(attachment);
        JSONArray jsonArray = new JSONArray();
        attachment.put("recipientPublicKeys", jsonArray);
        for (byte[] bytes : recipientPublicKeys) {
            jsonArray.add(Convert.toHexString(bytes));
        }
    }

    @Override
    public TransactionType getTransactionType() {
        return ShufflingTransaction.SHUFFLING_RECIPIENTS;
    }

    public byte[][] getRecipientPublicKeys() {
        return recipientPublicKeys;
    }
    
}
