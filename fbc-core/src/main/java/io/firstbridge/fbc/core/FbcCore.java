package io.firstbridge.fbc.core;

import io.firstbridge.fbc.core.account.Account;
import io.firstbridge.fbc.core.account.AccountLedger;
import io.firstbridge.fbc.core.account.AccountRestrictions;
import io.firstbridge.fbc.core.peer.Peers;
import io.firstbridge.fbc.crypto.Crypto;
import io.firstbridge.fbc.db.Db;
import io.firstbridge.fbc.http.API;
import io.firstbridge.fbc.http.APIProxy;
import io.firstbridge.fbc.http.AddOns;
import io.firstbridge.fbc.transaction.TransactionProcessorImpl;
import io.firstbridge.fbc.util.Constants;
import io.firstbridge.fbc.util.PropertyHelper;
import io.firstbridge.fbc.util.ThreadPool;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class initializes core and prepares other things
 *
 * @author al
 */
public class FbcCore {
   static final Logger logger = LoggerFactory.getLogger(FbcCore.class);

    public static final String NXT_DEFAULT_PROPERTIES = "nxt-default.properties";
    public static final String NXT_PROPERTIES = "nxt.properties";
 
    public static final String NXT_INSTALLER_PROPERTIES = "nxt-installer.properties";

    public static void init() {
        //make jetty less verbose
        System.setProperty("org.eclipse.jetty.util.log.class", "org.eclipse.jetty.util.log.StdErrLog");
        System.setProperty("org.eclipse.jetty.LEVEL", "WARN");
        PropertyHelper.loadProperties(PropertyHelper.properties, NXT_DEFAULT_PROPERTIES, true);
        String propertyVersion=PropertyHelper.properties.getProperty("nxt.version");
        if (!Constants.VERSION.equals(propertyVersion)) {
            throw new RuntimeException("Using an nxt-default.properties file from "
                    +"a version other than " + Constants.VERSION + " is not supported!!!");
        } 
        PropertyHelper.loadProperties(PropertyHelper.properties, NXT_INSTALLER_PROPERTIES, true);
        PropertyHelper.loadProperties(PropertyHelper.properties, NXT_PROPERTIES, false);
        
        long startTime = System.currentTimeMillis();
        setSystemProperties();
        logSystemProperties();
        Thread secureRandomInitThread = initSecureRandom();
        Genesis.init();
        
        Db.init();

        TransactionProcessorImpl.getInstance();
        BlockchainProcessorImpl.getInstance();
        Account.init();
        AccountRestrictions.init();
        AccountLedger.init();
        Alias.init();
        Asset.init();
        DigitalGoodsStore.init();
        Order.init();
        Poll.init();
        PhasingPoll.init();
        Trade.init();
        AssetTransfer.init();
        AssetDelete.init();
        AssetDividend.init();
        Vote.init();
        PhasingVote.init();
        Currency.init();
        CurrencyBuyOffer.init();
        CurrencySellOffer.init();
        CurrencyFounder.init();
        CurrencyMint.init();
        CurrencyTransfer.init();
        Exchange.init();
        ExchangeRequest.init();
        Shuffling.init();
        ShufflingParticipant.init();
        PrunableMessage.init();
        TaggedData.init();
        Peers.init();
        APIProxy.init();
        Generator.init();
        AddOns.init();
        API.init();
        DebugTrace.init();
        int timeMultiplier = (Constants.isTestnet && Constants.isOffline) ? Math.max(PropertyHelper.getIntProperty("nxt.timeMultiplier"), 1) : 1;
        ThreadPool.start(timeMultiplier);
        if (timeMultiplier > 1) {
            DiH.setTime(new Time.FasterTime(Math.max(DiH.getEpochTime(), DiH.getBlockchain().getLastBlock().getTimestamp()), timeMultiplier));
            logger.info("TIME WILL FLOW " + timeMultiplier + " TIMES FASTER!");
        }
        try {
            secureRandomInitThread.join(10000);
        } catch (InterruptedException ignore) {
        }
        testSecureRandom();
        long currentTime = System.currentTimeMillis();
        logger.info("Initialization took " + (currentTime - startTime) / 1000 + " seconds");
        logger.info(Constants.APPLICATION + " server " + Constants.VERSION + " started successfully.");
        logger.info("Copyright © 2013-2016 The Nxt Core Developers.");
        logger.info("Copyright © 2016-2018 Jelurida IP B.V.");
        logger.info("Distributed under the Jelurida Public License version 1.1 for the Nxt Public Blockchain Platform, with ABSOLUTELY NO WARRANTY.");
        if (API.getWelcomePageUri() != null) {
            logger.info("Client UI is at " + API.getWelcomePageUri());
        }

    }
    
    public static void init(Properties customProperties) {
        PropertyHelper.properties.putAll(customProperties);
        init();
    }
    public static void shutdown() {
        logger.warn("Shutting down...");
        AddOns.shutdown();
        API.shutdown();
        FundingMonitor.shutdown();
        ThreadPool.shutdown();
        BlockchainProcessorImpl.getInstance().shutdown();
        Peers.shutdown();
        Db.shutdown();
        logger.warn(Constants.APPLICATION + " server " + Constants.VERSION 
                + " stopped.");
    }

    private static void setSystemProperties() {
        // Override system settings that the user has define in nxt.properties file.
        String[] systemProperties = new String[]{
            "socksProxyHost",
            "socksProxyPort",};

        for (String propertyName : systemProperties) {
            String propertyValue;
            if ((propertyValue = PropertyHelper.getStringProperty(propertyName)) != null) {
                System.setProperty(propertyName, propertyValue);
            }
        }
    }

    private static void logSystemProperties() {
        String[] loggedProperties = new String[]{
            "java.version",
            "java.vm.version",
            "java.vm.name",
            "java.vendor",
            "java.vm.vendor",
            "java.home",
            "java.library.path",
            "java.class.path",
            "os.arch",
            "sun.arch.data.model",
            "os.name",
            "file.encoding",
            "java.security.policy",
            "java.security.manager", //               RuntimeEnvironment.RUNTIME_MODE_ARG,
        //               RuntimeEnvironment.DIRPROVIDER_ARG
        };
        for (String property : loggedProperties) {
            logger.debug(String.format("%s = %s", property, System.getProperty(property)));
        }
        logger.debug(String.format("availableProcessors = %s", Runtime.getRuntime().availableProcessors()));
        logger.debug(String.format("maxMemory = %s", Runtime.getRuntime().maxMemory()));
        logger.debug(String.format("processId = %s", PropertyHelper.getProcessId()));
    }

    private static Thread initSecureRandom() {
        Thread secureRandomInitThread = new Thread(() -> Crypto.getSecureRandom().nextBytes(new byte[1024]));
        secureRandomInitThread.setDaemon(true);
        secureRandomInitThread.start();
        return secureRandomInitThread;
    }

    private static void testSecureRandom() {
        Thread thread = new Thread(() -> Crypto.getSecureRandom().nextBytes(new byte[1024]));
        thread.setDaemon(true);
        thread.start();
        try {
            thread.join(2000);
            if (thread.isAlive()) {
                throw new RuntimeException("SecureRandom implementation too slow!!! "
                        + "Install haveged if on linux, or set nxt.useStrongSecureRandom=false.");
            }
        } catch (InterruptedException ignore) {
        }
    }

}
