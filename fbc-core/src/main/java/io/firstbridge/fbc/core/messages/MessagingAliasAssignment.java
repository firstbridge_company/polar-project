/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.messages;

import io.firstbridge.cryptolib.CryptoNotValidException;
import io.firstbridge.fbc.transaction.MessagingTr;
import io.firstbridge.fbc.transaction.TransactionType;
import io.firstbridge.fbc.util.Constants;
import io.firstbridge.fbc.crypto.util.Convert;
import io.firstbridge.fbc.util.FbcException;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public final class MessagingAliasAssignment extends AbstractAttachment {
    
    final String aliasName;
    final String aliasURI;

    public MessagingAliasAssignment(ByteBuffer buffer) throws FbcException.NotValidException {
        super(buffer);
        try {
            aliasName = Convert.readString(buffer, buffer.get(), Constants.MAX_ALIAS_LENGTH).trim();
            aliasURI = Convert.readString(buffer, buffer.getShort(), Constants.MAX_ALIAS_URI_LENGTH).trim();
        } catch (CryptoNotValidException ex) {
            throw new FbcException.NotValidException(ex.getMessage());
        }
    }

    public MessagingAliasAssignment(JSONObject attachmentData) {
        super(attachmentData);
        aliasName = Convert.nullToEmpty((String) attachmentData.get("alias")).trim();
        aliasURI = Convert.nullToEmpty((String) attachmentData.get("uri")).trim();
    }

    public MessagingAliasAssignment(String aliasName, String aliasURI) {
        this.aliasName = aliasName.trim();
        this.aliasURI = aliasURI.trim();
    }

    @Override
    public int getMySize() {
        return 1 + Convert.toBytes(aliasName).length + 2 + Convert.toBytes(aliasURI).length;
    }

    @Override
    public void putMyBytes(ByteBuffer buffer) {
        byte[] alias = Convert.toBytes(this.aliasName);
        byte[] uri = Convert.toBytes(this.aliasURI);
        buffer.put((byte) alias.length);
        buffer.put(alias);
        buffer.putShort((short) uri.length);
        buffer.put(uri);
    }

    @Override
    public void putMyJSON(JSONObject attachment) {
        attachment.put("alias", aliasName);
        attachment.put("uri", aliasURI);
    }

    @Override
    public TransactionType getTransactionType() {
        return MessagingTr.ALIAS_ASSIGNMENT;
    }

    public String getAliasName() {
        return aliasName;
    }

    public String getAliasURI() {
        return aliasURI;
    }
    
}
