package io.firstbridge.fbc.core.messages;

import static io.firstbridge.fbc.core.messages.Appendix.hasAppendix;
import io.firstbridge.fbc.core.interfaces.Transaction;
import io.firstbridge.fbc.crypto.EncryptedData;
import io.firstbridge.fbc.util.FbcException;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
    public class EncryptedMessage extends AbstractEncryptedMessage {

        private static final String appendixName = "EncryptedMessage";

        public static EncryptedMessage parse(JSONObject attachmentData) {
            if (!hasAppendix(appendixName, attachmentData)) {
                return null;
            }
            if (((JSONObject)attachmentData.get("encryptedMessage")).get("data") == null) {
                return new UnencryptedEncryptedMessage(attachmentData);
            }
            return new EncryptedMessage(attachmentData);
        }

        public EncryptedMessage(ByteBuffer buffer) throws FbcException.NotValidException {
            super(buffer);
        }

        public EncryptedMessage(JSONObject attachmentData) {
            super(attachmentData, (JSONObject)attachmentData.get("encryptedMessage"));
        }

        public EncryptedMessage(EncryptedData encryptedData, boolean isText, boolean isCompressed) {
            super(encryptedData, isText, isCompressed);
        }

        @Override
        public final String getAppendixName() {
            return appendixName;
        }

        @Override
        public void putMyJSON(JSONObject json) {
            JSONObject encryptedMessageJSON = new JSONObject();
            super.putMyJSON(encryptedMessageJSON);
            json.put("encryptedMessage", encryptedMessageJSON);
        }

        @Override
        public void validate(Transaction transaction) throws FbcException.ValidationException {
            super.validate(transaction);
            if (transaction.getRecipientId() == 0) {
                throw new FbcException.NotValidException("Encrypted messages cannot be attached to transactions with no recipient");
            }
        }

    }

