/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.transaction.ColoredCoinsTr;
import io.firstbridge.fbc.transaction.TransactionType;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public final class ColoredCoinsBidOrderCancellation extends ColoredCoinsOrderCancellation {
    
    public ColoredCoinsBidOrderCancellation(ByteBuffer buffer) {
        super(buffer);
    }

    public ColoredCoinsBidOrderCancellation(JSONObject attachmentData) {
        super(attachmentData);
    }

    public ColoredCoinsBidOrderCancellation(long orderId) {
        super(orderId);
    }

    @Override
    public TransactionType getTransactionType() {
        return ColoredCoinsTr.BID_ORDER_CANCELLATION;
    }
    
}
