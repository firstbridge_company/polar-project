/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.transaction.MessagingTr;
import io.firstbridge.fbc.transaction.TransactionType;
import io.firstbridge.fbc.util.Constants;
import io.firstbridge.fbc.crypto.util.Convert;
import io.firstbridge.fbc.util.FbcException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public final class MessagingPhasingVoteCasting extends AbstractAttachment {
    
    final List<byte[]> transactionFullHashes;
    final byte[] revealedSecret;

    public MessagingPhasingVoteCasting(ByteBuffer buffer) throws FbcException.NotValidException {
        super(buffer);
        byte length = buffer.get();
        transactionFullHashes = new ArrayList<>(length);
        for (int i = 0; i < length; i++) {
            byte[] hash = new byte[32];
            buffer.get(hash);
            transactionFullHashes.add(hash);
        }
        int secretLength = buffer.getInt();
        if (secretLength > Constants.MAX_PHASING_REVEALED_SECRET_LENGTH) {
            throw new FbcException.NotValidException("Invalid revealed secret length " + secretLength);
        }
        if (secretLength > 0) {
            revealedSecret = new byte[secretLength];
            buffer.get(revealedSecret);
        } else {
            revealedSecret = Convert.EMPTY_BYTE;
        }
    }

    public MessagingPhasingVoteCasting(JSONObject attachmentData) {
        super(attachmentData);
        JSONArray hashes = (JSONArray) attachmentData.get("transactionFullHashes");
        transactionFullHashes = new ArrayList<>(hashes.size());
        hashes.forEach((java.lang.Object hash) -> transactionFullHashes.add(Convert.parseHexString((String) hash)));
        String revealedSecret = Convert.emptyToNull((String) attachmentData.get("revealedSecret"));
        this.revealedSecret = revealedSecret != null ? Convert.parseHexString(revealedSecret) : Convert.EMPTY_BYTE;
    }

    public MessagingPhasingVoteCasting(List<byte[]> transactionFullHashes, byte[] revealedSecret) {
        this.transactionFullHashes = transactionFullHashes;
        this.revealedSecret = revealedSecret;
    }

    @Override
    public int getMySize() {
        return 1 + 32 * transactionFullHashes.size() + 4 + revealedSecret.length;
    }

    @Override
    public void putMyBytes(ByteBuffer buffer) {
        buffer.put((byte) transactionFullHashes.size());
        transactionFullHashes.forEach(buffer::put);
        buffer.putInt(revealedSecret.length);
        buffer.put(revealedSecret);
    }

    @Override
    public void putMyJSON(JSONObject attachment) {
        JSONArray jsonArray = new JSONArray();
        transactionFullHashes.forEach((byte[] hash) -> jsonArray.add(Convert.toHexString(hash)));
        attachment.put("transactionFullHashes", jsonArray);
        if (revealedSecret.length > 0) {
            attachment.put("revealedSecret", Convert.toHexString(revealedSecret));
        }
    }

    @Override
    public TransactionType getTransactionType() {
        return MessagingTr.PHASING_VOTE_CASTING;
    }

    public List<byte[]> getTransactionFullHashes() {
        return transactionFullHashes;
    }

    public byte[] getRevealedSecret() {
        return revealedSecret;
    }
    
}
