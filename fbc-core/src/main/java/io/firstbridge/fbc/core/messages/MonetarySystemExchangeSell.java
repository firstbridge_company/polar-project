/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.core.MonetarySystem;
import io.firstbridge.fbc.transaction.TransactionType;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public final class MonetarySystemExchangeSell extends MonetarySystemExchange {
    
    public MonetarySystemExchangeSell(ByteBuffer buffer) {
        super(buffer);
    }

    public MonetarySystemExchangeSell(JSONObject attachmentData) {
        super(attachmentData);
    }

    public MonetarySystemExchangeSell(long currencyId, long rateNQT, long units) {
        super(currencyId, rateNQT, units);
    }

    @Override
    public TransactionType getTransactionType() {
        return MonetarySystem.EXCHANGE_SELL;
    }
    
}
