
package io.firstbridge.fbc.core.account;

/**
 *
 * @author al
 */
public class DoubleSpendingException extends RuntimeException {
    
    DoubleSpendingException(String message, long accountId, long confirmed, long unconfirmed) {
        super(message + " account: " + Long.toUnsignedString(accountId) + " confirmed: " + confirmed + " unconfirmed: " + unconfirmed);
    }
    
}
