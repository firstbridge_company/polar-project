/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.transaction.DigitalGoodsTr;
import io.firstbridge.fbc.transaction.TransactionType;
import io.firstbridge.fbc.crypto.util.Convert;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public final class DigitalGoodsFeedback extends AbstractAttachment {
    
    final long purchaseId;

    public DigitalGoodsFeedback(ByteBuffer buffer) {
        super(buffer);
        this.purchaseId = buffer.getLong();
    }

    public DigitalGoodsFeedback(JSONObject attachmentData) {
        super(attachmentData);
        this.purchaseId = Convert.parseUnsignedLong((String) attachmentData.get("purchase"));
    }

    public DigitalGoodsFeedback(long purchaseId) {
        this.purchaseId = purchaseId;
    }

    @Override
    public int getMySize() {
        return 8;
    }

    @Override
    public void putMyBytes(ByteBuffer buffer) {
        buffer.putLong(purchaseId);
    }

    @Override
    public void putMyJSON(JSONObject attachment) {
        attachment.put("purchase", Long.toUnsignedString(purchaseId));
    }

    @Override
    public TransactionType getTransactionType() {
        return DigitalGoodsTr.FEEDBACK;
    }

    public long getPurchaseId() {
        return purchaseId;
    }
    
}
