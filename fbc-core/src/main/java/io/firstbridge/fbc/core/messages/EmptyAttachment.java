/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.messages;

import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public abstract class EmptyAttachment extends AbstractAttachment {
    
    public EmptyAttachment() {
        super(0);
    }

    @Override
    public final int getMySize() {
        return 0;
    }

    @Override
    public final void putMyBytes(ByteBuffer buffer) {
    }

    @Override
    public final void putMyJSON(JSONObject json) {
    }

    @Override
    public final boolean verifyVersion() {
        return getVersion() == 0;
    }
    
}
