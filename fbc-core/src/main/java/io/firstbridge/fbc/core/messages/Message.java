
package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.core.account.Account;
import static io.firstbridge.fbc.core.messages.Appendix.hasAppendix;
import io.firstbridge.fbc.core.interfaces.Fee;
import io.firstbridge.fbc.core.SizeBasedFee;
import io.firstbridge.fbc.core.interfaces.Transaction;
import io.firstbridge.fbc.util.Constants;
import io.firstbridge.fbc.crypto.util.Convert;
import io.firstbridge.fbc.util.FbcException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
    public class Message extends AbstractAppendix {

        private static final String appendixName = "Message";

        public static Message parse(JSONObject attachmentData) {
            if (!hasAppendix(appendixName, attachmentData)) {
                return null;
            }
            return new Message(attachmentData);
        }

        private static final Fee MESSAGE_FEE = new SizeBasedFee(0, Constants.ONE_NXT, 32) {
            @Override
            public int getSize(Transaction transaction, Appendix appendage) {
                return ((Message)appendage).getMessage().length;
            }
        };

        private final byte[] message;
        private final boolean isText;

        public Message(ByteBuffer buffer) throws FbcException.NotValidException {
            super(buffer);
            int messageLength = buffer.getInt();
            this.isText = messageLength < 0; // ugly hack
            if (messageLength < 0) {
                messageLength &= Integer.MAX_VALUE;
            }
            if (messageLength > 1000) {
                throw new FbcException.NotValidException("Invalid arbitrary message length: " + messageLength);
            }
            this.message = new byte[messageLength];
            buffer.get(this.message);
            if (isText && !Arrays.equals(message, Convert.toBytes(Convert.toString(message)))) {
                throw new FbcException.NotValidException("Message is not UTF-8 text");
            }
        }

        private Message(JSONObject attachmentData) {
            super(attachmentData);
            String messageString = (String)attachmentData.get("message");
            this.isText = Boolean.TRUE.equals(attachmentData.get("messageIsText"));
            this.message = isText ? Convert.toBytes(messageString) : Convert.parseHexString(messageString);
        }

        public Message(byte[] message) {
            this(message, false);
        }

        public Message(String string) {
            this(Convert.toBytes(string), true);
        }

        public Message(String string, boolean isText) {
            this(isText ? Convert.toBytes(string) : Convert.parseHexString(string), isText);
        }

        public Message(byte[] message, boolean isText) {
            this.message = message;
            this.isText = isText;
        }

        @Override
        public String getAppendixName() {
            return appendixName;
        }

        @Override
        public int getMySize() {
            return 4 + message.length;
        }

        @Override
        public void putMyBytes(ByteBuffer buffer) {
            buffer.putInt(isText ? (message.length | Integer.MIN_VALUE) : message.length);
            buffer.put(message);
        }

        @Override
        public void putMyJSON(JSONObject json) {
            json.put("message", Convert.toString(message, isText));
            json.put("messageIsText", isText);
        }

        @Override
        public Fee getBaselineFee(Transaction transaction) {
            return MESSAGE_FEE;
        }

        @Override
        public void validate(Transaction transaction) throws FbcException.ValidationException {
            if (message.length > Constants.MAX_ARBITRARY_MESSAGE_LENGTH) {
                throw new FbcException.NotValidException("Invalid arbitrary message length: " + message.length);
            }
        }

        @Override
        public void apply(Transaction transaction, Account senderAccount, Account recipientAccount) {}

        public byte[] getMessage() {
            return message;
        }

        public boolean isText() {
            return isText;
        }

        @Override
        public boolean isPhasable() {
            return false;
        }

    }

