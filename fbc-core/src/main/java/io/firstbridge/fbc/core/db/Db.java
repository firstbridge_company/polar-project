/*
 * Copyright © 2013-2016 The Nxt Core Developers.
 * Copyright © 2016-2018 Jelurida IP B.V.
 *
 * See the LICENSE.txt file at the top-level directory of this distribution
 * for licensing information.
 *
 * Unless otherwise agreed in a custom licensing agreement with Jelurida B.V.,
 * no part of the Nxt software, including this file, may be copied, modified,
 * propagated, or distributed except according to the terms contained in the
 * LICENSE.txt file.
 *
 * Removal or modification of this copyright notice is prohibited.
 *
 */

package io.firstbridge.fbc.db;

import io.firstbridge.fbc.util.Constants;
import io.firstbridge.fbc.util.PropertyHelper;

public final class Db {

    public static final String PREFIX = Constants.isTestnet ? "nxt.testDb" : "nxt.db";
    private static TransactionalDb db=null;

    public static void init() {
      if(db==null){  
         db = new TransactionalDb(new DbProperties()
            .maxCacheSize(PropertyHelper.getIntProperty("nxt.dbCacheKB"))
            .dbUrl(PropertyHelper.getStringProperty(PREFIX + "Url"))
            .dbType(PropertyHelper.getStringProperty(PREFIX + "Type"))
            .dbDir(PropertyHelper.getStringProperty(PREFIX + "Dir"))
            .dbParams(PropertyHelper.getStringProperty(PREFIX + "Params"))
            .dbUsername(PropertyHelper.getStringProperty(PREFIX + "Username"))
            .dbPassword(PropertyHelper.getStringProperty(PREFIX + "Password", null, true))
            .maxConnections(PropertyHelper.getIntProperty("nxt.maxDbConnections"))
            .loginTimeout(PropertyHelper.getIntProperty("nxt.dbLoginTimeout"))
            .defaultLockTimeout(PropertyHelper.getIntProperty("nxt.dbDefaultLockTimeout") * 1000)
            .maxMemoryRows(PropertyHelper.getIntProperty("nxt.dbMaxMemoryRows"))
         );        
         db.init(new NxtDbVersion());
      }
    }

    public static void shutdown() {
        db.shutdown();
    }
    
    public static TransactionalDb getDb(){
        init();
        return db;    
    }
    private Db() {} // never

}
