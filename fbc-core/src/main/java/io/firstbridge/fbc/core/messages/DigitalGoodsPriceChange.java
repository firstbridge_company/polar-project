/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.transaction.DigitalGoodsTr;
import io.firstbridge.fbc.transaction.TransactionType;
import io.firstbridge.fbc.crypto.util.Convert;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public final class DigitalGoodsPriceChange extends AbstractAttachment {
    
    final long goodsId;
    final long priceNQT;

    public DigitalGoodsPriceChange(ByteBuffer buffer) {
        super(buffer);
        this.goodsId = buffer.getLong();
        this.priceNQT = buffer.getLong();
    }

    public DigitalGoodsPriceChange(JSONObject attachmentData) {
        super(attachmentData);
        this.goodsId = Convert.parseUnsignedLong((String) attachmentData.get("goods"));
        this.priceNQT = Convert.parseLong(attachmentData.get("priceNQT"));
    }

    public DigitalGoodsPriceChange(long goodsId, long priceNQT) {
        this.goodsId = goodsId;
        this.priceNQT = priceNQT;
    }

    @Override
    public int getMySize() {
        return 8 + 8;
    }

    @Override
    public void putMyBytes(ByteBuffer buffer) {
        buffer.putLong(goodsId);
        buffer.putLong(priceNQT);
    }

    @Override
    public void putMyJSON(JSONObject attachment) {
        attachment.put("goods", Long.toUnsignedString(goodsId));
        attachment.put("priceNQT", priceNQT);
    }

    @Override
    public TransactionType getTransactionType() {
        return DigitalGoodsTr.PRICE_CHANGE;
    }

    public long getGoodsId() {
        return goodsId;
    }

    public long getPriceNQT() {
        return priceNQT;
    }
    
}
