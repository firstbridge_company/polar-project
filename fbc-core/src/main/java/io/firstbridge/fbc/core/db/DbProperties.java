/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.db;

/**
 *
 * @author al
 */
public final class DbProperties {
    
    long maxCacheSize;
    String dbUrl;
    String dbType;
    String dbDir;
    String dbParams;
    String dbUsername;
    String dbPassword;
    int maxConnections;
    int loginTimeout;
    int defaultLockTimeout;
    int maxMemoryRows;

    public DbProperties maxCacheSize(int maxCacheSize) {
        this.maxCacheSize = maxCacheSize;
        return this;
    }

    public DbProperties dbUrl(String dbUrl) {
        this.dbUrl = dbUrl;
        return this;
    }

    public DbProperties dbType(String dbType) {
        this.dbType = dbType;
        return this;
    }

    public DbProperties dbDir(String dbDir) {
        this.dbDir = dbDir;
        return this;
    }

    public DbProperties dbParams(String dbParams) {
        this.dbParams = dbParams;
        return this;
    }

    public DbProperties dbUsername(String dbUsername) {
        this.dbUsername = dbUsername;
        return this;
    }

    public DbProperties dbPassword(String dbPassword) {
        this.dbPassword = dbPassword;
        return this;
    }

    public DbProperties maxConnections(int maxConnections) {
        this.maxConnections = maxConnections;
        return this;
    }

    public DbProperties loginTimeout(int loginTimeout) {
        this.loginTimeout = loginTimeout;
        return this;
    }

    public DbProperties defaultLockTimeout(int defaultLockTimeout) {
        this.defaultLockTimeout = defaultLockTimeout;
        return this;
    }

    public DbProperties maxMemoryRows(int maxMemoryRows) {
        this.maxMemoryRows = maxMemoryRows;
        return this;
    }
    
}
