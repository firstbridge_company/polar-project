/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.core.MonetarySystem;
import io.firstbridge.fbc.transaction.TransactionType;
import io.firstbridge.fbc.crypto.util.Convert;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public final class MonetarySystemReserveIncrease extends AbstractAttachment implements Attachment.MonetarySystemAttachment {
    
    final long currencyId;
    final long amountPerUnitNQT;

    public MonetarySystemReserveIncrease(ByteBuffer buffer) {
        super(buffer);
        this.currencyId = buffer.getLong();
        this.amountPerUnitNQT = buffer.getLong();
    }

    public MonetarySystemReserveIncrease(JSONObject attachmentData) {
        super(attachmentData);
        this.currencyId = Convert.parseUnsignedLong((String) attachmentData.get("currency"));
        this.amountPerUnitNQT = Convert.parseLong(attachmentData.get("amountPerUnitNQT"));
    }

    public MonetarySystemReserveIncrease(long currencyId, long amountPerUnitNQT) {
        this.currencyId = currencyId;
        this.amountPerUnitNQT = amountPerUnitNQT;
    }

    @Override
    public int getMySize() {
        return 8 + 8;
    }

    @Override
    public void putMyBytes(ByteBuffer buffer) {
        buffer.putLong(currencyId);
        buffer.putLong(amountPerUnitNQT);
    }

    @Override
    public void putMyJSON(JSONObject attachment) {
        attachment.put("currency", Long.toUnsignedString(currencyId));
        attachment.put("amountPerUnitNQT", amountPerUnitNQT);
    }

    @Override
    public TransactionType getTransactionType() {
        return MonetarySystem.RESERVE_INCREASE;
    }

    @Override
    public long getCurrencyId() {
        return currencyId;
    }

    public long getAmountPerUnitNQT() {
        return amountPerUnitNQT;
    }
    
}
