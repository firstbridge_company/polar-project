/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.messages;

import io.firstbridge.cryptolib.CryptoNotValidException;
import io.firstbridge.fbc.transaction.TransactionType;
import io.firstbridge.fbc.crypto.EncryptedData;
import io.firstbridge.fbc.transaction.DigitalGoodsTr;
import io.firstbridge.fbc.util.Constants;
import io.firstbridge.fbc.crypto.util.Convert;
import io.firstbridge.fbc.util.FbcException;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public class DigitalGoodsDelivery extends AbstractAttachment {
    
    final long purchaseId;
    EncryptedData goods;
    final long discountNQT;
    final boolean goodsIsText;

    public DigitalGoodsDelivery(ByteBuffer buffer) throws FbcException.NotValidException {
        super(buffer);
        this.purchaseId = buffer.getLong();
        int length = buffer.getInt();
        goodsIsText = length < 0;
        if (length < 0) {
            length &= Integer.MAX_VALUE;
        }
        try {
            this.goods = EncryptedData.readEncryptedData(buffer, length, Constants.MAX_DGS_GOODS_LENGTH);
            this.discountNQT = buffer.getLong();
        } catch (CryptoNotValidException ex) {
            throw new FbcException.NotValidException(ex.getMessage());
        }
    }

    public DigitalGoodsDelivery(JSONObject attachmentData) {
        super(attachmentData);
        this.purchaseId = Convert.parseUnsignedLong((String) attachmentData.get("purchase"));
        this.goods = new EncryptedData(Convert.parseHexString((String) attachmentData.get("goodsData")), Convert.parseHexString((String) attachmentData.get("goodsNonce")));
        this.discountNQT = Convert.parseLong(attachmentData.get("discountNQT"));
        this.goodsIsText = Boolean.TRUE.equals(attachmentData.get("goodsIsText"));
    }

    public DigitalGoodsDelivery(long purchaseId, EncryptedData goods, boolean goodsIsText, long discountNQT) {
        this.purchaseId = purchaseId;
        this.goods = goods;
        this.discountNQT = discountNQT;
        this.goodsIsText = goodsIsText;
    }

    @Override
    public int getMySize() {
        return 8 + 4 + goods.getSize() + 8;
    }

    @Override
    public void putMyBytes(ByteBuffer buffer) {
        buffer.putLong(purchaseId);
        buffer.putInt(goodsIsText ? goods.getData().length | Integer.MIN_VALUE : goods.getData().length);
        buffer.put(goods.getData());
        buffer.put(goods.getNonce());
        buffer.putLong(discountNQT);
    }

    @Override
    public void putMyJSON(JSONObject attachment) {
        attachment.put("purchase", Long.toUnsignedString(purchaseId));
        attachment.put("goodsData", Convert.toHexString(goods.getData()));
        attachment.put("goodsNonce", Convert.toHexString(goods.getNonce()));
        attachment.put("discountNQT", discountNQT);
        attachment.put("goodsIsText", goodsIsText);
    }

    @Override
    public final TransactionType getTransactionType() {
        return DigitalGoodsTr.DELIVERY;
    }

    public final long getPurchaseId() {
        return purchaseId;
    }

    public final EncryptedData getGoods() {
        return goods;
    }

    public final void setGoods(EncryptedData goods) {
        this.goods = goods;
    }

    public int getGoodsDataLength() {
        return goods.getData().length;
    }

    public final long getDiscountNQT() {
        return discountNQT;
    }

    public final boolean goodsIsText() {
        return goodsIsText;
    }
    
}
