/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.messages;

import io.firstbridge.cryptolib.CryptoNotValidException;
import io.firstbridge.fbc.transaction.MessagingTr;
import io.firstbridge.fbc.transaction.TransactionType;
import io.firstbridge.fbc.util.Constants;
import io.firstbridge.fbc.crypto.util.Convert;
import io.firstbridge.fbc.util.FbcException;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public final class MessagingAliasSell extends AbstractAttachment {
    
    final String aliasName;
    final long priceNQT;

    public MessagingAliasSell(ByteBuffer buffer) throws FbcException.NotValidException {
        super(buffer);
        try {
            this.aliasName = Convert.readString(buffer, buffer.get(), Constants.MAX_ALIAS_LENGTH);
        } catch (CryptoNotValidException ex) {
            throw new FbcException.NotValidException(ex.getMessage());
        }
        this.priceNQT = buffer.getLong();
    }

    public MessagingAliasSell(JSONObject attachmentData) {
        super(attachmentData);
        this.aliasName = Convert.nullToEmpty((String) attachmentData.get("alias"));
        this.priceNQT = Convert.parseLong(attachmentData.get("priceNQT"));
    }

    public MessagingAliasSell(String aliasName, long priceNQT) {
        this.aliasName = aliasName;
        this.priceNQT = priceNQT;
    }

    @Override
    public TransactionType getTransactionType() {
        return MessagingTr.ALIAS_SELL;
    }

    @Override
    public int getMySize() {
        return 1 + Convert.toBytes(aliasName).length + 8;
    }

    @Override
    public void putMyBytes(ByteBuffer buffer) {
        byte[] aliasBytes = Convert.toBytes(aliasName);
        buffer.put((byte) aliasBytes.length);
        buffer.put(aliasBytes);
        buffer.putLong(priceNQT);
    }

    @Override
    public void putMyJSON(JSONObject attachment) {
        attachment.put("alias", aliasName);
        attachment.put("priceNQT", priceNQT);
    }

    public String getAliasName() {
        return aliasName;
    }

    public long getPriceNQT() {
        return priceNQT;
    }
    
}
