package io.firstbridge.fbc.core.interfaces;

/**
 *
 * @author al
 */
public class TransactionNotAcceptedException extends BlockNotAcceptedException {

        private final Transaction transaction;

        public TransactionNotAcceptedException(String message, Transaction transaction) {
            super(message, transaction.getBlock());
            this.transaction = transaction;
        }

        public TransactionNotAcceptedException(Throwable cause, Transaction transaction) {
            super(cause, transaction.getBlock());
            this.transaction = transaction;
        }

        public Transaction getTransaction() {
            return transaction;
        }

        @Override
        public String getMessage() {
            return super.getMessage() + ", transaction " + transaction.getStringId() + " " + transaction.getJSONObject().toJSONString();
        }
    }
