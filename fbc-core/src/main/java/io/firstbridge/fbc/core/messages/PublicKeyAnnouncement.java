
package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.core.account.Account;
import static io.firstbridge.fbc.core.messages.Appendix.hasAppendix;
import io.firstbridge.fbc.core.interfaces.Transaction;
import io.firstbridge.fbc.crypto.Crypto;
import io.firstbridge.fbc.crypto.util.Convert;
import io.firstbridge.fbc.util.FbcException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public  final class PublicKeyAnnouncement extends AbstractAppendix {

        private static final String appendixName = "PublicKeyAnnouncement";

        public static PublicKeyAnnouncement parse(JSONObject attachmentData) {
            if (!hasAppendix(appendixName, attachmentData)) {
                return null;
            }
            return new PublicKeyAnnouncement(attachmentData);
        }

        private final byte[] publicKey;

        public PublicKeyAnnouncement(ByteBuffer buffer) {
            super(buffer);
            this.publicKey = new byte[32];
            buffer.get(this.publicKey);
        }

        private PublicKeyAnnouncement(JSONObject attachmentData) {
            super(attachmentData);
            this.publicKey = Convert.parseHexString((String)attachmentData.get("recipientPublicKey"));
        }

        public PublicKeyAnnouncement(byte[] publicKey) {
            this.publicKey = publicKey;
        }

        @Override
        public String getAppendixName() {
            return appendixName;
        }

        @Override
        public int getMySize() {
            return 32;
        }

        @Override
        public void putMyBytes(ByteBuffer buffer) {
            buffer.put(publicKey);
        }

        @Override
        public void putMyJSON(JSONObject json) {
            json.put("recipientPublicKey", Convert.toHexString(publicKey));
        }

        @Override
        public void validate(Transaction transaction) throws FbcException.ValidationException {
            if (transaction.getRecipientId() == 0) {
                throw new FbcException.NotValidException("PublicKeyAnnouncement cannot be attached to transactions with no recipient");
            }
            if (!Crypto.isCanonicalPublicKey(publicKey)) {
                throw new FbcException.NotValidException("Invalid recipient public key: " + Convert.toHexString(publicKey));
            }
            long recipientId = transaction.getRecipientId();
            if (Account.getId(this.publicKey) != recipientId) {
                throw new FbcException.NotValidException("Announced public key does not match recipient accountId");
            }
            byte[] recipientPublicKey = Account.getPublicKey(recipientId);
            if (recipientPublicKey != null && ! Arrays.equals(publicKey, recipientPublicKey)) {
                throw new FbcException.NotCurrentlyValidException("A different public key for this account has already been announced");
            }
        }

        @Override
        public void apply(Transaction transaction, Account senderAccount, Account recipientAccount) {
            if (Account.setOrVerify(recipientAccount.getId(), publicKey)) {
                recipientAccount.apply(this.publicKey);
            }
        }

        @Override
        public boolean isPhasable() {
            return false;
        }

        public byte[] getPublicKey() {
            return publicKey;
        }

    }

