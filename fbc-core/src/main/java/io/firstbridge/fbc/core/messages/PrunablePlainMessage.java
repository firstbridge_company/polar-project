package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.core.DiH;
import io.firstbridge.fbc.core.account.Account;
import io.firstbridge.fbc.core.PrunableMessage;
import io.firstbridge.fbc.transaction.TransactionImpl;
import static io.firstbridge.fbc.core.messages.Appendix.hasAppendix;
import io.firstbridge.fbc.core.interfaces.Fee;
import io.firstbridge.fbc.core.SizeBasedFee;
import io.firstbridge.fbc.core.interfaces.Transaction;
import io.firstbridge.fbc.crypto.Crypto;
import io.firstbridge.fbc.util.Constants;
import io.firstbridge.fbc.crypto.util.Convert;
import io.firstbridge.fbc.util.FbcException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
    public class PrunablePlainMessage extends AbstractAppendix implements Appendix.Prunable {

        private static final String appendixName = "PrunablePlainMessage";

        private static final Fee PRUNABLE_MESSAGE_FEE = new SizeBasedFee(Constants.ONE_NXT/10) {
            @Override
            public int getSize(Transaction transaction, Appendix appendix) {
                return appendix.getFullSize();
            }
        };

        public static PrunablePlainMessage parse(JSONObject attachmentData) {
            if (!hasAppendix(appendixName, attachmentData)) {
                return null;
            }
            return new PrunablePlainMessage(attachmentData);
        }

        private final byte[] hash;
        private final byte[] message;
        private final boolean isText;
        private volatile PrunableMessage prunableMessage;

        public PrunablePlainMessage(ByteBuffer buffer) {
            super(buffer);
            this.hash = new byte[32];
            buffer.get(this.hash);
            this.message = null;
            this.isText = false;
        }

        private PrunablePlainMessage(JSONObject attachmentData) {
            super(attachmentData);
            String hashString = Convert.emptyToNull((String) attachmentData.get("messageHash"));
            String messageString = Convert.emptyToNull((String) attachmentData.get("message"));
            if (hashString != null && messageString == null) {
                this.hash = Convert.parseHexString(hashString);
                this.message = null;
                this.isText = false;
            } else {
                this.hash = null;
                this.isText = Boolean.TRUE.equals(attachmentData.get("messageIsText"));
                this.message = Convert.toBytes(messageString, isText);
            }
        }

        public PrunablePlainMessage(byte[] message) {
            this(message, false);
        }

        public PrunablePlainMessage(String string) {
            this(Convert.toBytes(string), true);
        }

        public PrunablePlainMessage(String string, boolean isText) {
            this(Convert.toBytes(string, isText), isText);
        }

        public PrunablePlainMessage(byte[] message, boolean isText) {
            this.message = message;
            this.isText = isText;
            this.hash = null;
        }

        @Override
        public String getAppendixName() {
            return appendixName;
        }

        @Override
        public Fee getBaselineFee(Transaction transaction) {
            return PRUNABLE_MESSAGE_FEE;
        }

        @Override
        public int getMySize() {
            return 32;
        }

        @Override
        public int getMyFullSize() {
            return getMessage() == null ? 0 : getMessage().length;
        }

        @Override
        public void putMyBytes(ByteBuffer buffer) {
            buffer.put(getHash());
        }

        @Override
        public void putMyJSON(JSONObject json) {
            if (prunableMessage != null) {
                json.put("message", Convert.toString(prunableMessage.getMessage(), prunableMessage.messageIsText()));
                json.put("messageIsText", prunableMessage.messageIsText());
            } else if (message != null) {
                json.put("message", Convert.toString(message, isText));
                json.put("messageIsText", isText);
            }
            json.put("messageHash", Convert.toHexString(getHash()));
        }

        @Override
        public void validate(Transaction transaction) throws FbcException.ValidationException {
            if (transaction.getMessage() != null) {
                throw new FbcException.NotValidException("Cannot have both message and prunable message attachments");
            }
            byte[] msg = getMessage();
            if (msg != null && msg.length > Constants.MAX_PRUNABLE_MESSAGE_LENGTH) {
                throw new FbcException.NotValidException("Invalid prunable message length: " + msg.length);
            }
            if (msg == null && DiH.getEpochTime() - transaction.getTimestamp() < Constants.MIN_PRUNABLE_LIFETIME) {
                throw new FbcException.NotCurrentlyValidException("Message has been pruned prematurely");
            }
        }

        @Override
        public void apply(Transaction transaction, Account senderAccount, Account recipientAccount) {
            if (DiH.getEpochTime() - transaction.getTimestamp() < Constants.MAX_PRUNABLE_LIFETIME) {
                PrunableMessage.add((TransactionImpl)transaction, this);
            }
        }

        public byte[] getMessage() {
            if (prunableMessage != null) {
                return prunableMessage.getMessage();
            }
            return message;
        }

        public boolean isText() {
            if (prunableMessage != null) {
                return prunableMessage.messageIsText();
            }
            return isText;
        }

        @Override
        public byte[] getHash() {
            if (hash != null) {
                return hash;
            }
            MessageDigest digest = Crypto.sha256();
            digest.update((byte)(isText ? 1 : 0));
            digest.update(message);
            return digest.digest();
        }

        @Override
        public final void loadPrunable(Transaction transaction, boolean includeExpiredPrunable) {
            if (!hasPrunableData() && shouldLoadPrunable(transaction, includeExpiredPrunable)) {
                PrunableMessage prunableMessage = PrunableMessage.getPrunableMessage(transaction.getId());
                if (prunableMessage != null && prunableMessage.getMessage() != null) {
                    this.prunableMessage = prunableMessage;
                }
            }
        }

        @Override
        public boolean isPhasable() {
            return false;
        }

        @Override
        public final boolean hasPrunableData() {
            return (prunableMessage != null || message != null);
        }

        @Override
        public void restorePrunableData(Transaction transaction, int blockTimestamp, int height) {
            PrunableMessage.add((TransactionImpl)transaction, this, blockTimestamp, height);
        }
    }

