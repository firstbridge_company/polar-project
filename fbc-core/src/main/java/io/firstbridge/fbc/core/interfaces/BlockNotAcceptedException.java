package io.firstbridge.fbc.core.interfaces;

import io.firstbridge.fbc.util.FbcException;

/**
 *
 * @author al
 */
public     class BlockNotAcceptedException extends FbcException {

        private final Block block;

        public BlockNotAcceptedException(String message, Block block) {
            super(message);
            this.block = block;
        }

        public BlockNotAcceptedException(Throwable cause, Block block) {
            super(cause);
            this.block = block;
        }

        @Override
        public String getMessage() {
            return block == null ? super.getMessage() : super.getMessage() + ", block " + block.getStringId() + " " + block.getJSONObject().toJSONString();
        }

    }
