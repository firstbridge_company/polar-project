/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.transaction.ColoredCoinsTr;
import io.firstbridge.fbc.transaction.TransactionType;
import io.firstbridge.fbc.crypto.util.Convert;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public final class ColoredCoinsDividendPayment extends AbstractAttachment {
    
    final long assetId;
    final int height;
    final long amountNQTPerQNT;

    public ColoredCoinsDividendPayment(ByteBuffer buffer) {
        super(buffer);
        this.assetId = buffer.getLong();
        this.height = buffer.getInt();
        this.amountNQTPerQNT = buffer.getLong();
    }

    public ColoredCoinsDividendPayment(JSONObject attachmentData) {
        super(attachmentData);
        this.assetId = Convert.parseUnsignedLong((String) attachmentData.get("asset"));
        this.height = ((Long) attachmentData.get("height")).intValue();
        this.amountNQTPerQNT = Convert.parseLong(attachmentData.get("amountNQTPerQNT"));
    }

    public ColoredCoinsDividendPayment(long assetId, int height, long amountNQTPerQNT) {
        this.assetId = assetId;
        this.height = height;
        this.amountNQTPerQNT = amountNQTPerQNT;
    }

    @Override
    public int getMySize() {
        return 8 + 4 + 8;
    }

    @Override
    public void putMyBytes(ByteBuffer buffer) {
        buffer.putLong(assetId);
        buffer.putInt(height);
        buffer.putLong(amountNQTPerQNT);
    }

    @Override
    public void putMyJSON(JSONObject attachment) {
        attachment.put("asset", Long.toUnsignedString(assetId));
        attachment.put("height", height);
        attachment.put("amountNQTPerQNT", amountNQTPerQNT);
    }

    @Override
    public TransactionType getTransactionType() {
        return ColoredCoinsTr.DIVIDEND_PAYMENT;
    }

    public long getAssetId() {
        return assetId;
    }

    public int getHeight() {
        return height;
    }

    public long getAmountNQTPerQNT() {
        return amountNQTPerQNT;
    }
    
}
