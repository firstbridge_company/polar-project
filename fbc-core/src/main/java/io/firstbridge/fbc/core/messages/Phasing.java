
package io.firstbridge.fbc.core.messages;


import io.firstbridge.fbc.core.DiH;
import io.firstbridge.fbc.core.account.Account;
import io.firstbridge.fbc.core.account.AccountLedger;
import io.firstbridge.fbc.core.PhasingParams;
import io.firstbridge.fbc.core.PhasingPoll;
import io.firstbridge.fbc.core.TransactionDb;
import io.firstbridge.fbc.transaction.TransactionImpl;
import io.firstbridge.fbc.transaction.TransactionProcessorImpl;
import io.firstbridge.fbc.transaction.TransactionType;
import io.firstbridge.fbc.core.VoteWeighting;
import static io.firstbridge.fbc.core.messages.Appendix.hasAppendix;
import io.firstbridge.fbc.core.interfaces.Fee;
import io.firstbridge.fbc.core.interfaces.Transaction;
import io.firstbridge.fbc.core.interfaces.TransactionProcessor;
import io.firstbridge.fbc.util.Constants;
import io.firstbridge.fbc.crypto.util.Convert;
import io.firstbridge.fbc.util.FbcException;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author al
 */
public final class Phasing extends AbstractAppendix {
   static final Logger logger = LoggerFactory.getLogger(Phasing.class);

        private static final String appendixName = "Phasing";

        private static final Fee PHASING_FEE = (transaction, appendage) -> {
            long fee = 0;
            Phasing phasing = (Phasing)appendage;
            if (!phasing.params.getVoteWeighting().isBalanceIndependent()) {
                fee += 20 * Constants.ONE_NXT;
            } else {
                fee += Constants.ONE_NXT;
            }
            if (phasing.hashedSecret.length > 0) {
                fee += (1 + (phasing.hashedSecret.length - 1) / 32) * Constants.ONE_NXT;
            }
            fee += Constants.ONE_NXT * phasing.linkedFullHashes.length;
            return fee;
        };

        public static Phasing parse(JSONObject attachmentData) {
            if (!hasAppendix(appendixName, attachmentData)) {
                return null;
            }
            return new Phasing(attachmentData);
        }

        private final int finishHeight;
        private final PhasingParams params;
        private final byte[][] linkedFullHashes;
        private final byte[] hashedSecret;
        private final byte algorithm;

        public Phasing(ByteBuffer buffer) {
            super(buffer);
            finishHeight = buffer.getInt();
            params = new PhasingParams(buffer);
            
            byte linkedFullHashesSize = buffer.get();
            if (linkedFullHashesSize > 0) {
                linkedFullHashes = new byte[linkedFullHashesSize][];
                for (int i = 0; i < linkedFullHashesSize; i++) {
                    linkedFullHashes[i] = new byte[32];
                    buffer.get(linkedFullHashes[i]);
                }
            } else {
                linkedFullHashes = Convert.EMPTY_BYTES;
            }
            byte hashedSecretLength = buffer.get();
            if (hashedSecretLength > 0) {
                hashedSecret = new byte[hashedSecretLength];
                buffer.get(hashedSecret);
            } else {
                hashedSecret = Convert.EMPTY_BYTE;
            }
            algorithm = buffer.get();
        }

        private Phasing(JSONObject attachmentData) {
            super(attachmentData);
            finishHeight = ((Long) attachmentData.get("phasingFinishHeight")).intValue();
            params = new PhasingParams(attachmentData);
            JSONArray linkedFullHashesJson = (JSONArray) attachmentData.get("phasingLinkedFullHashes");
            if (linkedFullHashesJson != null && linkedFullHashesJson.size() > 0) {
                linkedFullHashes = new byte[linkedFullHashesJson.size()][];
                for (int i = 0; i < linkedFullHashes.length; i++) {
                    linkedFullHashes[i] = Convert.parseHexString((String) linkedFullHashesJson.get(i));
                }
            } else {
                linkedFullHashes = Convert.EMPTY_BYTES;
            }
            String hashedSecret = Convert.emptyToNull((String)attachmentData.get("phasingHashedSecret"));
            if (hashedSecret != null) {
                this.hashedSecret = Convert.parseHexString(hashedSecret);
                this.algorithm = ((Long) attachmentData.get("phasingHashedSecretAlgorithm")).byteValue();
            } else {
                this.hashedSecret = Convert.EMPTY_BYTE;
                this.algorithm = 0;
            }
        }

        public Phasing(int finishHeight, PhasingParams phasingParams, byte[][] linkedFullHashes, byte[] hashedSecret, byte algorithm) {
            this.finishHeight = finishHeight;
            this.params = phasingParams;
            this.linkedFullHashes = Convert.nullToEmpty(linkedFullHashes);
            this.hashedSecret = hashedSecret != null ? hashedSecret : Convert.EMPTY_BYTE;
            this.algorithm = algorithm;
        }

        @Override
        public String getAppendixName() {
            return appendixName;
        }

        @Override
        public int getMySize() {
            return 4 + params.getMySize() + 1 + 32 * linkedFullHashes.length + 1 + hashedSecret.length + 1;
        }

        @Override
        public void putMyBytes(ByteBuffer buffer) {
            buffer.putInt(finishHeight);
            params.putMyBytes(buffer);
            buffer.put((byte) linkedFullHashes.length);
            for (byte[] hash : linkedFullHashes) {
                buffer.put(hash);
            }
            buffer.put((byte)hashedSecret.length);
            buffer.put(hashedSecret);
            buffer.put(algorithm);
        }

        @Override
        public void putMyJSON(JSONObject json) {
            json.put("phasingFinishHeight", finishHeight);
            params.putMyJSON(json);
            if (linkedFullHashes.length > 0) {
                JSONArray linkedFullHashesJson = new JSONArray();
                for (byte[] hash : linkedFullHashes) {
                    linkedFullHashesJson.add(Convert.toHexString(hash));
                }
                json.put("phasingLinkedFullHashes", linkedFullHashesJson);
            }
            if (hashedSecret.length > 0) {
                json.put("phasingHashedSecret", Convert.toHexString(hashedSecret));
                json.put("phasingHashedSecretAlgorithm", algorithm);
            }
        }

        @Override
        public void validate(Transaction transaction) throws FbcException.ValidationException {
            params.validate();
            int currentHeight = DiH.getBlockchain().getHeight();
            if (params.getVoteWeighting().getVotingModel() == VoteWeighting.VotingModel.TRANSACTION) {
                if (linkedFullHashes.length == 0 || linkedFullHashes.length > Constants.MAX_PHASING_LINKED_TRANSACTIONS) {
                    throw new FbcException.NotValidException("Invalid number of linkedFullHashes " + linkedFullHashes.length);
                }
                Set<Long> linkedTransactionIds = new HashSet<>(linkedFullHashes.length);
                for (byte[] hash : linkedFullHashes) {
                    if (Convert.emptyToNull(hash) == null || hash.length != 32) {
                        throw new FbcException.NotValidException("Invalid linkedFullHash " + Convert.toHexString(hash));
                    }
                    if (!linkedTransactionIds.add(Convert.fullHashToId(hash))) {
                        throw new FbcException.NotValidException("Duplicate linked transaction ids");
                    }
                    TransactionImpl linkedTransaction = TransactionDb.findTransactionByFullHash(hash, currentHeight);
                    if (linkedTransaction != null) {
                        if (transaction.getTimestamp() - linkedTransaction.getTimestamp() > Constants.MAX_REFERENCED_TRANSACTION_TIMESPAN) {
                            throw new FbcException.NotValidException("Linked transaction cannot be more than 60 days older than the phased transaction");
                        }
                        if (linkedTransaction.getPhasing() != null) {
                            throw new FbcException.NotCurrentlyValidException("Cannot link to an already existing phased transaction");
                        }
                    }
                }
                if (params.getQuorum() > linkedFullHashes.length) {
                    throw new FbcException.NotValidException("Quorum of " + params.getQuorum() + " cannot be achieved in by-transaction voting with "
                            + linkedFullHashes.length + " linked full hashes only");
                }
            } else {
                if (linkedFullHashes.length != 0) {
                    throw new FbcException.NotValidException("LinkedFullHashes can only be used with VotingModel.TRANSACTION");
                }
            }

            if (params.getVoteWeighting().getVotingModel() == VoteWeighting.VotingModel.HASH) {
                if (params.getQuorum() != 1) {
                    throw new FbcException.NotValidException("Quorum must be 1 for by-hash voting");
                }
                if (hashedSecret.length == 0 || hashedSecret.length > Byte.MAX_VALUE) {
                    throw new FbcException.NotValidException("Invalid hashedSecret " + Convert.toHexString(hashedSecret));
                }
                if (PhasingPoll.getHashFunction(algorithm) == null) {
                    throw new FbcException.NotValidException("Invalid hashedSecretAlgorithm " + algorithm);
                }
            } else {
                if (hashedSecret.length != 0) {
                    throw new FbcException.NotValidException("HashedSecret can only be used with VotingModel.HASH");
                }
                if (algorithm != 0) {
                    throw new FbcException.NotValidException("HashedSecretAlgorithm can only be used with VotingModel.HASH");
                }
            }

            if (finishHeight <= currentHeight + (params.getVoteWeighting().acceptsVotes() ? 2 : 1)
                    || finishHeight >= currentHeight + Constants.MAX_PHASING_DURATION) {
                throw new FbcException.NotCurrentlyValidException("Invalid finish height " + finishHeight);
            }
        }

        @Override
        public void validateAtFinish(Transaction transaction) throws FbcException.ValidationException {
            params.checkApprovable();
        }

        @Override
        public void apply(Transaction transaction, Account senderAccount, Account recipientAccount) {
            PhasingPoll.addPoll(transaction, this);
        }

        @Override
        public boolean isPhasable() {
            return false;
        }

        @Override
        public Fee getBaselineFee(Transaction transaction) {
            return PHASING_FEE;
        }

        private void release(TransactionImpl transaction) {
            Account senderAccount = Account.getAccount(transaction.getSenderId());
            Account recipientAccount = transaction.getRecipientId() == 0 ? null : Account.getAccount(transaction.getRecipientId());
            transaction.getAppendages().forEach(appendage -> {
                if (appendage.isPhasable()) {
                    appendage.apply(transaction, senderAccount, recipientAccount);
                }
            });
            TransactionProcessorImpl.getInstance().notifyListeners(Collections.singletonList(transaction), TransactionProcessor.Event.RELEASE_PHASED_TRANSACTION);
            logger.debug("Transaction " + transaction.getStringId() + " has been released");
        }

        public void reject(TransactionImpl transaction) {
            Account senderAccount = Account.getAccount(transaction.getSenderId());
            transaction.getType().undoAttachmentUnconfirmed(transaction, senderAccount);
            senderAccount.addToUnconfirmedBalanceNQT(AccountLedger.LedgerEvent.REJECT_PHASED_TRANSACTION, transaction.getId(),
                                                     transaction.getAmountNQT());
            TransactionProcessorImpl.getInstance()
                    .notifyListeners(Collections.singletonList(transaction), TransactionProcessor.Event.REJECT_PHASED_TRANSACTION);
            logger.debug("Transaction " + transaction.getStringId() + " has been rejected");
        }

        public void countVotes(TransactionImpl transaction) {
            if (PhasingPoll.getResult(transaction.getId()) != null) {
                return;
            }
            PhasingPoll poll = PhasingPoll.getPoll(transaction.getId());
            long result = poll.countVotes();
            poll.finish(result);
            if (result >= poll.getQuorum()) {
                try {
                    release(transaction);
                } catch (RuntimeException e) {
                    logger.error("Failed to release phased transaction " + transaction.getJSONObject().toJSONString(), e);
                    reject(transaction);
                }
            } else {
                reject(transaction);
            }
        }

        public void tryCountVotes(TransactionImpl transaction, Map<TransactionType, Map<String, Integer>> duplicates) {
            PhasingPoll poll = PhasingPoll.getPoll(transaction.getId());
            long result = poll.countVotes();
            if (result >= poll.getQuorum()) {
                if (!transaction.attachmentIsDuplicate(duplicates, false)) {
                    try {
                        release(transaction);
                        poll.finish(result);
                        logger.debug("Early finish of transaction " + transaction.getStringId() + " at height " + DiH.getBlockchain().getHeight());
                    } catch (RuntimeException e) {
                        logger.error("Failed to release phased transaction " + transaction.getJSONObject().toJSONString(), e);
                    }
                } else {
                    logger.debug("At height " + DiH.getBlockchain().getHeight() + " phased transaction " + transaction.getStringId()
                            + " is duplicate, cannot finish early");
                }
            } else {
                logger.debug("At height " + DiH.getBlockchain().getHeight() + " phased transaction " + transaction.getStringId()
                        + " does not yet meet quorum, cannot finish early");
            }
        }

        public int getFinishHeight() {
            return finishHeight;
        }

        public long getQuorum() {
            return params.getQuorum();
        }

        public long[] getWhitelist() {
            return params.getWhitelist();
        }

        public VoteWeighting getVoteWeighting() {
            return params.getVoteWeighting();
        }

        public byte[][] getLinkedFullHashes() {
            return linkedFullHashes;
        }

        public byte[] getHashedSecret() {
            return hashedSecret;
        }

        public byte getAlgorithm() {
            return algorithm;
        }

        public PhasingParams getParams() {
            return params;
        }
    }
