/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.crypto.util.Convert;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public abstract class ColoredCoinsOrderCancellation extends AbstractAttachment {
    
    final long orderId;

    public ColoredCoinsOrderCancellation(ByteBuffer buffer) {
        super(buffer);
        this.orderId = buffer.getLong();
    }

    public ColoredCoinsOrderCancellation(JSONObject attachmentData) {
        super(attachmentData);
        this.orderId = Convert.parseUnsignedLong((String) attachmentData.get("order"));
    }

    public ColoredCoinsOrderCancellation(long orderId) {
        this.orderId = orderId;
    }

    @Override
    public int getMySize() {
        return 8;
    }

    @Override
    public void putMyBytes(ByteBuffer buffer) {
        buffer.putLong(orderId);
    }

    @Override
    public void putMyJSON(JSONObject attachment) {
        attachment.put("order", Long.toUnsignedString(orderId));
    }

    public long getOrderId() {
        return orderId;
    }
    
}
