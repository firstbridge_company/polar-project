/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.core.MonetarySystem;
import io.firstbridge.fbc.transaction.TransactionType;
import io.firstbridge.fbc.crypto.util.Convert;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public final class MonetarySystemCurrencyDeletion extends AbstractAttachment implements Attachment.MonetarySystemAttachment {
    
    final long currencyId;

    public MonetarySystemCurrencyDeletion(ByteBuffer buffer) {
        super(buffer);
        this.currencyId = buffer.getLong();
    }

    public MonetarySystemCurrencyDeletion(JSONObject attachmentData) {
        super(attachmentData);
        this.currencyId = Convert.parseUnsignedLong((String) attachmentData.get("currency"));
    }

    public MonetarySystemCurrencyDeletion(long currencyId) {
        this.currencyId = currencyId;
    }

    @Override
    public int getMySize() {
        return 8;
    }

    @Override
    public void putMyBytes(ByteBuffer buffer) {
        buffer.putLong(currencyId);
    }

    @Override
    public void putMyJSON(JSONObject attachment) {
        attachment.put("currency", Long.toUnsignedString(currencyId));
    }

    @Override
    public TransactionType getTransactionType() {
        return MonetarySystem.CURRENCY_DELETION;
    }

    @Override
    public long getCurrencyId() {
        return currencyId;
    }
    
}
