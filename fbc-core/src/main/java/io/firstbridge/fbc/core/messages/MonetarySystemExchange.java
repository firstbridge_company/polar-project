/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.crypto.util.Convert;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public abstract class MonetarySystemExchange extends AbstractAttachment implements Attachment.MonetarySystemAttachment {
    
    final long currencyId;
    final long rateNQT;
    final long units;

    public MonetarySystemExchange(ByteBuffer buffer) {
        super(buffer);
        this.currencyId = buffer.getLong();
        this.rateNQT = buffer.getLong();
        this.units = buffer.getLong();
    }

    public MonetarySystemExchange(JSONObject attachmentData) {
        super(attachmentData);
        this.currencyId = Convert.parseUnsignedLong((String) attachmentData.get("currency"));
        this.rateNQT = Convert.parseLong(attachmentData.get("rateNQT"));
        this.units = Convert.parseLong(attachmentData.get("units"));
    }

    public MonetarySystemExchange(long currencyId, long rateNQT, long units) {
        this.currencyId = currencyId;
        this.rateNQT = rateNQT;
        this.units = units;
    }

    @Override
    public int getMySize() {
        return 8 + 8 + 8;
    }

    @Override
    public void putMyBytes(ByteBuffer buffer) {
        buffer.putLong(currencyId);
        buffer.putLong(rateNQT);
        buffer.putLong(units);
    }

    @Override
    public void putMyJSON(JSONObject attachment) {
        attachment.put("currency", Long.toUnsignedString(currencyId));
        attachment.put("rateNQT", rateNQT);
        attachment.put("units", units);
    }

    @Override
    public long getCurrencyId() {
        return currencyId;
    }

    public long getRateNQT() {
        return rateNQT;
    }

    public long getUnits() {
        return units;
    }
    
}
