
package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.core.DiH;
import io.firstbridge.fbc.core.account.Account;
import io.firstbridge.fbc.transaction.TransactionImpl;
import io.firstbridge.fbc.core.interfaces.Fee;
import io.firstbridge.fbc.core.interfaces.Transaction;
import io.firstbridge.fbc.util.FbcException;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public abstract class AbstractAttachment extends AbstractAppendix implements Attachment {
    
    public AbstractAttachment(ByteBuffer buffer) {
        super(buffer);
    }

    public AbstractAttachment(JSONObject attachmentData) {
        super(attachmentData);
    }

    public AbstractAttachment(int version) {
        super(version);
    }

    public AbstractAttachment() {
    }

    @Override
    public final String getAppendixName() {
        return getTransactionType().getName();
    }

    @Override
    public final void validate(Transaction transaction) throws FbcException.ValidationException {
        getTransactionType().validateAttachment(transaction);
    }

    @Override
    public final void apply(Transaction transaction, Account senderAccount, Account recipientAccount) {
        getTransactionType().apply((TransactionImpl) transaction, senderAccount, recipientAccount);
    }

    @Override
    public final Fee getBaselineFee(Transaction transaction) {
        return getTransactionType().getBaselineFee(transaction);
    }

    @Override
    public final Fee getNextFee(Transaction transaction) {
        return getTransactionType().getNextFee(transaction);
    }

    @Override
    public final int getBaselineFeeHeight() {
        return getTransactionType().getBaselineFeeHeight();
    }

    @Override
    public final int getNextFeeHeight() {
        return getTransactionType().getNextFeeHeight();
    }

    @Override
    public final boolean isPhasable() {
        return !(this instanceof Prunable) && getTransactionType().isPhasable();
    }

    public final int getFinishValidationHeight(Transaction transaction) {
        return isPhased(transaction) ? transaction.getPhasing().getFinishHeight() - 1 : DiH.getBlockchain().getHeight();
    }
    
}
