/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.messages;

import io.firstbridge.cryptolib.CryptoNotValidException;
import io.firstbridge.fbc.transaction.MessagingTr;
import io.firstbridge.fbc.transaction.TransactionType;
import io.firstbridge.fbc.util.Constants;
import io.firstbridge.fbc.crypto.util.Convert;
import io.firstbridge.fbc.util.FbcException;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public final class MessagingAliasDelete extends AbstractAttachment {
    
    final String aliasName;

    public MessagingAliasDelete(final ByteBuffer buffer) throws FbcException.NotValidException {
        super(buffer);
        try {
            this.aliasName = Convert.readString(buffer, buffer.get(), Constants.MAX_ALIAS_LENGTH);
        } catch (CryptoNotValidException ex) {
           throw new FbcException.NotValidException(ex.getMessage());
        }
    }

    public MessagingAliasDelete(final JSONObject attachmentData) {
        super(attachmentData);
        this.aliasName = Convert.nullToEmpty((String) attachmentData.get("alias"));
    }

    public MessagingAliasDelete(final String aliasName) {
        this.aliasName = aliasName;
    }

    @Override
    public TransactionType getTransactionType() {
        return MessagingTr.ALIAS_DELETE;
    }

    @Override
    public int getMySize() {
        return 1 + Convert.toBytes(aliasName).length;
    }

    @Override
    public void putMyBytes(final ByteBuffer buffer) {
        byte[] aliasBytes = Convert.toBytes(aliasName);
        buffer.put((byte) aliasBytes.length);
        buffer.put(aliasBytes);
    }

    @Override
    public void putMyJSON(final JSONObject attachment) {
        attachment.put("alias", aliasName);
    }

    public String getAliasName() {
        return aliasName;
    }
    
}
