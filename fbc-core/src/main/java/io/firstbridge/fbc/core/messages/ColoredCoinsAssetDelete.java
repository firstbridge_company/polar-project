/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.transaction.ColoredCoinsTr;
import io.firstbridge.fbc.transaction.TransactionType;
import io.firstbridge.fbc.crypto.util.Convert;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public final class ColoredCoinsAssetDelete extends AbstractAttachment {
    
    final long assetId;
    final long quantityQNT;

    public ColoredCoinsAssetDelete(ByteBuffer buffer) {
        super(buffer);
        this.assetId = buffer.getLong();
        this.quantityQNT = buffer.getLong();
    }

    public ColoredCoinsAssetDelete(JSONObject attachmentData) {
        super(attachmentData);
        this.assetId = Convert.parseUnsignedLong((String) attachmentData.get("asset"));
        this.quantityQNT = Convert.parseLong(attachmentData.get("quantityQNT"));
    }

    public ColoredCoinsAssetDelete(long assetId, long quantityQNT) {
        this.assetId = assetId;
        this.quantityQNT = quantityQNT;
    }

    @Override
    public int getMySize() {
        return 8 + 8;
    }

    @Override
    public void putMyBytes(ByteBuffer buffer) {
        buffer.putLong(assetId);
        buffer.putLong(quantityQNT);
    }

    @Override
    public void putMyJSON(JSONObject attachment) {
        attachment.put("asset", Long.toUnsignedString(assetId));
        attachment.put("quantityQNT", quantityQNT);
    }

    @Override
    public TransactionType getTransactionType() {
        return ColoredCoinsTr.ASSET_DELETE;
    }

    public long getAssetId() {
        return assetId;
    }

    public long getQuantityQNT() {
        return quantityQNT;
    }
    
}
