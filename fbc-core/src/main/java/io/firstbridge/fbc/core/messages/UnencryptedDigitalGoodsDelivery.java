/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.crypto.EncryptedData;
import io.firstbridge.fbc.crypto.util.Convert;
import io.firstbridge.fbc.util.FbcException;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public final class UnencryptedDigitalGoodsDelivery extends DigitalGoodsDelivery implements Appendix.Encryptable {
    
    final byte[] goodsToEncrypt;
    final byte[] recipientPublicKey;

    public UnencryptedDigitalGoodsDelivery(JSONObject attachmentData) {
        super(attachmentData);
        setGoods(null);
        String goodsToEncryptString = (String) attachmentData.get("goodsToEncrypt");
        this.goodsToEncrypt = goodsIsText() ? Convert.toBytes(goodsToEncryptString) : Convert.parseHexString(goodsToEncryptString);
        this.recipientPublicKey = Convert.parseHexString((String) attachmentData.get("recipientPublicKey"));
    }

    public UnencryptedDigitalGoodsDelivery(long purchaseId, byte[] goodsToEncrypt, boolean goodsIsText, long discountNQT, byte[] recipientPublicKey) {
        super(purchaseId, null, goodsIsText, discountNQT);
        this.goodsToEncrypt = goodsToEncrypt;
        this.recipientPublicKey = recipientPublicKey;
    }

    @Override
    public int getMySize() {
        if (getGoods() == null) {
            return 8 + 4 + EncryptedData.getEncryptedSize(getPlaintext()) + 8;
        }
        return super.getMySize();
    }

    @Override
    public void putMyBytes(ByteBuffer buffer) {
        if (getGoods() == null) {
            throw new FbcException.NotYetEncryptedException("Goods not yet encrypted");
        }
        super.putMyBytes(buffer);
    }

    @Override
    public void putMyJSON(JSONObject attachment) {
        if (getGoods() == null) {
            attachment.put("goodsToEncrypt", goodsIsText() ? Convert.toString(goodsToEncrypt) : Convert.toHexString(goodsToEncrypt));
            attachment.put("recipientPublicKey", Convert.toHexString(recipientPublicKey));
            attachment.put("purchase", Long.toUnsignedString(getPurchaseId()));
            attachment.put("discountNQT", getDiscountNQT());
            attachment.put("goodsIsText", goodsIsText());
        } else {
            super.putMyJSON(attachment);
        }
    }

    @Override
    public void encrypt(String secretPhrase) {
        setGoods(EncryptedData.encrypt(getPlaintext(), secretPhrase, recipientPublicKey));
    }

    @Override
    public int getGoodsDataLength() {
        return EncryptedData.getEncryptedDataLength(getPlaintext());
    }

    private byte[] getPlaintext() {
        return Convert.compress(goodsToEncrypt);
    }
    
}
