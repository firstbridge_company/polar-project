/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.messages;

import io.firstbridge.cryptolib.CryptoNotValidException;
import io.firstbridge.fbc.transaction.DigitalGoodsTr;
import io.firstbridge.fbc.transaction.TransactionType;
import io.firstbridge.fbc.util.Constants;
import io.firstbridge.fbc.crypto.util.Convert;
import io.firstbridge.fbc.util.FbcException;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public final class DigitalGoodsListing extends AbstractAttachment {
    
    final String name;
    final String description;
    final String tags;
    final int quantity;
    final long priceNQT;

    public DigitalGoodsListing(ByteBuffer buffer) throws FbcException.NotValidException {
        super(buffer);
        try {
            this.name = Convert.readString(buffer, buffer.getShort(), Constants.MAX_DGS_LISTING_NAME_LENGTH);
            this.description = Convert.readString(buffer, buffer.getShort(), Constants.MAX_DGS_LISTING_DESCRIPTION_LENGTH);
            this.tags = Convert.readString(buffer, buffer.getShort(), Constants.MAX_DGS_LISTING_TAGS_LENGTH);
            this.quantity = buffer.getInt();
            this.priceNQT = buffer.getLong();
        } catch (CryptoNotValidException ex) {
            throw new FbcException.NotValidException(ex.getMessage());
        }
    }

    public DigitalGoodsListing(JSONObject attachmentData) {
        super(attachmentData);
        this.name = (String) attachmentData.get("name");
        this.description = (String) attachmentData.get("description");
        this.tags = (String) attachmentData.get("tags");
        this.quantity = ((Long) attachmentData.get("quantity")).intValue();
        this.priceNQT = Convert.parseLong(attachmentData.get("priceNQT"));
    }

    public DigitalGoodsListing(String name, String description, String tags, int quantity, long priceNQT) {
        this.name = name;
        this.description = description;
        this.tags = tags;
        this.quantity = quantity;
        this.priceNQT = priceNQT;
    }

    @Override
    public int getMySize() {
        return 2 + Convert.toBytes(name).length + 2 + Convert.toBytes(description).length + 2 + Convert.toBytes(tags).length + 4 + 8;
    }

    @Override
    public void putMyBytes(ByteBuffer buffer) {
        byte[] nameBytes = Convert.toBytes(name);
        buffer.putShort((short) nameBytes.length);
        buffer.put(nameBytes);
        byte[] descriptionBytes = Convert.toBytes(description);
        buffer.putShort((short) descriptionBytes.length);
        buffer.put(descriptionBytes);
        byte[] tagsBytes = Convert.toBytes(tags);
        buffer.putShort((short) tagsBytes.length);
        buffer.put(tagsBytes);
        buffer.putInt(quantity);
        buffer.putLong(priceNQT);
    }

    @Override
    public void putMyJSON(JSONObject attachment) {
        attachment.put("name", name);
        attachment.put("description", description);
        attachment.put("tags", tags);
        attachment.put("quantity", quantity);
        attachment.put("priceNQT", priceNQT);
    }

    @Override
    public TransactionType getTransactionType() {
        return DigitalGoodsTr.LISTING;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getTags() {
        return tags;
    }

    public int getQuantity() {
        return quantity;
    }

    public long getPriceNQT() {
        return priceNQT;
    }
    
}
