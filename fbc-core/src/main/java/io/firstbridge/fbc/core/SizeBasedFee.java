/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core;

import io.firstbridge.fbc.core.interfaces.Fee;
import io.firstbridge.fbc.core.interfaces.Transaction;
import io.firstbridge.fbc.core.messages.Appendix;

/**
 *
 * @author al
 */
public abstract class SizeBasedFee implements Fee {
    
    private final long constantFee;
    private final long feePerSize;
    private final int unitSize;

    public SizeBasedFee(long feePerSize) {
        this(0, feePerSize);
    }

    public SizeBasedFee(long constantFee, long feePerSize) {
        this(constantFee, feePerSize, 1024);
    }

    public SizeBasedFee(long constantFee, long feePerSize, int unitSize) {
        this.constantFee = constantFee;
        this.feePerSize = feePerSize;
        this.unitSize = unitSize;
    }

    // the first size unit is free if constantFee is 0
    @Override
    public final long getFee(Transaction transaction, Appendix appendage) {
        int size = getSize(transaction, appendage) - 1;
        if (size < 0) {
            return constantFee;
        }
        return Math.addExact(constantFee, Math.multiplyExact((long) (size / unitSize), feePerSize));
    }

    public abstract int getSize(Transaction transaction, Appendix appendage);
    
}
