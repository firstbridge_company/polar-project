package io.firstbridge.fbc.core;

import io.firstbridge.fbc.core.interfaces.Blockchain;
import io.firstbridge.fbc.core.interfaces.BlockchainProcessor;
import io.firstbridge.fbc.core.interfaces.Transaction;
import io.firstbridge.fbc.core.interfaces.TransactionProcessor;
import io.firstbridge.fbc.core.messages.AbstractAttachment;
import io.firstbridge.fbc.core.messages.Attachment;
import io.firstbridge.fbc.transaction.TransactionImpl;
import io.firstbridge.fbc.transaction.TransactionProcessorImpl;
import io.firstbridge.fbc.util.FbcException;
import org.json.simple.JSONObject;

/**
 * This is temporary class, it should be cleaned later after dome dependency
 * injection mechanism will be added
 *
 * @author al
 */
public class DiH {

    private static volatile Time time = new Time.EpochTime();

    public static Blockchain getBlockchain() {
        return BlockchainImpl.getInstance();
    }

    public static BlockchainProcessor getBlockchainProcessor() {
        return BlockchainProcessorImpl.getInstance();
    }

    public static TransactionProcessor getTransactionProcessor() {
        return TransactionProcessorImpl.getInstance();
    }

    public static int getEpochTime() {
        return time.getTime();
    }

    public static void setTime(Time time) {
        DiH.time = time;
    }

    public static Transaction.Builder newTransactionBuilder(byte[] senderPublicKey, long amountNQT, long feeNQT, short deadline, Attachment attachment) {
        return new TransactionImpl.BuilderImpl((byte) 1, senderPublicKey, amountNQT, feeNQT, deadline, (AbstractAttachment) attachment);
    }

    public static Transaction.Builder newTransactionBuilder(byte[] transactionBytes) throws FbcException.NotValidException {
        return TransactionImpl.newTransactionBuilder(transactionBytes);
    }

    public static Transaction.Builder newTransactionBuilder(JSONObject transactionJSON) throws FbcException.NotValidException {
        return TransactionImpl.newTransactionBuilder(transactionJSON);
    }

    public static Transaction.Builder newTransactionBuilder(byte[] transactionBytes, JSONObject prunableAttachments) throws FbcException.NotValidException {
        return TransactionImpl.newTransactionBuilder(transactionBytes, prunableAttachments);
    }
}
