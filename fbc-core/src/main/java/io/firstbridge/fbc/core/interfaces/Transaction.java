/*
 * Copyright © 2013-2016 The Nxt Core Developers.
 * Copyright © 2016-2018 Jelurida IP B.V.
 *
 * See the LICENSE.txt file at the top-level directory of this distribution
 * for licensing information.
 *
 * Unless otherwise agreed in a custom licensing agreement with Jelurida B.V.,
 * no part of the Nxt software, including this file, may be copied, modified,
 * propagated, or distributed except according to the terms contained in the
 * LICENSE.txt file.
 *
 * Removal or modification of this copyright notice is prohibited.
 *
 */

package io.firstbridge.fbc.core.interfaces;

import io.firstbridge.fbc.core.messages.Appendix;
import io.firstbridge.fbc.core.messages.Attachment;
import io.firstbridge.fbc.transaction.TransactionType;
import io.firstbridge.fbc.core.messages.EncryptToSelfMessage;
import io.firstbridge.fbc.core.messages.EncryptedMessage;
import io.firstbridge.fbc.core.messages.Message;
import io.firstbridge.fbc.core.messages.Phasing;
import io.firstbridge.fbc.core.messages.PrunableEncryptedMessage;
import io.firstbridge.fbc.core.messages.PrunablePlainMessage;
import io.firstbridge.fbc.core.messages.PublicKeyAnnouncement;
import io.firstbridge.fbc.util.Filter;
import io.firstbridge.fbc.util.FbcException;
import org.json.simple.JSONObject;

import java.util.List;

public interface Transaction {

    interface Builder {

        Builder recipientId(long recipientId);

        Builder referencedTransactionFullHash(String referencedTransactionFullHash);

        Builder appendix(Message message);

        Builder appendix(EncryptedMessage encryptedMessage);

        Builder appendix(EncryptToSelfMessage encryptToSelfMessage);

        Builder appendix(PublicKeyAnnouncement publicKeyAnnouncement);

        Builder appendix(PrunablePlainMessage prunablePlainMessage);

        Builder appendix(PrunableEncryptedMessage prunableEncryptedMessage);

        Builder appendix(Phasing phasing);

        Builder timestamp(int timestamp);

        Builder ecBlockHeight(int height);

        Builder ecBlockId(long blockId);

        Transaction build() throws FbcException.NotValidException;

        Transaction build(String secretPhrase) throws FbcException.NotValidException;

    }

    long getId();

    String getStringId();

    long getSenderId();

    byte[] getSenderPublicKey();

    long getRecipientId();

    int getHeight();

    long getBlockId();

    Block getBlock();

    short getIndex();

    int getTimestamp();

    int getBlockTimestamp();

    short getDeadline();

    int getExpiration();

    long getAmountNQT();

    long getFeeNQT();

    String getReferencedTransactionFullHash();

    byte[] getSignature();

    String getFullHash();

    TransactionType getType();

    Attachment getAttachment();

    boolean verifySignature();

    void validate() throws FbcException.ValidationException;

    byte[] getBytes();

    byte[] getUnsignedBytes();

    JSONObject getJSONObject();

    JSONObject getPrunableAttachmentJSON();

    byte getVersion();

    int getFullSize();

    Message getMessage();

    EncryptedMessage getEncryptedMessage();

    EncryptToSelfMessage getEncryptToSelfMessage();

    Phasing getPhasing();

    PrunablePlainMessage getPrunablePlainMessage();

    PrunableEncryptedMessage getPrunableEncryptedMessage();

    List<? extends Appendix> getAppendages();

    List<? extends Appendix> getAppendages(boolean includeExpiredPrunable);

    List<? extends Appendix> getAppendages(Filter<Appendix> filter, boolean includeExpiredPrunable);

    int getECBlockHeight();

    long getECBlockId();
}
