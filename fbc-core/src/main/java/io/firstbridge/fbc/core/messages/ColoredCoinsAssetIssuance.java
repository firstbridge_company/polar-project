/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.messages;

import io.firstbridge.cryptolib.CryptoNotValidException;
import io.firstbridge.fbc.transaction.ColoredCoinsTr;
import io.firstbridge.fbc.transaction.TransactionType;
import io.firstbridge.fbc.util.Constants;
import io.firstbridge.fbc.crypto.util.Convert;
import io.firstbridge.fbc.util.FbcException;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public final class ColoredCoinsAssetIssuance extends AbstractAttachment {
    
    final String name;
    final String description;
    final long quantityQNT;
    final byte decimals;

    public ColoredCoinsAssetIssuance(ByteBuffer buffer) throws FbcException.NotValidException {
        super(buffer);
        try {
            this.name = Convert.readString(buffer, buffer.get(), Constants.MAX_ASSET_NAME_LENGTH);
            this.description = Convert.readString(buffer, buffer.getShort(), Constants.MAX_ASSET_DESCRIPTION_LENGTH);
            this.quantityQNT = buffer.getLong();
            this.decimals = buffer.get();
        } catch (CryptoNotValidException ex) {
            throw new FbcException.NotValidException(ex.getMessage());
        }
    }

    public ColoredCoinsAssetIssuance(JSONObject attachmentData) {
        super(attachmentData);
        this.name = (String) attachmentData.get("name");
        this.description = Convert.nullToEmpty((String) attachmentData.get("description"));
        this.quantityQNT = Convert.parseLong(attachmentData.get("quantityQNT"));
        this.decimals = ((Long) attachmentData.get("decimals")).byteValue();
    }

    public ColoredCoinsAssetIssuance(String name, String description, long quantityQNT, byte decimals) {
        this.name = name;
        this.description = Convert.nullToEmpty(description);
        this.quantityQNT = quantityQNT;
        this.decimals = decimals;
    }

    @Override
    public int getMySize() {
        return 1 + Convert.toBytes(name).length + 2 + Convert.toBytes(description).length + 8 + 1;
    }

    @Override
    public void putMyBytes(ByteBuffer buffer) {
        byte[] name = Convert.toBytes(this.name);
        byte[] description = Convert.toBytes(this.description);
        buffer.put((byte) name.length);
        buffer.put(name);
        buffer.putShort((short) description.length);
        buffer.put(description);
        buffer.putLong(quantityQNT);
        buffer.put(decimals);
    }

    @Override
    public void putMyJSON(JSONObject attachment) {
        attachment.put("name", name);
        attachment.put("description", description);
        attachment.put("quantityQNT", quantityQNT);
        attachment.put("decimals", decimals);
    }

    @Override
    public TransactionType getTransactionType() {
        return ColoredCoinsTr.ASSET_ISSUANCE;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public long getQuantityQNT() {
        return quantityQNT;
    }

    public byte getDecimals() {
        return decimals;
    }
    
}
