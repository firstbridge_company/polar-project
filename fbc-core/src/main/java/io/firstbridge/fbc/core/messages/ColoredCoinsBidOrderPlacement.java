/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.transaction.ColoredCoinsTr;
import io.firstbridge.fbc.transaction.TransactionType;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public final class ColoredCoinsBidOrderPlacement extends ColoredCoinsOrderPlacement {
    
    public ColoredCoinsBidOrderPlacement(ByteBuffer buffer) {
        super(buffer);
    }

    public ColoredCoinsBidOrderPlacement(JSONObject attachmentData) {
        super(attachmentData);
    }

    public ColoredCoinsBidOrderPlacement(long assetId, long quantityQNT, long priceNQT) {
        super(assetId, quantityQNT, priceNQT);
    }

    @Override
    public TransactionType getTransactionType() {
        return ColoredCoinsTr.BID_ORDER_PLACEMENT;
    }
    
}
