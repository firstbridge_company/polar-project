
package io.firstbridge.fbc.core.messages;

import io.firstbridge.fbc.core.ShufflingParticipant;
import io.firstbridge.fbc.transaction.ShufflingTransaction;
import io.firstbridge.fbc.transaction.TransactionType;
import io.firstbridge.fbc.core.interfaces.Transaction;
import io.firstbridge.fbc.crypto.Crypto;
import io.firstbridge.fbc.crypto.util.Convert;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.util.Arrays;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public final class ShufflingProcessing extends AbstractShufflingAttachment implements Appendix.Prunable {
    
    private static final byte[] emptyDataHash = Crypto.sha256().digest();

    public static ShufflingProcessing parse(JSONObject attachmentData) {
        if (!Appendix.hasAppendix(ShufflingTransaction.SHUFFLING_PROCESSING.getName(), attachmentData)) {
            return null;
        }
        return new ShufflingProcessing(attachmentData);
    }
    volatile byte[][] data;
    final byte[] hash;

    public ShufflingProcessing(ByteBuffer buffer) {
        super(buffer);
        this.hash = new byte[32];
        buffer.get(hash);
        this.data = Arrays.equals(hash, emptyDataHash) ? Convert.EMPTY_BYTES : null;
    }

    public ShufflingProcessing(JSONObject attachmentData) {
        super(attachmentData);
        JSONArray jsonArray = (JSONArray) attachmentData.get("data");
        if (jsonArray != null) {
            this.data = new byte[jsonArray.size()][];
            for (int i = 0; i < this.data.length; i++) {
                this.data[i] = Convert.parseHexString((String) jsonArray.get(i));
            }
            this.hash = null;
        } else {
            this.hash = Convert.parseHexString(Convert.emptyToNull((String) attachmentData.get("hash")));
            this.data = Arrays.equals(hash, emptyDataHash) ? Convert.EMPTY_BYTES : null;
        }
    }

    public ShufflingProcessing(long shufflingId, byte[][] data, byte[] shufflingStateHash) {
        super(shufflingId, shufflingStateHash);
        this.data = data;
        this.hash = null;
    }

    @Override
    public int getMyFullSize() {
        int size = super.getMySize();
        if (data != null) {
            size += 1;
            for (byte[] bytes : data) {
                size += 4;
                size += bytes.length;
            }
        }
        return size / 2; // just lie
    }

    @Override
    public int getMySize() {
        return super.getMySize() + 32;
    }

    @Override
    public void putMyBytes(ByteBuffer buffer) {
        super.putMyBytes(buffer);
        buffer.put(getHash());
    }

    @Override
    public void putMyJSON(JSONObject attachment) {
        super.putMyJSON(attachment);
        if (data != null) {
            JSONArray jsonArray = new JSONArray();
            attachment.put("data", jsonArray);
            for (byte[] bytes : data) {
                jsonArray.add(Convert.toHexString(bytes));
            }
        }
        attachment.put("hash", Convert.toHexString(getHash()));
    }

    @Override
    public TransactionType getTransactionType() {
        return ShufflingTransaction.SHUFFLING_PROCESSING;
    }

    @Override
    public byte[] getHash() {
        if (hash != null) {
            return hash;
        } else if (data != null) {
            MessageDigest digest = Crypto.sha256();
            for (byte[] bytes : data) {
                digest.update(bytes);
            }
            return digest.digest();
        } else {
            throw new IllegalStateException("Both hash and data are null");
        }
    }

    public byte[][] getData() {
        return data;
    }

    @Override
    public void loadPrunable(Transaction transaction, boolean includeExpiredPrunable) {
        if (data == null && shouldLoadPrunable(transaction, includeExpiredPrunable)) {
            data = ShufflingParticipant.getData(getShufflingId(), transaction.getSenderId());
        }
    }

    @Override
    public boolean hasPrunableData() {
        return data != null;
    }

    @Override
    public void restorePrunableData(Transaction transaction, int blockTimestamp, int height) {
        ShufflingParticipant.restoreData(getShufflingId(), transaction.getSenderId(), getData(), transaction.getTimestamp(), height);
    }
    
}
