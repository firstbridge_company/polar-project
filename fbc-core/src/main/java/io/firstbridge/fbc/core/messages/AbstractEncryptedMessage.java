package io.firstbridge.fbc.core.messages;

import io.firstbridge.cryptolib.CryptoNotValidException;
import io.firstbridge.fbc.core.account.Account;
import io.firstbridge.fbc.core.interfaces.Fee;
import io.firstbridge.fbc.core.SizeBasedFee;
import io.firstbridge.fbc.core.interfaces.Transaction;
import io.firstbridge.fbc.crypto.EncryptedData;
import io.firstbridge.fbc.util.Constants;
import io.firstbridge.fbc.crypto.util.Convert;
import io.firstbridge.fbc.util.FbcException;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
   public abstract class AbstractEncryptedMessage extends AbstractAppendix {

        private static final Fee ENCRYPTED_MESSAGE_FEE = new SizeBasedFee(Constants.ONE_NXT, Constants.ONE_NXT, 32) {
            @Override
            public int getSize(Transaction transaction, Appendix appendage) {
                return ((AbstractEncryptedMessage)appendage).getEncryptedDataLength() - 16;
            }
        };

        private EncryptedData encryptedData;
        private final boolean isText;
        private final boolean isCompressed;

        public AbstractEncryptedMessage(ByteBuffer buffer) throws FbcException.NotValidException {
            super(buffer);
            int length = buffer.getInt();
            this.isText = length < 0;
            if (length < 0) {
                length &= Integer.MAX_VALUE;
            }
            try {
                this.encryptedData = EncryptedData.readEncryptedData(buffer, length, 1000);
                this.isCompressed = getVersion() != 2;
            } catch (CryptoNotValidException ex) {
                throw new FbcException.NotValidException(ex.getMessage());
            }
        }

        public AbstractEncryptedMessage(JSONObject attachmentJSON, JSONObject encryptedMessageJSON) {
            super(attachmentJSON);
            byte[] data = Convert.parseHexString((String)encryptedMessageJSON.get("data"));
            byte[] nonce = Convert.parseHexString((String) encryptedMessageJSON.get("nonce"));
            this.encryptedData = new EncryptedData(data, nonce);
            this.isText = Boolean.TRUE.equals(encryptedMessageJSON.get("isText"));
            Object isCompressed = encryptedMessageJSON.get("isCompressed");
            this.isCompressed = isCompressed == null || Boolean.TRUE.equals(isCompressed);
        }

        public AbstractEncryptedMessage(EncryptedData encryptedData, boolean isText, boolean isCompressed) {
            super(isCompressed ? 1 : 2);
            this.encryptedData = encryptedData;
            this.isText = isText;
            this.isCompressed = isCompressed;
        }

        @Override
        public int getMySize() {
            return 4 + encryptedData.getSize();
        }

        @Override
        public void putMyBytes(ByteBuffer buffer) {
            buffer.putInt(isText ? (encryptedData.getData().length | Integer.MIN_VALUE) : encryptedData.getData().length);
            buffer.put(encryptedData.getData());
            buffer.put(encryptedData.getNonce());
        }

        @Override
        public void putMyJSON(JSONObject json) {
            json.put("data", Convert.toHexString(encryptedData.getData()));
            json.put("nonce", Convert.toHexString(encryptedData.getNonce()));
            json.put("isText", isText);
            json.put("isCompressed", isCompressed);
        }

        @Override
        public Fee getBaselineFee(Transaction transaction) {
            return ENCRYPTED_MESSAGE_FEE;
        }

        @Override
        public void validate(Transaction transaction) throws FbcException.ValidationException {
            if (getEncryptedDataLength() > Constants.MAX_ENCRYPTED_MESSAGE_LENGTH) {
                throw new FbcException.NotValidException("Max encrypted message length exceeded");
            }
            if (encryptedData != null) {
                if ((encryptedData.getNonce().length != 32 && encryptedData.getData().length > 0)
                        || (encryptedData.getNonce().length != 0 && encryptedData.getData().length == 0)) {
                    throw new FbcException.NotValidException("Invalid nonce length " + encryptedData.getNonce().length);
                }
            }
            if ((getVersion() != 2 && !isCompressed) || (getVersion() == 2 && isCompressed)) {
                throw new FbcException.NotValidException("Version mismatch - version " + getVersion() + ", isCompressed " + isCompressed);
            }
        }

        @Override
        public boolean verifyVersion() {
            return getVersion() == 1 || getVersion() == 2;
        }

        @Override
        public void apply(Transaction transaction, Account senderAccount, Account recipientAccount) {}

        public final EncryptedData getEncryptedData() {
            return encryptedData;
        }

        public final void setEncryptedData(EncryptedData encryptedData) {
            this.encryptedData = encryptedData;
        }

        public int getEncryptedDataLength() {
            return encryptedData.getData().length;
        }

        public final boolean isText() {
            return isText;
        }

        public final boolean isCompressed() {
            return isCompressed;
        }

        @Override
        public final boolean isPhasable() {
            return false;
        }

    }

