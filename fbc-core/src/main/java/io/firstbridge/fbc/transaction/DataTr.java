package io.firstbridge.fbc.transaction;

import io.firstbridge.fbc.core.DiH;
import io.firstbridge.fbc.core.account.Account;
import io.firstbridge.fbc.core.account.AccountLedger;
import io.firstbridge.fbc.core.TaggedData;
import io.firstbridge.fbc.core.TransactionDb;
import io.firstbridge.fbc.core.interfaces.Fee;
import io.firstbridge.fbc.core.SizeBasedFee;
import io.firstbridge.fbc.core.interfaces.Transaction;
import io.firstbridge.fbc.core.messages.Appendix;
import io.firstbridge.fbc.core.messages.TaggedDataExtend;
import io.firstbridge.fbc.core.messages.TaggedDataUpload;
import io.firstbridge.fbc.util.Constants;
import io.firstbridge.fbc.crypto.util.Convert;
import io.firstbridge.fbc.util.FbcException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public abstract class DataTr extends TransactionType {
    
    private static final Fee TAGGED_DATA_FEE = new SizeBasedFee(Constants.ONE_NXT, Constants.ONE_NXT / 10) {
        @Override
        public int getSize(Transaction transaction, Appendix appendix) {
            return appendix.getFullSize();
        }
    };

    private DataTr() {
    }

    @Override
    public final byte getType() {
        return TransactionType.TYPE_DATA;
    }

    @Override
    public final Fee getBaselineFee(Transaction transaction) {
        return TAGGED_DATA_FEE;
    }

    @Override
    public final boolean applyAttachmentUnconfirmed(Transaction transaction, Account senderAccount) {
        return true;
    }

    @Override
    public final void undoAttachmentUnconfirmed(Transaction transaction, Account senderAccount) {
    }

    @Override
    public final boolean canHaveRecipient() {
        return false;
    }

    @Override
    public final boolean isPhasingSafe() {
        return false;
    }

    @Override
    public final boolean isPhasable() {
        return false;
    }
    public static final TransactionType TAGGED_DATA_UPLOAD = new DataTr() {
        @Override
        public byte getSubtype() {
            return SUBTYPE_DATA_TAGGED_DATA_UPLOAD;
        }

        @Override
        public AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.TAGGED_DATA_UPLOAD;
        }

        @Override
        public TaggedDataUpload parseAttachment(ByteBuffer buffer) {
            return new TaggedDataUpload(buffer);
        }

        @Override
        public TaggedDataUpload parseAttachment(JSONObject attachmentData) {
            return new TaggedDataUpload(attachmentData);
        }

        @Override
        public void validateAttachment(Transaction transaction) throws FbcException.ValidationException {
            TaggedDataUpload attachment = (TaggedDataUpload) transaction.getAttachment();
            if (attachment.getData() == null && DiH.getEpochTime() - transaction.getTimestamp() < Constants.MIN_PRUNABLE_LIFETIME) {
                throw new FbcException.NotCurrentlyValidException("Data has been pruned prematurely");
            }
            if (attachment.getData() != null) {
                if (attachment.getName().length() == 0 || attachment.getName().length() > Constants.MAX_TAGGED_DATA_NAME_LENGTH) {
                    throw new FbcException.NotValidException("Invalid name length: " + attachment.getName().length());
                }
                if (attachment.getDescription().length() > Constants.MAX_TAGGED_DATA_DESCRIPTION_LENGTH) {
                    throw new FbcException.NotValidException("Invalid description length: " + attachment.getDescription().length());
                }
                if (attachment.getTags().length() > Constants.MAX_TAGGED_DATA_TAGS_LENGTH) {
                    throw new FbcException.NotValidException("Invalid tags length: " + attachment.getTags().length());
                }
                if (attachment.getType().length() > Constants.MAX_TAGGED_DATA_TYPE_LENGTH) {
                    throw new FbcException.NotValidException("Invalid type length: " + attachment.getType().length());
                }
                if (attachment.getChannel().length() > Constants.MAX_TAGGED_DATA_CHANNEL_LENGTH) {
                    throw new FbcException.NotValidException("Invalid channel length: " + attachment.getChannel().length());
                }
                if (attachment.getFilename().length() > Constants.MAX_TAGGED_DATA_FILENAME_LENGTH) {
                    throw new FbcException.NotValidException("Invalid filename length: " + attachment.getFilename().length());
                }
                if (attachment.getData().length == 0 || attachment.getData().length > Constants.MAX_TAGGED_DATA_DATA_LENGTH) {
                    throw new FbcException.NotValidException("Invalid data length: " + attachment.getData().length);
                }
            }
        }

        @Override
        public void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {
            TaggedDataUpload attachment = (TaggedDataUpload) transaction.getAttachment();
            TaggedData.add((TransactionImpl) transaction, attachment);
        }

        @Override
        public String getName() {
            return "TaggedDataUpload";
        }

        @Override
        public boolean isPruned(long transactionId) {
            return TaggedData.isPruned(transactionId);
        }
    };
    public static final TransactionType TAGGED_DATA_EXTEND = new DataTr() {
        @Override
        public byte getSubtype() {
            return SUBTYPE_DATA_TAGGED_DATA_EXTEND;
        }

        @Override
        public AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.TAGGED_DATA_EXTEND;
        }

        @Override
        public TaggedDataExtend parseAttachment(ByteBuffer buffer) {
            return new TaggedDataExtend(buffer);
        }

        @Override
        public TaggedDataExtend parseAttachment(JSONObject attachmentData) {
            return new TaggedDataExtend(attachmentData);
        }

        @Override
        public void validateAttachment(Transaction transaction) throws FbcException.ValidationException {
            TaggedDataExtend attachment = (TaggedDataExtend) transaction.getAttachment();
            if ((attachment.jsonIsPruned() || attachment.getData() == null) && DiH.getEpochTime() - transaction.getTimestamp() < Constants.MIN_PRUNABLE_LIFETIME) {
                throw new FbcException.NotCurrentlyValidException("Data has been pruned prematurely");
            }
            TransactionImpl uploadTransaction = TransactionDb.findTransaction(attachment.getTaggedDataId(), DiH.getBlockchain().getHeight());
            if (uploadTransaction == null) {
                throw new FbcException.NotCurrentlyValidException("No such tagged data upload " + Long.toUnsignedString(attachment.getTaggedDataId()));
            }
            if (uploadTransaction.getType() != TAGGED_DATA_UPLOAD) {
                throw new FbcException.NotValidException("Transaction " + Long.toUnsignedString(attachment.getTaggedDataId()) + " is not a tagged data upload");
            }
            if (attachment.getData() != null) {
                TaggedDataUpload taggedDataUpload = (TaggedDataUpload) uploadTransaction.getAttachment();
                if (!Arrays.equals(attachment.getHash(), taggedDataUpload.getHash())) {
                    throw new FbcException.NotValidException("Hashes don't match! Extend hash: " + Convert.toHexString(attachment.getHash()) + " upload hash: " + Convert.toHexString(taggedDataUpload.getHash()));
                }
            }
            TaggedData taggedData = TaggedData.getData(attachment.getTaggedDataId());
            if (taggedData != null && taggedData.getTransactionTimestamp() > DiH.getEpochTime() + 6 * Constants.MIN_PRUNABLE_LIFETIME) {
                throw new FbcException.NotCurrentlyValidException("Data already extended, timestamp is " + taggedData.getTransactionTimestamp());
            }
        }

        @Override
        public void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {
            TaggedDataExtend attachment = (TaggedDataExtend) transaction.getAttachment();
            TaggedData.extend(transaction, attachment);
        }

        @Override
        public String getName() {
            return "TaggedDataExtend";
        }

        @Override
        public boolean isPruned(long transactionId) {
            return false;
        }
    };
    
}
