package io.firstbridge.fbc.transaction;

import io.firstbridge.fbc.core.account.Account;
import io.firstbridge.fbc.core.account.AccountLedger;
import io.firstbridge.fbc.core.Genesis;
import io.firstbridge.fbc.core.interfaces.Transaction;
import io.firstbridge.fbc.core.messages.Attachment;
import io.firstbridge.fbc.core.messages.EmptyAttachment;
import io.firstbridge.fbc.util.Constants;
import io.firstbridge.fbc.util.FbcException;
import java.nio.ByteBuffer;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public abstract class PaymentTr extends TransactionType {
    
    private PaymentTr() {
    }

    @Override
    public final byte getType() {
        return TransactionType.TYPE_PAYMENT;
    }

    @Override
    public final boolean applyAttachmentUnconfirmed(Transaction transaction, Account senderAccount) {
        return true;
    }

    @Override
    public final void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {
        if (recipientAccount == null) {
            Account.getAccount(Genesis.CREATOR_ID).addToBalanceAndUnconfirmedBalanceNQT(getLedgerEvent(), transaction.getId(), transaction.getAmountNQT());
        }
    }

    @Override
    public final void undoAttachmentUnconfirmed(Transaction transaction, Account senderAccount) {
    }

    @Override
    public final boolean canHaveRecipient() {
        return true;
    }

    @Override
    public final boolean isPhasingSafe() {
        return true;
    }
    public static final TransactionType ORDINARY = new PaymentTr() {
        @Override
        public final byte getSubtype() {
            return TransactionType.SUBTYPE_PAYMENT_ORDINARY_PAYMENT;
        }

        @Override
        public final AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.ORDINARY_PAYMENT;
        }

        @Override
        public String getName() {
            return "OrdinaryPayment";
        }

        @Override
        public EmptyAttachment parseAttachment(ByteBuffer buffer) {
            return Attachment.ORDINARY_PAYMENT;
        }

        @Override
        public EmptyAttachment parseAttachment(JSONObject attachmentData) {
            return Attachment.ORDINARY_PAYMENT;
        }

        @Override
        public void validateAttachment(Transaction transaction) throws FbcException.ValidationException {
            if (transaction.getAmountNQT() <= 0 || transaction.getAmountNQT() >= Constants.MAX_BALANCE_NQT) {
                throw new FbcException.NotValidException("Invalid ordinary payment");
            }
        }
    };
    
}
