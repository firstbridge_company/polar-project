package io.firstbridge.fbc.transaction;

import io.firstbridge.fbc.core.DiH;
import io.firstbridge.fbc.core.account.Account;
import io.firstbridge.fbc.core.account.AccountLedger;
import io.firstbridge.fbc.core.DigitalGoodsStore;
import io.firstbridge.fbc.core.interfaces.Fee;
import io.firstbridge.fbc.core.SizeBasedFee;
import io.firstbridge.fbc.core.interfaces.Transaction;
import io.firstbridge.fbc.core.messages.Appendix;
import io.firstbridge.fbc.core.messages.DigitalGoodsDelisting;
import io.firstbridge.fbc.core.messages.DigitalGoodsDelivery;
import io.firstbridge.fbc.core.messages.DigitalGoodsFeedback;
import io.firstbridge.fbc.core.messages.DigitalGoodsListing;
import io.firstbridge.fbc.core.messages.DigitalGoodsPriceChange;
import io.firstbridge.fbc.core.messages.DigitalGoodsPurchase;
import io.firstbridge.fbc.core.messages.DigitalGoodsQuantityChange;
import io.firstbridge.fbc.core.messages.DigitalGoodsRefund;
import io.firstbridge.fbc.core.messages.PrunablePlainMessage;
import io.firstbridge.fbc.core.messages.UnencryptedDigitalGoodsDelivery;
import io.firstbridge.fbc.util.Constants;
import io.firstbridge.fbc.util.FbcException;
import java.nio.ByteBuffer;
import java.util.Map;
import org.apache.tika.Tika;
import org.apache.tika.mime.MediaType;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author al
 */
public abstract class DigitalGoodsTr extends TransactionType {
     static final Logger logger = LoggerFactory.getLogger(DigitalGoodsTr.class);
  
    private DigitalGoodsTr() {
    }

    @Override
    public final byte getType() {
        return TransactionType.TYPE_DIGITAL_GOODS;
    }

    @Override
    public boolean applyAttachmentUnconfirmed(Transaction transaction, Account senderAccount) {
        return true;
    }

    @Override
    public void undoAttachmentUnconfirmed(Transaction transaction, Account senderAccount) {
    }

    @Override
    public final void validateAttachment(Transaction transaction) throws FbcException.ValidationException {
        if (transaction.getAmountNQT() != 0) {
            throw new FbcException.NotValidException("Invalid digital goods transaction");
        }
        doValidateAttachment(transaction);
    }

    public abstract void doValidateAttachment(Transaction transaction) throws FbcException.ValidationException;
    public static final TransactionType LISTING = new DigitalGoodsTr() {
        private final Fee DGS_LISTING_FEE = new SizeBasedFee(2 * Constants.ONE_NXT, 2 * Constants.ONE_NXT, 32) {
            @Override
            public int getSize(Transaction transaction, Appendix appendage) {
                DigitalGoodsListing attachment = (DigitalGoodsListing) transaction.getAttachment();
                return attachment.getName().length() + attachment.getDescription().length();
            }
        };

        @Override
        public final byte getSubtype() {
            return TransactionType.SUBTYPE_DIGITAL_GOODS_LISTING;
        }

        @Override
        public AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.DIGITAL_GOODS_LISTING;
        }

        @Override
        public String getName() {
            return "DigitalGoodsListing";
        }

        @Override
        public Fee getBaselineFee(Transaction transaction) {
            return DGS_LISTING_FEE;
        }

        @Override
        public DigitalGoodsListing parseAttachment(ByteBuffer buffer) throws FbcException.NotValidException {
            return new DigitalGoodsListing(buffer);
        }

        @Override
        public DigitalGoodsListing parseAttachment(JSONObject attachmentData) {
            return new DigitalGoodsListing(attachmentData);
        }

        @Override
        public void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {
            DigitalGoodsListing attachment = (DigitalGoodsListing) transaction.getAttachment();
            DigitalGoodsStore.listGoods(transaction, attachment);
        }

        @Override
        public void doValidateAttachment(Transaction transaction) throws FbcException.ValidationException {
            DigitalGoodsListing attachment = (DigitalGoodsListing) transaction.getAttachment();
            if (attachment.getName().length() == 0 || attachment.getName().length() > Constants.MAX_DGS_LISTING_NAME_LENGTH || attachment.getDescription().length() > Constants.MAX_DGS_LISTING_DESCRIPTION_LENGTH || attachment.getTags().length() > Constants.MAX_DGS_LISTING_TAGS_LENGTH || attachment.getQuantity() < 0 || attachment.getQuantity() > Constants.MAX_DGS_LISTING_QUANTITY || attachment.getPriceNQT() <= 0 || attachment.getPriceNQT() > Constants.MAX_BALANCE_NQT) {
                throw new FbcException.NotValidException("Invalid digital goods listing: " + attachment.getJSONObject());
            }
            PrunablePlainMessage prunablePlainMessage = transaction.getPrunablePlainMessage();
            if (prunablePlainMessage != null) {
                byte[] image = prunablePlainMessage.getMessage();
                if (image != null) {
                    Tika tika = new Tika();
                    MediaType mediaType = null;
                    try {
                        String mediaTypeName = tika.detect(image);
                        mediaType = MediaType.parse(mediaTypeName);
                    } catch (NoClassDefFoundError e) {
                        logger.error("Error running Tika parsers", e);
                    }
                    if (mediaType == null || !"image".equals(mediaType.getType())) {
                        throw new FbcException.NotValidException("Only image attachments allowed for DGS listing, media type is " + mediaType);
                    }
                }
            }
        }

        @Override
        public boolean isBlockDuplicate(Transaction transaction, Map<TransactionType, Map<String, Integer>> duplicates) {
            return isDuplicate(DigitalGoodsTr.LISTING, getName(), duplicates, true);
        }

        @Override
        public boolean canHaveRecipient() {
            return false;
        }

        @Override
        public boolean isPhasingSafe() {
            return true;
        }
    };
    public static final TransactionType DELISTING = new DigitalGoodsTr() {
        @Override
        public final byte getSubtype() {
            return TransactionType.SUBTYPE_DIGITAL_GOODS_DELISTING;
        }

        @Override
        public AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.DIGITAL_GOODS_DELISTING;
        }

        @Override
        public String getName() {
            return "DigitalGoodsDelisting";
        }

        @Override
        public DigitalGoodsDelisting parseAttachment(ByteBuffer buffer) {
            return new DigitalGoodsDelisting(buffer);
        }

        @Override
        public DigitalGoodsDelisting parseAttachment(JSONObject attachmentData) {
            return new DigitalGoodsDelisting(attachmentData);
        }

        @Override
        public void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {
            DigitalGoodsDelisting attachment = (DigitalGoodsDelisting) transaction.getAttachment();
            DigitalGoodsStore.delistGoods(attachment.getGoodsId());
        }

        @Override
        public void doValidateAttachment(Transaction transaction) throws FbcException.ValidationException {
            DigitalGoodsDelisting attachment = (DigitalGoodsDelisting) transaction.getAttachment();
            DigitalGoodsStore.Goods goods = DigitalGoodsStore.Goods.getGoods(attachment.getGoodsId());
            if (goods != null && transaction.getSenderId() != goods.getSellerId()) {
                throw new FbcException.NotValidException("Invalid digital goods delisting - seller is different: " + attachment.getJSONObject());
            }
            if (goods == null || goods.isDelisted()) {
                throw new FbcException.NotCurrentlyValidException("Goods " + Long.toUnsignedString(attachment.getGoodsId()) + "not yet listed or already delisted");
            }
        }

        @Override
        public boolean isDuplicate(Transaction transaction, Map<TransactionType, Map<String, Integer>> duplicates) {
            DigitalGoodsDelisting attachment = (DigitalGoodsDelisting) transaction.getAttachment();
            return isDuplicate(DigitalGoodsTr.DELISTING, Long.toUnsignedString(attachment.getGoodsId()), duplicates, true);
        }

        @Override
        public boolean canHaveRecipient() {
            return false;
        }

        @Override
        public boolean isPhasingSafe() {
            return true;
        }
    };
    public static final TransactionType PRICE_CHANGE = new DigitalGoodsTr() {
        @Override
        public final byte getSubtype() {
            return TransactionType.SUBTYPE_DIGITAL_GOODS_PRICE_CHANGE;
        }

        @Override
        public AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.DIGITAL_GOODS_PRICE_CHANGE;
        }

        @Override
        public String getName() {
            return "DigitalGoodsPriceChange";
        }

        @Override
        public DigitalGoodsPriceChange parseAttachment(ByteBuffer buffer) {
            return new DigitalGoodsPriceChange(buffer);
        }

        @Override
        public DigitalGoodsPriceChange parseAttachment(JSONObject attachmentData) {
            return new DigitalGoodsPriceChange(attachmentData);
        }

        @Override
        public void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {
            DigitalGoodsPriceChange attachment = (DigitalGoodsPriceChange) transaction.getAttachment();
            DigitalGoodsStore.changePrice(attachment.getGoodsId(), attachment.getPriceNQT());
        }

        @Override
        public void doValidateAttachment(Transaction transaction) throws FbcException.ValidationException {
            DigitalGoodsPriceChange attachment = (DigitalGoodsPriceChange) transaction.getAttachment();
            DigitalGoodsStore.Goods goods = DigitalGoodsStore.Goods.getGoods(attachment.getGoodsId());
            if (attachment.getPriceNQT() <= 0 || attachment.getPriceNQT() > Constants.MAX_BALANCE_NQT || (goods != null && transaction.getSenderId() != goods.getSellerId())) {
                throw new FbcException.NotValidException("Invalid digital goods price change: " + attachment.getJSONObject());
            }
            if (goods == null || goods.isDelisted()) {
                throw new FbcException.NotCurrentlyValidException("Goods " + Long.toUnsignedString(attachment.getGoodsId()) + "not yet listed or already delisted");
            }
        }

        @Override
        public boolean isDuplicate(Transaction transaction, Map<TransactionType, Map<String, Integer>> duplicates) {
            DigitalGoodsPriceChange attachment = (DigitalGoodsPriceChange) transaction.getAttachment();
            // not a bug, uniqueness is based on DigitalGoods.DELISTING
            return isDuplicate(DigitalGoodsTr.DELISTING, Long.toUnsignedString(attachment.getGoodsId()), duplicates, true);
        }

        @Override
        public boolean canHaveRecipient() {
            return false;
        }

        @Override
        public boolean isPhasingSafe() {
            return false;
        }
    };
    public static final TransactionType QUANTITY_CHANGE = new DigitalGoodsTr() {
        @Override
        public final byte getSubtype() {
            return TransactionType.SUBTYPE_DIGITAL_GOODS_QUANTITY_CHANGE;
        }

        @Override
        public AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.DIGITAL_GOODS_QUANTITY_CHANGE;
        }

        @Override
        public String getName() {
            return "DigitalGoodsQuantityChange";
        }

        @Override
        public DigitalGoodsQuantityChange parseAttachment(ByteBuffer buffer) {
            return new DigitalGoodsQuantityChange(buffer);
        }

        @Override
        public DigitalGoodsQuantityChange parseAttachment(JSONObject attachmentData) {
            return new DigitalGoodsQuantityChange(attachmentData);
        }

        @Override
        public void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {
            DigitalGoodsQuantityChange attachment = (DigitalGoodsQuantityChange) transaction.getAttachment();
            DigitalGoodsStore.changeQuantity(attachment.getGoodsId(), attachment.getDeltaQuantity());
        }

        @Override
        public void doValidateAttachment(Transaction transaction) throws FbcException.ValidationException {
            DigitalGoodsQuantityChange attachment = (DigitalGoodsQuantityChange) transaction.getAttachment();
            DigitalGoodsStore.Goods goods = DigitalGoodsStore.Goods.getGoods(attachment.getGoodsId());
            if (attachment.getDeltaQuantity() < -Constants.MAX_DGS_LISTING_QUANTITY || attachment.getDeltaQuantity() > Constants.MAX_DGS_LISTING_QUANTITY || (goods != null && transaction.getSenderId() != goods.getSellerId())) {
                throw new FbcException.NotValidException("Invalid digital goods quantity change: " + attachment.getJSONObject());
            }
            if (goods == null || goods.isDelisted()) {
                throw new FbcException.NotCurrentlyValidException("Goods " + Long.toUnsignedString(attachment.getGoodsId()) + "not yet listed or already delisted");
            }
        }

        @Override
        public boolean isDuplicate(Transaction transaction, Map<TransactionType, Map<String, Integer>> duplicates) {
            DigitalGoodsQuantityChange attachment = (DigitalGoodsQuantityChange) transaction.getAttachment();
            // not a bug, uniqueness is based on DigitalGoods.DELISTING
            return isDuplicate(DigitalGoodsTr.DELISTING, Long.toUnsignedString(attachment.getGoodsId()), duplicates, true);
        }

        @Override
        public boolean canHaveRecipient() {
            return false;
        }

        @Override
        public boolean isPhasingSafe() {
            return false;
        }
    };
    public static final TransactionType PURCHASE = new DigitalGoodsTr() {
        @Override
        public final byte getSubtype() {
            return TransactionType.SUBTYPE_DIGITAL_GOODS_PURCHASE;
        }

        @Override
        public AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.DIGITAL_GOODS_PURCHASE;
        }

        @Override
        public String getName() {
            return "DigitalGoodsPurchase";
        }

        @Override
        public DigitalGoodsPurchase parseAttachment(ByteBuffer buffer) {
            return new DigitalGoodsPurchase(buffer);
        }

        @Override
        public DigitalGoodsPurchase parseAttachment(JSONObject attachmentData) {
            return new DigitalGoodsPurchase(attachmentData);
        }

        @Override
        public boolean applyAttachmentUnconfirmed(Transaction transaction, Account senderAccount) {
            DigitalGoodsPurchase attachment = (DigitalGoodsPurchase) transaction.getAttachment();
            if (senderAccount.getUnconfirmedBalanceNQT() >= Math.multiplyExact((long) attachment.getQuantity(), attachment.getPriceNQT())) {
                senderAccount.addToUnconfirmedBalanceNQT(getLedgerEvent(), transaction.getId(), -Math.multiplyExact((long) attachment.getQuantity(), attachment.getPriceNQT()));
                return true;
            }
            return false;
        }

        @Override
        public void undoAttachmentUnconfirmed(Transaction transaction, Account senderAccount) {
            DigitalGoodsPurchase attachment = (DigitalGoodsPurchase) transaction.getAttachment();
            senderAccount.addToUnconfirmedBalanceNQT(getLedgerEvent(), transaction.getId(), Math.multiplyExact((long) attachment.getQuantity(), attachment.getPriceNQT()));
        }

        @Override
        public void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {
            DigitalGoodsPurchase attachment = (DigitalGoodsPurchase) transaction.getAttachment();
            DigitalGoodsStore.purchase(transaction, attachment);
        }

        @Override
        public void doValidateAttachment(Transaction transaction) throws FbcException.ValidationException {
            DigitalGoodsPurchase attachment = (DigitalGoodsPurchase) transaction.getAttachment();
            DigitalGoodsStore.Goods goods = DigitalGoodsStore.Goods.getGoods(attachment.getGoodsId());
            if (attachment.getQuantity() <= 0 || attachment.getQuantity() > Constants.MAX_DGS_LISTING_QUANTITY || attachment.getPriceNQT() <= 0 || attachment.getPriceNQT() > Constants.MAX_BALANCE_NQT || (goods != null && goods.getSellerId() != transaction.getRecipientId())) {
                throw new FbcException.NotValidException("Invalid digital goods purchase: " + attachment.getJSONObject());
            }
            if (transaction.getEncryptedMessage() != null && !transaction.getEncryptedMessage().isText()) {
                throw new FbcException.NotValidException("Only text encrypted messages allowed");
            }
            if (goods == null || goods.isDelisted()) {
                throw new FbcException.NotCurrentlyValidException("Goods " + Long.toUnsignedString(attachment.getGoodsId()) + "not yet listed or already delisted");
            }
            if (attachment.getQuantity() > goods.getQuantity() || attachment.getPriceNQT() != goods.getPriceNQT()) {
                throw new FbcException.NotCurrentlyValidException("Goods price or quantity changed: " + attachment.getJSONObject());
            }
            if (attachment.getDeliveryDeadlineTimestamp() <= DiH.getBlockchain().getLastBlockTimestamp()) {
                throw new FbcException.NotCurrentlyValidException("Delivery deadline has already expired: " + attachment.getDeliveryDeadlineTimestamp());
            }
        }

        @Override
        public boolean isDuplicate(Transaction transaction, Map<TransactionType, Map<String, Integer>> duplicates) {
            DigitalGoodsPurchase attachment = (DigitalGoodsPurchase) transaction.getAttachment();
            // not a bug, uniqueness is based on DigitalGoods.DELISTING
            return isDuplicate(DigitalGoodsTr.DELISTING, Long.toUnsignedString(attachment.getGoodsId()), duplicates, false);
        }

        @Override
        public boolean canHaveRecipient() {
            return true;
        }

        @Override
        public boolean isPhasingSafe() {
            return false;
        }
    };
    public static final TransactionType DELIVERY = new DigitalGoodsTr() {
        private final Fee DGS_DELIVERY_FEE = new SizeBasedFee(Constants.ONE_NXT, 2 * Constants.ONE_NXT, 32) {
            @Override
            public int getSize(Transaction transaction, Appendix appendage) {
                DigitalGoodsDelivery attachment = (DigitalGoodsDelivery) transaction.getAttachment();
                return attachment.getGoodsDataLength() - 16;
            }
        };

        @Override
        public final byte getSubtype() {
            return TransactionType.SUBTYPE_DIGITAL_GOODS_DELIVERY;
        }

        @Override
        public AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.DIGITAL_GOODS_DELIVERY;
        }

        @Override
        public String getName() {
            return "DigitalGoodsDelivery";
        }

        @Override
        public Fee getBaselineFee(Transaction transaction) {
            return DGS_DELIVERY_FEE;
        }

        @Override
        public DigitalGoodsDelivery parseAttachment(ByteBuffer buffer) throws FbcException.NotValidException {
            return new DigitalGoodsDelivery(buffer);
        }

        @Override
        public DigitalGoodsDelivery parseAttachment(JSONObject attachmentData) {
            if (attachmentData.get("goodsData") == null) {
                return new UnencryptedDigitalGoodsDelivery(attachmentData);
            }
            return new DigitalGoodsDelivery(attachmentData);
        }

        @Override
        public void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {
            DigitalGoodsDelivery attachment = (DigitalGoodsDelivery) transaction.getAttachment();
            DigitalGoodsStore.deliver(transaction, attachment);
        }

        @Override
        public void doValidateAttachment(Transaction transaction) throws FbcException.ValidationException {
            DigitalGoodsDelivery attachment = (DigitalGoodsDelivery) transaction.getAttachment();
            DigitalGoodsStore.Purchase purchase = DigitalGoodsStore.Purchase.getPendingPurchase(attachment.getPurchaseId());
            if (attachment.getGoodsDataLength() > Constants.MAX_DGS_GOODS_LENGTH) {
                throw new FbcException.NotValidException("Invalid digital goods delivery data length: " + attachment.getGoodsDataLength());
            }
            if (attachment.getGoods() != null) {
                if (attachment.getGoods().getData().length == 0 || attachment.getGoods().getNonce().length != 32) {
                    throw new FbcException.NotValidException("Invalid digital goods delivery: " + attachment.getJSONObject());
                }
            }
            if (attachment.getDiscountNQT() < 0 || attachment.getDiscountNQT() > Constants.MAX_BALANCE_NQT || (purchase != null && (purchase.getBuyerId() != transaction.getRecipientId() || transaction.getSenderId() != purchase.getSellerId() || attachment.getDiscountNQT() > Math.multiplyExact(purchase.getPriceNQT(), (long) purchase.getQuantity())))) {
                throw new FbcException.NotValidException("Invalid digital goods delivery: " + attachment.getJSONObject());
            }
            if (purchase == null || purchase.getEncryptedGoods() != null) {
                throw new FbcException.NotCurrentlyValidException("Purchase does not exist yet, or already delivered: " + attachment.getJSONObject());
            }
        }

        @Override
        public boolean isDuplicate(Transaction transaction, Map<TransactionType, Map<String, Integer>> duplicates) {
            DigitalGoodsDelivery attachment = (DigitalGoodsDelivery) transaction.getAttachment();
            return isDuplicate(DigitalGoodsTr.DELIVERY, Long.toUnsignedString(attachment.getPurchaseId()), duplicates, true);
        }

        @Override
        public boolean canHaveRecipient() {
            return true;
        }

        @Override
        public boolean isPhasingSafe() {
            return false;
        }
    };
    public static final TransactionType FEEDBACK = new DigitalGoodsTr() {
        @Override
        public final byte getSubtype() {
            return TransactionType.SUBTYPE_DIGITAL_GOODS_FEEDBACK;
        }

        @Override
        public AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.DIGITAL_GOODS_FEEDBACK;
        }

        @Override
        public String getName() {
            return "DigitalGoodsFeedback";
        }

        @Override
        public DigitalGoodsFeedback parseAttachment(ByteBuffer buffer) {
            return new DigitalGoodsFeedback(buffer);
        }

        @Override
        public DigitalGoodsFeedback parseAttachment(JSONObject attachmentData) {
            return new DigitalGoodsFeedback(attachmentData);
        }

        @Override
        public void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {
            DigitalGoodsFeedback attachment = (DigitalGoodsFeedback) transaction.getAttachment();
            DigitalGoodsStore.feedback(attachment.getPurchaseId(), transaction.getEncryptedMessage(), transaction.getMessage());
        }

        @Override
        public void doValidateAttachment(Transaction transaction) throws FbcException.ValidationException {
            DigitalGoodsFeedback attachment = (DigitalGoodsFeedback) transaction.getAttachment();
            DigitalGoodsStore.Purchase purchase = DigitalGoodsStore.Purchase.getPurchase(attachment.getPurchaseId());
            if (purchase != null && (purchase.getSellerId() != transaction.getRecipientId() || transaction.getSenderId() != purchase.getBuyerId())) {
                throw new FbcException.NotValidException("Invalid digital goods feedback: " + attachment.getJSONObject());
            }
            if (transaction.getEncryptedMessage() == null && transaction.getMessage() == null) {
                throw new FbcException.NotValidException("Missing feedback message");
            }
            if (transaction.getEncryptedMessage() != null && !transaction.getEncryptedMessage().isText()) {
                throw new FbcException.NotValidException("Only text encrypted messages allowed");
            }
            if (transaction.getMessage() != null && !transaction.getMessage().isText()) {
                throw new FbcException.NotValidException("Only text public messages allowed");
            }
            if (purchase == null || purchase.getEncryptedGoods() == null) {
                throw new FbcException.NotCurrentlyValidException("Purchase does not exist yet or not yet delivered");
            }
        }

        @Override
        public boolean canHaveRecipient() {
            return true;
        }

        @Override
        public boolean isPhasingSafe() {
            return false;
        }
    };
    public static final TransactionType REFUND = new DigitalGoodsTr() {
        @Override
        public final byte getSubtype() {
            return TransactionType.SUBTYPE_DIGITAL_GOODS_REFUND;
        }

        @Override
        public AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.DIGITAL_GOODS_REFUND;
        }

        @Override
        public String getName() {
            return "DigitalGoodsRefund";
        }

        @Override
        public DigitalGoodsRefund parseAttachment(ByteBuffer buffer) {
            return new DigitalGoodsRefund(buffer);
        }

        @Override
        public DigitalGoodsRefund parseAttachment(JSONObject attachmentData) {
            return new DigitalGoodsRefund(attachmentData);
        }

        @Override
        public boolean applyAttachmentUnconfirmed(Transaction transaction, Account senderAccount) {
            DigitalGoodsRefund attachment = (DigitalGoodsRefund) transaction.getAttachment();
            if (senderAccount.getUnconfirmedBalanceNQT() >= attachment.getRefundNQT()) {
                senderAccount.addToUnconfirmedBalanceNQT(getLedgerEvent(), transaction.getId(), -attachment.getRefundNQT());
                return true;
            }
            return false;
        }

        @Override
        public void undoAttachmentUnconfirmed(Transaction transaction, Account senderAccount) {
            DigitalGoodsRefund attachment = (DigitalGoodsRefund) transaction.getAttachment();
            senderAccount.addToUnconfirmedBalanceNQT(getLedgerEvent(), transaction.getId(), attachment.getRefundNQT());
        }

        @Override
        public void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {
            DigitalGoodsRefund attachment = (DigitalGoodsRefund) transaction.getAttachment();
            DigitalGoodsStore.refund(getLedgerEvent(), transaction.getId(), transaction.getSenderId(), attachment.getPurchaseId(), attachment.getRefundNQT(), transaction.getEncryptedMessage());
        }

        @Override
        public void doValidateAttachment(Transaction transaction) throws FbcException.ValidationException {
            DigitalGoodsRefund attachment = (DigitalGoodsRefund) transaction.getAttachment();
            DigitalGoodsStore.Purchase purchase = DigitalGoodsStore.Purchase.getPurchase(attachment.getPurchaseId());
            if (attachment.getRefundNQT() < 0 || attachment.getRefundNQT() > Constants.MAX_BALANCE_NQT || (purchase != null && (purchase.getBuyerId() != transaction.getRecipientId() || transaction.getSenderId() != purchase.getSellerId()))) {
                throw new FbcException.NotValidException("Invalid digital goods refund: " + attachment.getJSONObject());
            }
            if (transaction.getEncryptedMessage() != null && !transaction.getEncryptedMessage().isText()) {
                throw new FbcException.NotValidException("Only text encrypted messages allowed");
            }
            if (purchase == null || purchase.getEncryptedGoods() == null || purchase.getRefundNQT() != 0) {
                throw new FbcException.NotCurrentlyValidException("Purchase does not exist or is not delivered or is already refunded");
            }
        }

        @Override
        public boolean isDuplicate(Transaction transaction, Map<TransactionType, Map<String, Integer>> duplicates) {
            DigitalGoodsRefund attachment = (DigitalGoodsRefund) transaction.getAttachment();
            return isDuplicate(DigitalGoodsTr.REFUND, Long.toUnsignedString(attachment.getPurchaseId()), duplicates, true);
        }

        @Override
        public boolean canHaveRecipient() {
            return true;
        }

        @Override
        public boolean isPhasingSafe() {
            return false;
        }
    };
    
}
