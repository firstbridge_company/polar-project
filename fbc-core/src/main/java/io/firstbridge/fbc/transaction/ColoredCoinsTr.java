package io.firstbridge.fbc.transaction;


import io.firstbridge.fbc.core.account.Account;
import io.firstbridge.fbc.core.account.AccountLedger;
import io.firstbridge.fbc.core.Asset;
import io.firstbridge.fbc.core.AssetDividend;
import io.firstbridge.fbc.core.AssetTransfer;
import io.firstbridge.fbc.core.DiH;
import io.firstbridge.fbc.core.Genesis;
import io.firstbridge.fbc.core.Order;
import io.firstbridge.fbc.core.interfaces.Fee;
import io.firstbridge.fbc.core.SizeBasedFee;
import io.firstbridge.fbc.core.interfaces.Transaction;
import io.firstbridge.fbc.core.messages.Appendix;
import io.firstbridge.fbc.core.messages.ColoredCoinsAskOrderCancellation;
import io.firstbridge.fbc.core.messages.ColoredCoinsAskOrderPlacement;
import io.firstbridge.fbc.core.messages.ColoredCoinsAssetDelete;
import io.firstbridge.fbc.core.messages.ColoredCoinsAssetIssuance;
import io.firstbridge.fbc.core.messages.ColoredCoinsAssetTransfer;
import io.firstbridge.fbc.core.messages.ColoredCoinsBidOrderCancellation;
import io.firstbridge.fbc.core.messages.ColoredCoinsBidOrderPlacement;
import io.firstbridge.fbc.core.messages.ColoredCoinsDividendPayment;
import io.firstbridge.fbc.core.messages.ColoredCoinsOrderCancellation;
import io.firstbridge.fbc.core.messages.ColoredCoinsOrderPlacement;
import io.firstbridge.fbc.util.Constants;
import io.firstbridge.fbc.crypto.util.Convert;
import io.firstbridge.fbc.util.FbcException;
import java.nio.ByteBuffer;
import java.util.Locale;
import java.util.Map;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public abstract class ColoredCoinsTr extends TransactionType {
    
    private ColoredCoinsTr() {
    }

    @Override
    public final byte getType() {
        return TransactionType.TYPE_COLORED_COINS;
    }
    public static final TransactionType ASSET_ISSUANCE = new ColoredCoinsTr() {
        private final Fee SINGLETON_ASSET_FEE = new SizeBasedFee(Constants.ONE_NXT, Constants.ONE_NXT, 32) {
            public int getSize(Transaction transaction, Appendix appendage) {
                ColoredCoinsAssetIssuance attachment = (ColoredCoinsAssetIssuance) transaction.getAttachment();
                return attachment.getDescription().length();
            }
        };
        private final Fee ASSET_ISSUANCE_FEE = (io.firstbridge.fbc.core.interfaces.Transaction transaction, io.firstbridge.fbc.core.messages.Appendix appendage) -> isSingletonIssuance(transaction) ? SINGLETON_ASSET_FEE.getFee(transaction, appendage) : 1000 * Constants.ONE_NXT;

        @Override
        public final byte getSubtype() {
            return TransactionType.SUBTYPE_COLORED_COINS_ASSET_ISSUANCE;
        }

        @Override
        public AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.ASSET_ISSUANCE;
        }

        @Override
        public String getName() {
            return "AssetIssuance";
        }

        @Override
        public Fee getBaselineFee(Transaction transaction) {
            return ASSET_ISSUANCE_FEE;
        }

        @Override
        public long[] getBackFees(Transaction transaction) {
            if (isSingletonIssuance(transaction)) {
                return Convert.EMPTY_LONG;
            }
            long feeNQT = transaction.getFeeNQT();
            return new long[]{feeNQT * 3 / 10, feeNQT * 2 / 10, feeNQT / 10};
        }

        @Override
        public ColoredCoinsAssetIssuance parseAttachment(ByteBuffer buffer) throws FbcException.NotValidException {
            return new ColoredCoinsAssetIssuance(buffer);
        }

        @Override
        public ColoredCoinsAssetIssuance parseAttachment(JSONObject attachmentData) {
            return new ColoredCoinsAssetIssuance(attachmentData);
        }

        @Override
        public boolean applyAttachmentUnconfirmed(Transaction transaction, Account senderAccount) {
            return true;
        }

        @Override
        public void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {
            ColoredCoinsAssetIssuance attachment = (ColoredCoinsAssetIssuance) transaction.getAttachment();
            long assetId = transaction.getId();
            Asset.addAsset(transaction, attachment);
            senderAccount.addToAssetAndUnconfirmedAssetBalanceQNT(getLedgerEvent(), assetId, assetId, attachment.getQuantityQNT());
        }

        @Override
        public void undoAttachmentUnconfirmed(Transaction transaction, Account senderAccount) {
        }

        @Override
        public void validateAttachment(Transaction transaction) throws FbcException.ValidationException {
            ColoredCoinsAssetIssuance attachment = (ColoredCoinsAssetIssuance) transaction.getAttachment();
            if (attachment.getName().length() < Constants.MIN_ASSET_NAME_LENGTH || attachment.getName().length() > Constants.MAX_ASSET_NAME_LENGTH || attachment.getDescription().length() > Constants.MAX_ASSET_DESCRIPTION_LENGTH || attachment.getDecimals() < 0 || attachment.getDecimals() > 8 || attachment.getQuantityQNT() <= 0 || attachment.getQuantityQNT() > Constants.MAX_ASSET_QUANTITY_QNT) {
                throw new FbcException.NotValidException("Invalid asset issuance: " + attachment.getJSONObject());
            }
            String normalizedName = attachment.getName().toLowerCase(Locale.ROOT);
            for (int i = 0; i < normalizedName.length(); i++) {
                if (Constants.ALPHABET.indexOf(normalizedName.charAt(i)) < 0) {
                    throw new FbcException.NotValidException("Invalid asset name: " + normalizedName);
                }
            }
        }

        @Override
        public boolean isBlockDuplicate(final Transaction transaction, final Map<TransactionType, Map<String, Integer>> duplicates) {
            return !isSingletonIssuance(transaction) && isDuplicate(ColoredCoinsTr.ASSET_ISSUANCE, getName(), duplicates, true);
        }

        @Override
        public boolean canHaveRecipient() {
            return false;
        }

        @Override
        public boolean isPhasingSafe() {
            return true;
        }

        private boolean isSingletonIssuance(Transaction transaction) {
            ColoredCoinsAssetIssuance attachment = (ColoredCoinsAssetIssuance) transaction.getAttachment();
            return attachment.getQuantityQNT() == 1 && attachment.getDecimals() == 0 && attachment.getDescription().length() <= Constants.MAX_SINGLETON_ASSET_DESCRIPTION_LENGTH;
        }
    };
    public static final TransactionType ASSET_TRANSFER = new ColoredCoinsTr() {
        @Override
        public final byte getSubtype() {
            return TransactionType.SUBTYPE_COLORED_COINS_ASSET_TRANSFER;
        }

        @Override
        public AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.ASSET_TRANSFER;
        }

        @Override
        public String getName() {
            return "AssetTransfer";
        }

        @Override
        public ColoredCoinsAssetTransfer parseAttachment(ByteBuffer buffer) throws FbcException.NotValidException {
            return new ColoredCoinsAssetTransfer(buffer);
        }

        @Override
        public ColoredCoinsAssetTransfer parseAttachment(JSONObject attachmentData) {
            return new ColoredCoinsAssetTransfer(attachmentData);
        }

        @Override
        public boolean applyAttachmentUnconfirmed(Transaction transaction, Account senderAccount) {
            ColoredCoinsAssetTransfer attachment = (ColoredCoinsAssetTransfer) transaction.getAttachment();
            long unconfirmedAssetBalance = senderAccount.getUnconfirmedAssetBalanceQNT(attachment.getAssetId());
            if (unconfirmedAssetBalance >= 0 && unconfirmedAssetBalance >= attachment.getQuantityQNT()) {
                senderAccount.addToUnconfirmedAssetBalanceQNT(getLedgerEvent(), transaction.getId(), attachment.getAssetId(), -attachment.getQuantityQNT());
                return true;
            }
            return false;
        }

        @Override
        public void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {
            ColoredCoinsAssetTransfer attachment = (ColoredCoinsAssetTransfer) transaction.getAttachment();
            senderAccount.addToAssetBalanceQNT(getLedgerEvent(), transaction.getId(), attachment.getAssetId(), -attachment.getQuantityQNT());
            if (recipientAccount.getId() == Genesis.CREATOR_ID) {
                Asset.deleteAsset(transaction, attachment.getAssetId(), attachment.getQuantityQNT());
            } else {
                recipientAccount.addToAssetAndUnconfirmedAssetBalanceQNT(getLedgerEvent(), transaction.getId(), attachment.getAssetId(), attachment.getQuantityQNT());
                AssetTransfer.addAssetTransfer(transaction, attachment);
            }
        }

        @Override
        public void undoAttachmentUnconfirmed(Transaction transaction, Account senderAccount) {
            ColoredCoinsAssetTransfer attachment = (ColoredCoinsAssetTransfer) transaction.getAttachment();
            senderAccount.addToUnconfirmedAssetBalanceQNT(getLedgerEvent(), transaction.getId(), attachment.getAssetId(), attachment.getQuantityQNT());
        }

        @Override
        public void validateAttachment(Transaction transaction) throws FbcException.ValidationException {
            ColoredCoinsAssetTransfer attachment = (ColoredCoinsAssetTransfer) transaction.getAttachment();
            if (transaction.getAmountNQT() != 0 || attachment.getAssetId() == 0) {
                throw new FbcException.NotValidException("Invalid asset transfer amount or asset: " + attachment.getJSONObject());
            }
            if (transaction.getRecipientId() == Genesis.CREATOR_ID) {
                throw new FbcException.NotValidException("Asset transfer to Genesis not allowed, " + "use asset delete attachment instead");
            }
            Asset asset = Asset.getAsset(attachment.getAssetId());
            if (attachment.getQuantityQNT() <= 0 || (asset != null && attachment.getQuantityQNT() > asset.getInitialQuantityQNT())) {
                throw new FbcException.NotValidException("Invalid asset transfer asset or quantity: " + attachment.getJSONObject());
            }
            if (asset == null) {
                throw new FbcException.NotCurrentlyValidException("Asset " + Long.toUnsignedString(attachment.getAssetId()) + " does not exist yet");
            }
        }

        @Override
        public boolean canHaveRecipient() {
            return true;
        }

        @Override
        public boolean isPhasingSafe() {
            return true;
        }
    };
    public static final TransactionType ASSET_DELETE = new ColoredCoinsTr() {
        @Override
        public final byte getSubtype() {
            return TransactionType.SUBTYPE_COLORED_COINS_ASSET_DELETE;
        }

        @Override
        public AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.ASSET_DELETE;
        }

        @Override
        public String getName() {
            return "AssetDelete";
        }

        @Override
        public ColoredCoinsAssetDelete parseAttachment(ByteBuffer buffer) {
            return new ColoredCoinsAssetDelete(buffer);
        }

        @Override
        public ColoredCoinsAssetDelete parseAttachment(JSONObject attachmentData) {
            return new ColoredCoinsAssetDelete(attachmentData);
        }

        @Override
        public boolean applyAttachmentUnconfirmed(Transaction transaction, Account senderAccount) {
            ColoredCoinsAssetDelete attachment = (ColoredCoinsAssetDelete) transaction.getAttachment();
            long unconfirmedAssetBalance = senderAccount.getUnconfirmedAssetBalanceQNT(attachment.getAssetId());
            if (unconfirmedAssetBalance >= 0 && unconfirmedAssetBalance >= attachment.getQuantityQNT()) {
                senderAccount.addToUnconfirmedAssetBalanceQNT(getLedgerEvent(), transaction.getId(), attachment.getAssetId(), -attachment.getQuantityQNT());
                return true;
            }
            return false;
        }

        @Override
        public void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {
            ColoredCoinsAssetDelete attachment = (ColoredCoinsAssetDelete) transaction.getAttachment();
            senderAccount.addToAssetBalanceQNT(getLedgerEvent(), transaction.getId(), attachment.getAssetId(), -attachment.getQuantityQNT());
            Asset.deleteAsset(transaction, attachment.getAssetId(), attachment.getQuantityQNT());
        }

        @Override
        public void undoAttachmentUnconfirmed(Transaction transaction, Account senderAccount) {
            ColoredCoinsAssetDelete attachment = (ColoredCoinsAssetDelete) transaction.getAttachment();
            senderAccount.addToUnconfirmedAssetBalanceQNT(getLedgerEvent(), transaction.getId(), attachment.getAssetId(), attachment.getQuantityQNT());
        }

        @Override
        public void validateAttachment(Transaction transaction) throws FbcException.ValidationException {
            ColoredCoinsAssetDelete attachment = (ColoredCoinsAssetDelete) transaction.getAttachment();
            if (attachment.getAssetId() == 0) {
                throw new FbcException.NotValidException("Invalid asset identifier: " + attachment.getJSONObject());
            }
            Asset asset = Asset.getAsset(attachment.getAssetId());
            if (attachment.getQuantityQNT() <= 0 || (asset != null && attachment.getQuantityQNT() > asset.getInitialQuantityQNT())) {
                throw new FbcException.NotValidException("Invalid asset delete asset or quantity: " + attachment.getJSONObject());
            }
            if (asset == null) {
                throw new FbcException.NotCurrentlyValidException("Asset " + Long.toUnsignedString(attachment.getAssetId()) + " does not exist yet");
            }
        }

        @Override
        public boolean canHaveRecipient() {
            return false;
        }

        @Override
        public boolean isPhasingSafe() {
            return true;
        }
    };

    static abstract class ColoredCoinsOrderPlacementTr extends ColoredCoinsTr {

        @Override
        public final void validateAttachment(Transaction transaction) throws FbcException.ValidationException {
            ColoredCoinsOrderPlacement attachment = (ColoredCoinsOrderPlacement) transaction.getAttachment();
            if (attachment.getPriceNQT() <= 0 || attachment.getPriceNQT() > Constants.MAX_BALANCE_NQT || attachment.getAssetId() == 0) {
                throw new FbcException.NotValidException("Invalid asset order placement: " + attachment.getJSONObject());
            }
            Asset asset = Asset.getAsset(attachment.getAssetId());
            if (attachment.getQuantityQNT() <= 0 || (asset != null && attachment.getQuantityQNT() > asset.getInitialQuantityQNT())) {
                throw new FbcException.NotValidException("Invalid asset order placement asset or quantity: " + attachment.getJSONObject());
            }
            if (asset == null) {
                throw new FbcException.NotCurrentlyValidException("Asset " + Long.toUnsignedString(attachment.getAssetId()) + " does not exist yet");
            }
        }

        @Override
        public final boolean canHaveRecipient() {
            return false;
        }

        @Override
        public final boolean isPhasingSafe() {
            return true;
        }
    }
    public static final TransactionType ASK_ORDER_PLACEMENT = new ColoredCoinsTr.ColoredCoinsOrderPlacementTr() {
        @Override
        public final byte getSubtype() {
            return TransactionType.SUBTYPE_COLORED_COINS_ASK_ORDER_PLACEMENT;
        }

        @Override
        public AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.ASSET_ASK_ORDER_PLACEMENT;
        }

        @Override
        public String getName() {
            return "AskOrderPlacement";
        }

        @Override
        public ColoredCoinsAskOrderPlacement parseAttachment(ByteBuffer buffer) {
            return new ColoredCoinsAskOrderPlacement(buffer);
        }

        @Override
        public ColoredCoinsAskOrderPlacement parseAttachment(JSONObject attachmentData) {
            return new ColoredCoinsAskOrderPlacement(attachmentData);
        }

        @Override
        public boolean applyAttachmentUnconfirmed(Transaction transaction, Account senderAccount) {
            ColoredCoinsAskOrderPlacement attachment = (ColoredCoinsAskOrderPlacement) transaction.getAttachment();
            long unconfirmedAssetBalance = senderAccount.getUnconfirmedAssetBalanceQNT(attachment.getAssetId());
            if (unconfirmedAssetBalance >= 0 && unconfirmedAssetBalance >= attachment.getQuantityQNT()) {
                senderAccount.addToUnconfirmedAssetBalanceQNT(getLedgerEvent(), transaction.getId(), attachment.getAssetId(), -attachment.getQuantityQNT());
                return true;
            }
            return false;
        }

        @Override
        public void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {
            ColoredCoinsAskOrderPlacement attachment = (ColoredCoinsAskOrderPlacement) transaction.getAttachment();
            Order.Ask.addOrder(transaction, attachment);
        }

        @Override
        public void undoAttachmentUnconfirmed(Transaction transaction, Account senderAccount) {
            ColoredCoinsAskOrderPlacement attachment = (ColoredCoinsAskOrderPlacement) transaction.getAttachment();
            senderAccount.addToUnconfirmedAssetBalanceQNT(getLedgerEvent(), transaction.getId(), attachment.getAssetId(), attachment.getQuantityQNT());
        }
    };
    public static final TransactionType BID_ORDER_PLACEMENT = new ColoredCoinsTr.ColoredCoinsOrderPlacementTr() {
        @Override
        public final byte getSubtype() {
            return TransactionType.SUBTYPE_COLORED_COINS_BID_ORDER_PLACEMENT;
        }

        @Override
        public AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.ASSET_BID_ORDER_PLACEMENT;
        }

        @Override
        public String getName() {
            return "BidOrderPlacement";
        }

        @Override
        public ColoredCoinsBidOrderPlacement parseAttachment(ByteBuffer buffer) {
            return new ColoredCoinsBidOrderPlacement(buffer);
        }

        @Override
        public ColoredCoinsBidOrderPlacement parseAttachment(JSONObject attachmentData) {
            return new ColoredCoinsBidOrderPlacement(attachmentData);
        }

        @Override
        public boolean applyAttachmentUnconfirmed(Transaction transaction, Account senderAccount) {
            ColoredCoinsBidOrderPlacement attachment = (ColoredCoinsBidOrderPlacement) transaction.getAttachment();
            if (senderAccount.getUnconfirmedBalanceNQT() >= Math.multiplyExact(attachment.getQuantityQNT(), attachment.getPriceNQT())) {
                senderAccount.addToUnconfirmedBalanceNQT(getLedgerEvent(), transaction.getId(), -Math.multiplyExact(attachment.getQuantityQNT(), attachment.getPriceNQT()));
                return true;
            }
            return false;
        }

        @Override
        public void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {
            ColoredCoinsBidOrderPlacement attachment = (ColoredCoinsBidOrderPlacement) transaction.getAttachment();
            Order.Bid.addOrder(transaction, attachment);
        }

        @Override
        public void undoAttachmentUnconfirmed(Transaction transaction, Account senderAccount) {
            ColoredCoinsBidOrderPlacement attachment = (ColoredCoinsBidOrderPlacement) transaction.getAttachment();
            senderAccount.addToUnconfirmedBalanceNQT(getLedgerEvent(), transaction.getId(), Math.multiplyExact(attachment.getQuantityQNT(), attachment.getPriceNQT()));
        }
    };

    static abstract class ColoredCoinsOrderCancellationTr extends ColoredCoinsTr {

        @Override
        public final boolean applyAttachmentUnconfirmed(Transaction transaction, Account senderAccount) {
            return true;
        }

        @Override
        public final void undoAttachmentUnconfirmed(Transaction transaction, Account senderAccount) {
        }

        @Override
        public boolean isUnconfirmedDuplicate(Transaction transaction, Map<TransactionType, Map<String, Integer>> duplicates) {
            ColoredCoinsOrderCancellation attachment = (ColoredCoinsOrderCancellation) transaction.getAttachment();
            return TransactionType.isDuplicate(ColoredCoinsTr.ASK_ORDER_CANCELLATION, Long.toUnsignedString(attachment.getOrderId()), duplicates, true);
        }

        @Override
        public final boolean canHaveRecipient() {
            return false;
        }

        @Override
        public final boolean isPhasingSafe() {
            return true;
        }
    }
    public static final TransactionType ASK_ORDER_CANCELLATION = new ColoredCoinsTr.ColoredCoinsOrderCancellationTr() {
        @Override
        public final byte getSubtype() {
            return TransactionType.SUBTYPE_COLORED_COINS_ASK_ORDER_CANCELLATION;
        }

        @Override
        public AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.ASSET_ASK_ORDER_CANCELLATION;
        }

        @Override
        public String getName() {
            return "AskOrderCancellation";
        }

        @Override
        public ColoredCoinsAskOrderCancellation parseAttachment(ByteBuffer buffer) {
            return new ColoredCoinsAskOrderCancellation(buffer);
        }

        @Override
        public ColoredCoinsAskOrderCancellation parseAttachment(JSONObject attachmentData) {
            return new ColoredCoinsAskOrderCancellation(attachmentData);
        }

        @Override
        public void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {
            ColoredCoinsAskOrderCancellation attachment = (ColoredCoinsAskOrderCancellation) transaction.getAttachment();
            Order order = Order.Ask.getAskOrder(attachment.getOrderId());
            Order.Ask.removeOrder(attachment.getOrderId());
            if (order != null) {
                senderAccount.addToUnconfirmedAssetBalanceQNT(getLedgerEvent(), transaction.getId(), order.getAssetId(), order.getQuantityQNT());
            }
        }

        @Override
        public void validateAttachment(Transaction transaction) throws FbcException.ValidationException {
            ColoredCoinsAskOrderCancellation attachment = (ColoredCoinsAskOrderCancellation) transaction.getAttachment();
            Order ask = Order.Ask.getAskOrder(attachment.getOrderId());
            if (ask == null) {
                throw new FbcException.NotCurrentlyValidException("Invalid ask order: " + Long.toUnsignedString(attachment.getOrderId()));
            }
            if (ask.getAccountId() != transaction.getSenderId()) {
                throw new FbcException.NotValidException("Order " + Long.toUnsignedString(attachment.getOrderId()) + " was created by account " + Long.toUnsignedString(ask.getAccountId()));
            }
        }
    };
    public static final TransactionType BID_ORDER_CANCELLATION = new ColoredCoinsTr.ColoredCoinsOrderCancellationTr() {
        @Override
        public final byte getSubtype() {
            return TransactionType.SUBTYPE_COLORED_COINS_BID_ORDER_CANCELLATION;
        }

        @Override
        public AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.ASSET_BID_ORDER_CANCELLATION;
        }

        @Override
        public String getName() {
            return "BidOrderCancellation";
        }

        @Override
        public ColoredCoinsBidOrderCancellation parseAttachment(ByteBuffer buffer) {
            return new ColoredCoinsBidOrderCancellation(buffer);
        }

        @Override
        public ColoredCoinsBidOrderCancellation parseAttachment(JSONObject attachmentData) {
            return new ColoredCoinsBidOrderCancellation(attachmentData);
        }

        @Override
        public void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {
            ColoredCoinsBidOrderCancellation attachment = (ColoredCoinsBidOrderCancellation) transaction.getAttachment();
            Order order = Order.Bid.getBidOrder(attachment.getOrderId());
            Order.Bid.removeOrder(attachment.getOrderId());
            if (order != null) {
                senderAccount.addToUnconfirmedBalanceNQT(getLedgerEvent(), transaction.getId(), Math.multiplyExact(order.getQuantityQNT(), order.getPriceNQT()));
            }
        }

        @Override
        public void validateAttachment(Transaction transaction) throws FbcException.ValidationException {
            ColoredCoinsBidOrderCancellation attachment = (ColoredCoinsBidOrderCancellation) transaction.getAttachment();
            Order bid = Order.Bid.getBidOrder(attachment.getOrderId());
            if (bid == null) {
                throw new FbcException.NotCurrentlyValidException("Invalid bid order: " + Long.toUnsignedString(attachment.getOrderId()));
            }
            if (bid.getAccountId() != transaction.getSenderId()) {
                throw new FbcException.NotValidException("Order " + Long.toUnsignedString(attachment.getOrderId()) + " was created by account " + Long.toUnsignedString(bid.getAccountId()));
            }
        }
    };
    public static final TransactionType DIVIDEND_PAYMENT = new ColoredCoinsTr() {
        @Override
        public final byte getSubtype() {
            return TransactionType.SUBTYPE_COLORED_COINS_DIVIDEND_PAYMENT;
        }

        @Override
        public AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.ASSET_DIVIDEND_PAYMENT;
        }

        @Override
        public String getName() {
            return "DividendPayment";
        }

        @Override
        public ColoredCoinsDividendPayment parseAttachment(ByteBuffer buffer) {
            return new ColoredCoinsDividendPayment(buffer);
        }

        @Override
        public ColoredCoinsDividendPayment parseAttachment(JSONObject attachmentData) {
            return new ColoredCoinsDividendPayment(attachmentData);
        }

        @Override
        public boolean applyAttachmentUnconfirmed(Transaction transaction, Account senderAccount) {
            ColoredCoinsDividendPayment attachment = (ColoredCoinsDividendPayment) transaction.getAttachment();
            long assetId = attachment.getAssetId();
            Asset asset = Asset.getAsset(assetId, attachment.getHeight());
            if (asset == null) {
                return true;
            }
            long quantityQNT = asset.getQuantityQNT() - senderAccount.getAssetBalanceQNT(assetId, attachment.getHeight());
            long totalDividendPayment = Math.multiplyExact(attachment.getAmountNQTPerQNT(), quantityQNT);
            if (senderAccount.getUnconfirmedBalanceNQT() >= totalDividendPayment) {
                senderAccount.addToUnconfirmedBalanceNQT(getLedgerEvent(), transaction.getId(), -totalDividendPayment);
                return true;
            }
            return false;
        }

        @Override
        public void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {
            ColoredCoinsDividendPayment attachment = (ColoredCoinsDividendPayment) transaction.getAttachment();
            senderAccount.payDividends(transaction.getId(), attachment);
        }

        @Override
        public void undoAttachmentUnconfirmed(Transaction transaction, Account senderAccount) {
            ColoredCoinsDividendPayment attachment = (ColoredCoinsDividendPayment) transaction.getAttachment();
            long assetId = attachment.getAssetId();
            Asset asset = Asset.getAsset(assetId, attachment.getHeight());
            if (asset == null) {
                return;
            }
            long quantityQNT = asset.getQuantityQNT() - senderAccount.getAssetBalanceQNT(assetId, attachment.getHeight());
            long totalDividendPayment = Math.multiplyExact(attachment.getAmountNQTPerQNT(), quantityQNT);
            senderAccount.addToUnconfirmedBalanceNQT(getLedgerEvent(), transaction.getId(), totalDividendPayment);
        }

        @Override
        public void validateAttachment(Transaction transaction) throws FbcException.ValidationException {
            ColoredCoinsDividendPayment attachment = (ColoredCoinsDividendPayment) transaction.getAttachment();
            if (attachment.getHeight() > DiH.getBlockchain().getHeight()) {
                throw new FbcException.NotCurrentlyValidException("Invalid dividend payment height: " + attachment.getHeight() + ", must not exceed current blockchain height " + DiH.getBlockchain().getHeight());
            }
            if (attachment.getHeight() <= attachment.getFinishValidationHeight(transaction) - Constants.MAX_DIVIDEND_PAYMENT_ROLLBACK) {
                throw new FbcException.NotCurrentlyValidException("Invalid dividend payment height: " + attachment.getHeight() + ", must be less than " + Constants.MAX_DIVIDEND_PAYMENT_ROLLBACK + " blocks before " + attachment.getFinishValidationHeight(transaction));
            }
            Asset asset = Asset.getAsset(attachment.getAssetId(), attachment.getHeight());
            if (asset == null) {
                throw new FbcException.NotCurrentlyValidException("Asset " + Long.toUnsignedString(attachment.getAssetId()) + " for dividend payment doesn't exist yet");
            }
            if (asset.getAccountId() != transaction.getSenderId() || attachment.getAmountNQTPerQNT() <= 0) {
                throw new FbcException.NotValidException("Invalid dividend payment sender or amount " + attachment.getJSONObject());
            }
            AssetDividend lastDividend = AssetDividend.getLastDividend(attachment.getAssetId());
            if (lastDividend != null && lastDividend.getHeight() > DiH.getBlockchain().getHeight() - 60) {
                throw new FbcException.NotCurrentlyValidException("Last dividend payment for asset " + Long.toUnsignedString(attachment.getAssetId()) + " was less than 60 blocks ago at " + lastDividend.getHeight() + ", current height is " + DiH.getBlockchain().getHeight() + ", limit is one dividend per 60 blocks");
            }
        }

        @Override
        public boolean isDuplicate(Transaction transaction, Map<TransactionType, Map<String, Integer>> duplicates) {
            ColoredCoinsDividendPayment attachment = (ColoredCoinsDividendPayment) transaction.getAttachment();
            return isDuplicate(ColoredCoinsTr.DIVIDEND_PAYMENT, Long.toUnsignedString(attachment.getAssetId()), duplicates, true);
        }

        @Override
        public boolean canHaveRecipient() {
            return false;
        }

        @Override
        public boolean isPhasingSafe() {
            return false;
        }
    };
    
}
