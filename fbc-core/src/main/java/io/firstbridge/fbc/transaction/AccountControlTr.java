package io.firstbridge.fbc.transaction;

import io.firstbridge.fbc.core.account.Account;
import io.firstbridge.fbc.core.account.AccountLedger;
import io.firstbridge.fbc.core.account.AccountRestrictions;
import io.firstbridge.fbc.core.Genesis;
import io.firstbridge.fbc.core.VoteWeighting;
import io.firstbridge.fbc.core.interfaces.Transaction;
import io.firstbridge.fbc.core.messages.AbstractAttachment;
import io.firstbridge.fbc.core.messages.AccountControlEffectiveBalanceLeasing;
import io.firstbridge.fbc.core.messages.SetPhasingOnly;
import io.firstbridge.fbc.util.Constants;
import io.firstbridge.fbc.util.FbcException;
import java.nio.ByteBuffer;
import java.util.Map;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public abstract class AccountControlTr extends TransactionType {
    
    private AccountControlTr() {
    }

    @Override
    public final byte getType() {
        return TransactionType.TYPE_ACCOUNT_CONTROL;
    }

    @Override
    public final boolean applyAttachmentUnconfirmed(Transaction transaction, Account senderAccount) {
        return true;
    }

    @Override
    public final void undoAttachmentUnconfirmed(Transaction transaction, Account senderAccount) {
    }
    public static final TransactionType EFFECTIVE_BALANCE_LEASING = new AccountControlTr() {
        @Override
        public final byte getSubtype() {
            return TransactionType.SUBTYPE_ACCOUNT_CONTROL_EFFECTIVE_BALANCE_LEASING;
        }

        @Override
        public AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.ACCOUNT_CONTROL_EFFECTIVE_BALANCE_LEASING;
        }

        @Override
        public String getName() {
            return "EffectiveBalanceLeasing";
        }

        @Override
        public AccountControlEffectiveBalanceLeasing parseAttachment(ByteBuffer buffer) {
            return new AccountControlEffectiveBalanceLeasing(buffer);
        }

        @Override
        public AccountControlEffectiveBalanceLeasing parseAttachment(JSONObject attachmentData) {
            return new AccountControlEffectiveBalanceLeasing(attachmentData);
        }

        @Override
        public void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {
            AccountControlEffectiveBalanceLeasing attachment = (AccountControlEffectiveBalanceLeasing) transaction.getAttachment();
            Account.getAccount(transaction.getSenderId()).leaseEffectiveBalance(transaction.getRecipientId(), attachment.getPeriod());
        }

        @Override
        public void validateAttachment(Transaction transaction) throws FbcException.ValidationException {
            AccountControlEffectiveBalanceLeasing attachment = (AccountControlEffectiveBalanceLeasing) transaction.getAttachment();
            if (transaction.getSenderId() == transaction.getRecipientId()) {
                throw new FbcException.NotValidException("Account cannot lease balance to itself");
            }
            if (transaction.getAmountNQT() != 0) {
                throw new FbcException.NotValidException("Transaction amount must be 0 for effective balance leasing");
            }
            if (attachment.getPeriod() < Constants.LEASING_DELAY || attachment.getPeriod() > 65535) {
                throw new FbcException.NotValidException("Invalid effective balance leasing period: " + attachment.getPeriod());
            }
            byte[] recipientPublicKey = Account.getPublicKey(transaction.getRecipientId());
            if (recipientPublicKey == null) {
                throw new FbcException.NotCurrentlyValidException("Invalid effective balance leasing: " + " recipient account " + Long.toUnsignedString(transaction.getRecipientId()) + " not found or no public key published");
            }
            if (transaction.getRecipientId() == Genesis.CREATOR_ID) {
                throw new FbcException.NotValidException("Leasing to Genesis account not allowed");
            }
        }

        @Override
        public boolean canHaveRecipient() {
            return true;
        }

        @Override
        public boolean isPhasingSafe() {
            return true;
        }
    };
    public static final TransactionType SET_PHASING_ONLY = new AccountControlTr() {
        @Override
        public byte getSubtype() {
            return SUBTYPE_ACCOUNT_CONTROL_PHASING_ONLY;
        }

        @Override
        public AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.ACCOUNT_CONTROL_PHASING_ONLY;
        }

        @Override
        public AbstractAttachment parseAttachment(ByteBuffer buffer) {
            return new SetPhasingOnly(buffer);
        }

        @Override
        public AbstractAttachment parseAttachment(JSONObject attachmentData) {
            return new SetPhasingOnly(attachmentData);
        }

        @Override
        public void validateAttachment(Transaction transaction) throws FbcException.ValidationException {
            SetPhasingOnly attachment = (SetPhasingOnly) transaction.getAttachment();
            VoteWeighting.VotingModel votingModel = attachment.getPhasingParams().getVoteWeighting().getVotingModel();
            attachment.getPhasingParams().validate();
            if (votingModel == VoteWeighting.VotingModel.NONE) {
                Account senderAccount = Account.getAccount(transaction.getSenderId());
                if (senderAccount == null || !senderAccount.getControls().contains(Account.ControlType.PHASING_ONLY)) {
                    throw new FbcException.NotCurrentlyValidException("Phasing only account control is not currently enabled");
                }
            } else if (votingModel == VoteWeighting.VotingModel.TRANSACTION || votingModel == VoteWeighting.VotingModel.HASH) {
                throw new FbcException.NotValidException("Invalid voting model " + votingModel + " for account control");
            }
            long maxFees = attachment.getMaxFees();
            long maxFeesLimit = (attachment.getPhasingParams().getVoteWeighting().isBalanceIndependent() ? 3 : 22) * Constants.ONE_NXT;
            if (maxFees < 0 || (maxFees > 0 && maxFees < maxFeesLimit) || maxFees > Constants.MAX_BALANCE_NQT) {
                throw new FbcException.NotValidException(String.format("Invalid max fees %f %s", ((double) maxFees) / Constants.ONE_NXT, Constants.COIN_SYMBOL));
            }
            short minDuration = attachment.getMinDuration();
            if (minDuration < 0 || (minDuration > 0 && minDuration < 3) || minDuration >= Constants.MAX_PHASING_DURATION) {
                throw new FbcException.NotValidException("Invalid min duration " + attachment.getMinDuration());
            }
            short maxDuration = attachment.getMaxDuration();
            if (maxDuration < 0 || (maxDuration > 0 && maxDuration < 3) || maxDuration >= Constants.MAX_PHASING_DURATION) {
                throw new FbcException.NotValidException("Invalid max duration " + maxDuration);
            }
            if (minDuration > maxDuration) {
                throw new FbcException.NotValidException(String.format("Min duration %d cannot exceed max duration %d ", minDuration, maxDuration));
            }
        }

        @Override
        public boolean isDuplicate(Transaction transaction, Map<TransactionType, Map<String, Integer>> duplicates) {
            return TransactionType.isDuplicate(SET_PHASING_ONLY, Long.toUnsignedString(transaction.getSenderId()), duplicates, true);
        }

        @Override
        public void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {
            SetPhasingOnly attachment = (SetPhasingOnly) transaction.getAttachment();
            AccountRestrictions.PhasingOnly.set(senderAccount, attachment);
        }

        @Override
        public boolean canHaveRecipient() {
            return false;
        }

        @Override
        public String getName() {
            return "SetPhasingOnly";
        }

        @Override
        public boolean isPhasingSafe() {
            return false;
        }
    };
    
}
