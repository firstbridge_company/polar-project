package io.firstbridge.fbc.transaction;

import io.firstbridge.fbc.core.account.Account;
import io.firstbridge.fbc.core.account.AccountLedger;
import io.firstbridge.fbc.core.Alias;
import io.firstbridge.fbc.core.Genesis;
import io.firstbridge.fbc.core.PhasingPoll;
import io.firstbridge.fbc.core.PhasingVote;
import io.firstbridge.fbc.core.Poll;
import io.firstbridge.fbc.core.Vote;
import io.firstbridge.fbc.core.VoteWeighting;
import io.firstbridge.fbc.core.interfaces.Fee;
import io.firstbridge.fbc.core.SizeBasedFee;
import io.firstbridge.fbc.core.interfaces.Transaction;
import io.firstbridge.fbc.core.messages.Appendix;
import io.firstbridge.fbc.core.messages.Attachment;
import io.firstbridge.fbc.core.messages.EmptyAttachment;
import io.firstbridge.fbc.core.messages.MessagingAccountInfo;
import io.firstbridge.fbc.core.messages.MessagingAccountProperty;
import io.firstbridge.fbc.core.messages.MessagingAccountPropertyDelete;
import io.firstbridge.fbc.core.messages.MessagingAliasAssignment;
import io.firstbridge.fbc.core.messages.MessagingAliasBuy;
import io.firstbridge.fbc.core.messages.MessagingAliasDelete;
import io.firstbridge.fbc.core.messages.MessagingAliasSell;
import io.firstbridge.fbc.core.messages.MessagingPhasingVoteCasting;
import io.firstbridge.fbc.core.messages.MessagingPollCreation;
import io.firstbridge.fbc.core.messages.MessagingVoteCasting;
import io.firstbridge.fbc.util.Constants;
import io.firstbridge.fbc.crypto.util.Convert;
import io.firstbridge.fbc.util.FbcException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.simple.JSONObject;

/**
 *
 * @author al
 */
public abstract class MessagingTr extends TransactionType {
    
    private MessagingTr() {
    }

    @Override
    public final byte getType() {
        return TransactionType.TYPE_MESSAGING;
    }

    @Override
    public final boolean applyAttachmentUnconfirmed(Transaction transaction, Account senderAccount) {
        return true;
    }

    @Override
    public final void undoAttachmentUnconfirmed(Transaction transaction, Account senderAccount) {
    }
    public static final TransactionType ARBITRARY_MESSAGE = new MessagingTr() {
        @Override
        public final byte getSubtype() {
            return TransactionType.SUBTYPE_MESSAGING_ARBITRARY_MESSAGE;
        }

        @Override
        public AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.ARBITRARY_MESSAGE;
        }

        @Override
        public String getName() {
            return "ArbitraryMessage";
        }

        @Override
        public EmptyAttachment parseAttachment(ByteBuffer buffer) {
            return Attachment.ARBITRARY_MESSAGE;
        }

        @Override
        public EmptyAttachment parseAttachment(JSONObject attachmentData) {
            return Attachment.ARBITRARY_MESSAGE;
        }

        @Override
        public void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {
        }

        @Override
        public void validateAttachment(Transaction transaction) throws FbcException.ValidationException {
            Attachment attachment = transaction.getAttachment();
            if (transaction.getAmountNQT() != 0) {
                throw new FbcException.NotValidException("Invalid arbitrary message: " + attachment.getJSONObject());
            }
            if (transaction.getRecipientId() == Genesis.CREATOR_ID) {
                throw new FbcException.NotValidException("Sending messages to Genesis not allowed.");
            }
        }

        @Override
        public boolean canHaveRecipient() {
            return true;
        }

        @Override
        public boolean mustHaveRecipient() {
            return false;
        }

        @Override
        public boolean isPhasingSafe() {
            return false;
        }
    };
    public static final TransactionType ALIAS_ASSIGNMENT = new MessagingTr() {
        private final Fee ALIAS_FEE = new SizeBasedFee(2 * Constants.ONE_NXT, 2 * Constants.ONE_NXT, 32) {
            @Override
            public int getSize(Transaction transaction, Appendix appendage) {
                MessagingAliasAssignment attachment = (MessagingAliasAssignment) transaction.getAttachment();
                return attachment.getAliasName().length() + attachment.getAliasURI().length();
            }
        };

        @Override
        public final byte getSubtype() {
            return TransactionType.SUBTYPE_MESSAGING_ALIAS_ASSIGNMENT;
        }

        @Override
        public AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.ALIAS_ASSIGNMENT;
        }

        @Override
        public String getName() {
            return "AliasAssignment";
        }

        @Override
        public Fee getBaselineFee(Transaction transaction) {
            return ALIAS_FEE;
        }

        @Override
        public MessagingAliasAssignment parseAttachment(ByteBuffer buffer) throws FbcException.NotValidException {
            return new MessagingAliasAssignment(buffer);
        }

        @Override
        public MessagingAliasAssignment parseAttachment(JSONObject attachmentData) {
            return new MessagingAliasAssignment(attachmentData);
        }

        @Override
        public void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {
            MessagingAliasAssignment attachment = (MessagingAliasAssignment) transaction.getAttachment();
            Alias.addOrUpdateAlias(transaction, attachment);
        }

        @Override
        public boolean isDuplicate(Transaction transaction, Map<TransactionType, Map<String, Integer>> duplicates) {
            MessagingAliasAssignment attachment = (MessagingAliasAssignment) transaction.getAttachment();
            return isDuplicate(MessagingTr.ALIAS_ASSIGNMENT, attachment.getAliasName().toLowerCase(Locale.ROOT), duplicates, true);
        }

        @Override
        public boolean isBlockDuplicate(Transaction transaction, Map<TransactionType, Map<String, Integer>> duplicates) {
            return Alias.getAlias(((MessagingAliasAssignment) transaction.getAttachment()).getAliasName()) == null && isDuplicate(MessagingTr.ALIAS_ASSIGNMENT, "", duplicates, true);
        }

        @Override
        public void validateAttachment(Transaction transaction) throws FbcException.ValidationException {
            MessagingAliasAssignment attachment = (MessagingAliasAssignment) transaction.getAttachment();
            if (attachment.getAliasName().length() == 0 || attachment.getAliasName().length() > Constants.MAX_ALIAS_LENGTH || attachment.getAliasURI().length() > Constants.MAX_ALIAS_URI_LENGTH) {
                throw new FbcException.NotValidException("Invalid alias assignment: " + attachment.getJSONObject());
            }
            String normalizedAlias = attachment.getAliasName().toLowerCase(Locale.ROOT);
            for (int i = 0; i < normalizedAlias.length(); i++) {
                if (Constants.ALPHABET.indexOf(normalizedAlias.charAt(i)) < 0) {
                    throw new FbcException.NotValidException("Invalid alias name: " + normalizedAlias);
                }
            }
            Alias alias = Alias.getAlias(normalizedAlias);
            if (alias != null && alias.getAccountId() != transaction.getSenderId()) {
                throw new FbcException.NotCurrentlyValidException("Alias already owned by another account: " + normalizedAlias);
            }
        }

        @Override
        public boolean canHaveRecipient() {
            return false;
        }

        @Override
        public boolean isPhasingSafe() {
            return false;
        }
    };
    public static final TransactionType ALIAS_SELL = new MessagingTr() {
        @Override
        public final byte getSubtype() {
            return TransactionType.SUBTYPE_MESSAGING_ALIAS_SELL;
        }

        @Override
        public AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.ALIAS_SELL;
        }

        @Override
        public String getName() {
            return "AliasSell";
        }

        @Override
        public MessagingAliasSell parseAttachment(ByteBuffer buffer) throws FbcException.NotValidException {
            return new MessagingAliasSell(buffer);
        }

        @Override
        public MessagingAliasSell parseAttachment(JSONObject attachmentData) {
            return new MessagingAliasSell(attachmentData);
        }

        @Override
        public void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {
            MessagingAliasSell attachment = (MessagingAliasSell) transaction.getAttachment();
            Alias.sellAlias(transaction, attachment);
        }

        @Override
        public boolean isDuplicate(Transaction transaction, Map<TransactionType, Map<String, Integer>> duplicates) {
            MessagingAliasSell attachment = (MessagingAliasSell) transaction.getAttachment();
            // not a bug, uniqueness is based on Messaging.ALIAS_ASSIGNMENT
            return isDuplicate(MessagingTr.ALIAS_ASSIGNMENT, attachment.getAliasName().toLowerCase(Locale.ROOT), duplicates, true);
        }

        @Override
        public void validateAttachment(Transaction transaction) throws FbcException.ValidationException {
            if (transaction.getAmountNQT() != 0) {
                throw new FbcException.NotValidException("Invalid sell alias transaction: " + transaction.getJSONObject());
            }
            final MessagingAliasSell attachment = (MessagingAliasSell) transaction.getAttachment();
            final String aliasName = attachment.getAliasName();
            if (aliasName == null || aliasName.length() == 0) {
                throw new FbcException.NotValidException("Missing alias name");
            }
            long priceNQT = attachment.getPriceNQT();
            if (priceNQT < 0 || priceNQT > Constants.MAX_BALANCE_NQT) {
                throw new FbcException.NotValidException("Invalid alias sell price: " + priceNQT);
            }
            if (priceNQT == 0) {
                if (Genesis.CREATOR_ID == transaction.getRecipientId()) {
                    throw new FbcException.NotValidException("Transferring aliases to Genesis account not allowed");
                } else if (transaction.getRecipientId() == 0) {
                    throw new FbcException.NotValidException("Missing alias transfer recipient");
                }
            }
            final Alias alias = Alias.getAlias(aliasName);
            if (alias == null) {
                throw new FbcException.NotCurrentlyValidException("No such alias: " + aliasName);
            } else if (alias.getAccountId() != transaction.getSenderId()) {
                throw new FbcException.NotCurrentlyValidException("Alias doesn't belong to sender: " + aliasName);
            }
            if (transaction.getRecipientId() == Genesis.CREATOR_ID) {
                throw new FbcException.NotValidException("Selling alias to Genesis not allowed");
            }
        }

        @Override
        public boolean canHaveRecipient() {
            return true;
        }

        @Override
        public boolean mustHaveRecipient() {
            return false;
        }

        @Override
        public boolean isPhasingSafe() {
            return false;
        }
    };
    public static final TransactionType ALIAS_BUY = new MessagingTr() {
        @Override
        public final byte getSubtype() {
            return TransactionType.SUBTYPE_MESSAGING_ALIAS_BUY;
        }

        @Override
        public AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.ALIAS_BUY;
        }

        @Override
        public String getName() {
            return "AliasBuy";
        }

        @Override
        public MessagingAliasBuy parseAttachment(ByteBuffer buffer) throws FbcException.NotValidException {
            return new MessagingAliasBuy(buffer);
        }

        @Override
        public MessagingAliasBuy parseAttachment(JSONObject attachmentData) {
            return new MessagingAliasBuy(attachmentData);
        }

        @Override
        public void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {
            final MessagingAliasBuy attachment = (MessagingAliasBuy) transaction.getAttachment();
            final String aliasName = attachment.getAliasName();
            Alias.changeOwner(transaction.getSenderId(), aliasName);
        }

        @Override
        public boolean isDuplicate(Transaction transaction, Map<TransactionType, Map<String, Integer>> duplicates) {
            MessagingAliasBuy attachment = (MessagingAliasBuy) transaction.getAttachment();
            // not a bug, uniqueness is based on Messaging.ALIAS_ASSIGNMENT
            return isDuplicate(MessagingTr.ALIAS_ASSIGNMENT, attachment.getAliasName().toLowerCase(Locale.ROOT), duplicates, true);
        }

        @Override
        public void validateAttachment(Transaction transaction) throws FbcException.ValidationException {
            final MessagingAliasBuy attachment = (MessagingAliasBuy) transaction.getAttachment();
            final String aliasName = attachment.getAliasName();
            final Alias alias = Alias.getAlias(aliasName);
            if (alias == null) {
                throw new FbcException.NotCurrentlyValidException("No such alias: " + aliasName);
            } else if (alias.getAccountId() != transaction.getRecipientId()) {
                throw new FbcException.NotCurrentlyValidException("Alias is owned by account other than recipient: " + Long.toUnsignedString(alias.getAccountId()));
            }
            Alias.Offer offer = Alias.getOffer(alias);
            if (offer == null) {
                throw new FbcException.NotCurrentlyValidException("Alias is not for sale: " + aliasName);
            }
            if (transaction.getAmountNQT() < offer.getPriceNQT()) {
                String msg = "Price is too low for: " + aliasName + " (" + transaction.getAmountNQT() + " < " + offer.getPriceNQT() + ")";
                throw new FbcException.NotCurrentlyValidException(msg);
            }
            if (offer.getBuyerId() != 0 && offer.getBuyerId() != transaction.getSenderId()) {
                throw new FbcException.NotCurrentlyValidException("Wrong buyer for " + aliasName + ": " + Long.toUnsignedString(transaction.getSenderId()) + " expected: " + Long.toUnsignedString(offer.getBuyerId()));
            }
        }

        @Override
        public boolean canHaveRecipient() {
            return true;
        }

        @Override
        public boolean isPhasingSafe() {
            return false;
        }
    };
    public static final TransactionType ALIAS_DELETE = new MessagingTr() {
        @Override
        public final byte getSubtype() {
            return TransactionType.SUBTYPE_MESSAGING_ALIAS_DELETE;
        }

        @Override
        public AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.ALIAS_DELETE;
        }

        @Override
        public String getName() {
            return "AliasDelete";
        }

        @Override
        public MessagingAliasDelete parseAttachment(final ByteBuffer buffer) throws FbcException.NotValidException {
            return new MessagingAliasDelete(buffer);
        }

        @Override
        public MessagingAliasDelete parseAttachment(final JSONObject attachmentData) {
            return new MessagingAliasDelete(attachmentData);
        }

        @Override
        public void applyAttachment(final Transaction transaction, final Account senderAccount, final Account recipientAccount) {
            final MessagingAliasDelete attachment = (MessagingAliasDelete) transaction.getAttachment();
            Alias.deleteAlias(attachment.getAliasName());
        }

        @Override
        public boolean isDuplicate(final Transaction transaction, final Map<TransactionType, Map<String, Integer>> duplicates) {
            MessagingAliasDelete attachment = (MessagingAliasDelete) transaction.getAttachment();
            // not a bug, uniqueness is based on Messaging.ALIAS_ASSIGNMENT
            return isDuplicate(MessagingTr.ALIAS_ASSIGNMENT, attachment.getAliasName().toLowerCase(Locale.ROOT), duplicates, true);
        }

        @Override
        public void validateAttachment(final Transaction transaction) throws FbcException.ValidationException {
            final MessagingAliasDelete attachment = (MessagingAliasDelete) transaction.getAttachment();
            final String aliasName = attachment.getAliasName();
            if (aliasName == null || aliasName.length() == 0) {
                throw new FbcException.NotValidException("Missing alias name");
            }
            final Alias alias = Alias.getAlias(aliasName);
            if (alias == null) {
                throw new FbcException.NotCurrentlyValidException("No such alias: " + aliasName);
            } else if (alias.getAccountId() != transaction.getSenderId()) {
                throw new FbcException.NotCurrentlyValidException("Alias doesn't belong to sender: " + aliasName);
            }
        }

        @Override
        public boolean canHaveRecipient() {
            return false;
        }

        @Override
        public boolean isPhasingSafe() {
            return false;
        }
    };
    public static final TransactionType POLL_CREATION = new MessagingTr() {
        private final Fee POLL_OPTIONS_FEE = new SizeBasedFee(10 * Constants.ONE_NXT, Constants.ONE_NXT, 1) {
            @Override
            public int getSize(Transaction transaction, Appendix appendage) {
                int numOptions = ((MessagingPollCreation) appendage).getPollOptions().length;
                return numOptions <= 19 ? 0 : numOptions - 19;
            }
        };
        private final Fee POLL_SIZE_FEE = new SizeBasedFee(0, 2 * Constants.ONE_NXT, 32) {
            @Override
            public int getSize(Transaction transaction, Appendix appendage) {
                MessagingPollCreation attachment = (MessagingPollCreation) appendage;
                int size = attachment.getPollName().length() + attachment.getPollDescription().length();
                for (String option : ((MessagingPollCreation) appendage).getPollOptions()) {
                    size += option.length();
                }
                return size <= 288 ? 0 : size - 288;
            }
        };
        private final Fee POLL_FEE = (transaction, appendage) -> POLL_OPTIONS_FEE.getFee(transaction, appendage) + POLL_SIZE_FEE.getFee(transaction, appendage);

        @Override
        public final byte getSubtype() {
            return TransactionType.SUBTYPE_MESSAGING_POLL_CREATION;
        }

        @Override
        public AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.POLL_CREATION;
        }

        @Override
        public String getName() {
            return "PollCreation";
        }

        @Override
        public Fee getBaselineFee(Transaction transaction) {
            return POLL_FEE;
        }

        @Override
        public MessagingPollCreation parseAttachment(ByteBuffer buffer) throws FbcException.NotValidException {
            return new MessagingPollCreation(buffer);
        }

        @Override
        public MessagingPollCreation parseAttachment(JSONObject attachmentData) {
            return new MessagingPollCreation(attachmentData);
        }

        @Override
        public void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {
            MessagingPollCreation attachment = (MessagingPollCreation) transaction.getAttachment();
            Poll.addPoll(transaction, attachment);
        }

        @Override
        public void validateAttachment(Transaction transaction) throws FbcException.ValidationException {
            MessagingPollCreation attachment = (MessagingPollCreation) transaction.getAttachment();
            int optionsCount = attachment.getPollOptions().length;
            if (attachment.getPollName().length() > Constants.MAX_POLL_NAME_LENGTH || attachment.getPollName().isEmpty() || attachment.getPollDescription().length() > Constants.MAX_POLL_DESCRIPTION_LENGTH || optionsCount > Constants.MAX_POLL_OPTION_COUNT || optionsCount == 0) {
                throw new FbcException.NotValidException("Invalid poll attachment: " + attachment.getJSONObject());
            }
            if (attachment.getMinNumberOfOptions() < 1 || attachment.getMinNumberOfOptions() > optionsCount) {
                throw new FbcException.NotValidException("Invalid min number of options: " + attachment.getJSONObject());
            }
            if (attachment.getMaxNumberOfOptions() < 1 || attachment.getMaxNumberOfOptions() < attachment.getMinNumberOfOptions() || attachment.getMaxNumberOfOptions() > optionsCount) {
                throw new FbcException.NotValidException("Invalid max number of options: " + attachment.getJSONObject());
            }
            for (int i = 0; i < optionsCount; i++) {
                if (attachment.getPollOptions()[i].length() > Constants.MAX_POLL_OPTION_LENGTH || attachment.getPollOptions()[i].isEmpty()) {
                    throw new FbcException.NotValidException("Invalid poll options length: " + attachment.getJSONObject());
                }
            }
            if (attachment.getMinRangeValue() < Constants.MIN_VOTE_VALUE || attachment.getMaxRangeValue() > Constants.MAX_VOTE_VALUE || attachment.getMaxRangeValue() < attachment.getMinRangeValue()) {
                throw new FbcException.NotValidException("Invalid range: " + attachment.getJSONObject());
            }
            if (attachment.getFinishHeight() <= attachment.getFinishValidationHeight(transaction) + 1 || attachment.getFinishHeight() >= attachment.getFinishValidationHeight(transaction) + Constants.MAX_POLL_DURATION) {
                throw new FbcException.NotCurrentlyValidException("Invalid finishing height" + attachment.getJSONObject());
            }
            if (!attachment.getVoteWeighting().acceptsVotes() || attachment.getVoteWeighting().getVotingModel() == VoteWeighting.VotingModel.HASH) {
                throw new FbcException.NotValidException("VotingModel " + attachment.getVoteWeighting().getVotingModel() + " not valid for regular polls");
            }
            attachment.getVoteWeighting().validate();
        }

        @Override
        public boolean isBlockDuplicate(Transaction transaction, Map<TransactionType, Map<String, Integer>> duplicates) {
            return isDuplicate(MessagingTr.POLL_CREATION, getName(), duplicates, true);
        }

        @Override
        public boolean canHaveRecipient() {
            return false;
        }

        @Override
        public boolean isPhasingSafe() {
            return false;
        }
    };
    public static final TransactionType VOTE_CASTING = new MessagingTr() {
        @Override
        public final byte getSubtype() {
            return TransactionType.SUBTYPE_MESSAGING_VOTE_CASTING;
        }

        @Override
        public AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.VOTE_CASTING;
        }

        @Override
        public String getName() {
            return "VoteCasting";
        }

        @Override
        public MessagingVoteCasting parseAttachment(ByteBuffer buffer) throws FbcException.NotValidException {
            return new MessagingVoteCasting(buffer);
        }

        @Override
        public MessagingVoteCasting parseAttachment(JSONObject attachmentData) {
            return new MessagingVoteCasting(attachmentData);
        }

        @Override
        public void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {
            MessagingVoteCasting attachment = (MessagingVoteCasting) transaction.getAttachment();
            Vote.addVote(transaction, attachment);
        }

        @Override
        public void validateAttachment(Transaction transaction) throws FbcException.ValidationException {
            MessagingVoteCasting attachment = (MessagingVoteCasting) transaction.getAttachment();
            if (attachment.getPollId() == 0 || attachment.getPollVote() == null || attachment.getPollVote().length > Constants.MAX_POLL_OPTION_COUNT) {
                throw new FbcException.NotValidException("Invalid vote casting attachment: " + attachment.getJSONObject());
            }
            long pollId = attachment.getPollId();
            Poll poll = Poll.getPoll(pollId);
            if (poll == null) {
                throw new FbcException.NotCurrentlyValidException("Invalid poll: " + Long.toUnsignedString(attachment.getPollId()));
            }
            if (Vote.getVote(pollId, transaction.getSenderId()) != null) {
                throw new FbcException.NotCurrentlyValidException("Double voting attempt");
            }
            if (poll.getFinishHeight() <= attachment.getFinishValidationHeight(transaction)) {
                throw new FbcException.NotCurrentlyValidException("Voting for this poll finishes at " + poll.getFinishHeight());
            }
            byte[] votes = attachment.getPollVote();
            int positiveCount = 0;
            for (byte vote : votes) {
                if (vote != Constants.NO_VOTE_VALUE && (vote < poll.getMinRangeValue() || vote > poll.getMaxRangeValue())) {
                    throw new FbcException.NotValidException(String.format("Invalid vote %d, vote must be between %d and %d", vote, poll.getMinRangeValue(), poll.getMaxRangeValue()));
                }
                if (vote != Constants.NO_VOTE_VALUE) {
                    positiveCount++;
                }
            }
            if (positiveCount < poll.getMinNumberOfOptions() || positiveCount > poll.getMaxNumberOfOptions()) {
                throw new FbcException.NotValidException(String.format("Invalid num of choices %d, number of choices must be between %d and %d", positiveCount, poll.getMinNumberOfOptions(), poll.getMaxNumberOfOptions()));
            }
        }

        @Override
        public boolean isDuplicate(final Transaction transaction, final Map<TransactionType, Map<String, Integer>> duplicates) {
            MessagingVoteCasting attachment = (MessagingVoteCasting) transaction.getAttachment();
            String key = Long.toUnsignedString(attachment.getPollId()) + ":" + Long.toUnsignedString(transaction.getSenderId());
            return isDuplicate(MessagingTr.VOTE_CASTING, key, duplicates, true);
        }

        @Override
        public boolean canHaveRecipient() {
            return false;
        }

        @Override
        public boolean isPhasingSafe() {
            return false;
        }
    };
    public static final TransactionType PHASING_VOTE_CASTING = new MessagingTr() {
        private final Fee PHASING_VOTE_FEE = (io.firstbridge.fbc.core.interfaces.Transaction transaction, io.firstbridge.fbc.core.messages.Appendix appendage) -> {
            MessagingPhasingVoteCasting attachment = (MessagingPhasingVoteCasting) transaction.getAttachment();
            return attachment.getTransactionFullHashes().size() * Constants.ONE_NXT;
        };

        @Override
        public final byte getSubtype() {
            return TransactionType.SUBTYPE_MESSAGING_PHASING_VOTE_CASTING;
        }

        @Override
        public AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.PHASING_VOTE_CASTING;
        }

        @Override
        public String getName() {
            return "PhasingVoteCasting";
        }

        @Override
        public Fee getBaselineFee(Transaction transaction) {
            return PHASING_VOTE_FEE;
        }

        @Override
        public MessagingPhasingVoteCasting parseAttachment(ByteBuffer buffer) throws FbcException.NotValidException {
            return new MessagingPhasingVoteCasting(buffer);
        }

        @Override
        public MessagingPhasingVoteCasting parseAttachment(JSONObject attachmentData) {
            return new MessagingPhasingVoteCasting(attachmentData);
        }

        @Override
        public boolean canHaveRecipient() {
            return false;
        }

        @Override
        public void validateAttachment(Transaction transaction) throws FbcException.ValidationException {
            MessagingPhasingVoteCasting attachment = (MessagingPhasingVoteCasting) transaction.getAttachment();
            byte[] revealedSecret = attachment.getRevealedSecret();
            if (revealedSecret.length > Constants.MAX_PHASING_REVEALED_SECRET_LENGTH) {
                throw new FbcException.NotValidException("Invalid revealed secret length " + revealedSecret.length);
            }
            byte[] hashedSecret = null;
            byte algorithm = 0;
            List<byte[]> hashes = attachment.getTransactionFullHashes();
            if (hashes.size() > Constants.MAX_PHASING_VOTE_TRANSACTIONS) {
                throw new FbcException.NotValidException("No more than " + Constants.MAX_PHASING_VOTE_TRANSACTIONS + " votes allowed for two-phased multi-voting");
            }
            long voterId = transaction.getSenderId();
            for (byte[] hash : hashes) {
                long phasedTransactionId = Convert.fullHashToId(hash);
                if (phasedTransactionId == 0) {
                    throw new FbcException.NotValidException("Invalid phased transactionFullHash " + Convert.toHexString(hash));
                }
                PhasingPoll poll = PhasingPoll.getPoll(phasedTransactionId);
                if (poll == null) {
                    throw new FbcException.NotCurrentlyValidException("Invalid phased transaction " + Long.toUnsignedString(phasedTransactionId) + ", or phasing is finished");
                }
                if (!poll.getVoteWeighting().acceptsVotes()) {
                    throw new FbcException.NotValidException("This phased transaction does not require or accept voting");
                }
                long[] whitelist = poll.getWhitelist();
                if (whitelist.length > 0 && Arrays.binarySearch(whitelist, voterId) < 0) {
                    throw new FbcException.NotValidException("Voter is not in the phased transaction whitelist");
                }
                if (revealedSecret.length > 0) {
                    if (poll.getVoteWeighting().getVotingModel() != VoteWeighting.VotingModel.HASH) {
                        throw new FbcException.NotValidException("Phased transaction " + Long.toUnsignedString(phasedTransactionId) + " does not accept by-hash voting");
                    }
                    if (hashedSecret != null && !Arrays.equals(poll.getHashedSecret(), hashedSecret)) {
                        throw new FbcException.NotValidException("Phased transaction " + Long.toUnsignedString(phasedTransactionId) + " is using a different hashedSecret");
                    }
                    if (algorithm != 0 && algorithm != poll.getAlgorithm()) {
                        throw new FbcException.NotValidException("Phased transaction " + Long.toUnsignedString(phasedTransactionId) + " is using a different hashedSecretAlgorithm");
                    }
                    if (hashedSecret == null && !poll.verifySecret(revealedSecret)) {
                        throw new FbcException.NotValidException("Revealed secret does not match phased transaction hashed secret");
                    }
                    hashedSecret = poll.getHashedSecret();
                    algorithm = poll.getAlgorithm();
                } else if (poll.getVoteWeighting().getVotingModel() == VoteWeighting.VotingModel.HASH) {
                    throw new FbcException.NotValidException("Phased transaction " + Long.toUnsignedString(phasedTransactionId) + " requires revealed secret for approval");
                }
                if (!Arrays.equals(poll.getFullHash(), hash)) {
                    throw new FbcException.NotCurrentlyValidException("Phased transaction hash does not match hash in voting transaction");
                }
                if (poll.getFinishHeight() <= attachment.getFinishValidationHeight(transaction) + 1) {
                    throw new FbcException.NotCurrentlyValidException(String.format("Phased transaction finishes at height %d which is not after approval transaction height %d", poll.getFinishHeight(), attachment.getFinishValidationHeight(transaction) + 1));
                }
            }
        }

        @Override
        public final void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {
            MessagingPhasingVoteCasting attachment = (MessagingPhasingVoteCasting) transaction.getAttachment();
            List<byte[]> hashes = attachment.getTransactionFullHashes();
            for (byte[] hash : hashes) {
                PhasingVote.addVote(transaction, senderAccount, Convert.fullHashToId(hash));
            }
        }

        @Override
        public boolean isPhasingSafe() {
            return true;
        }
    };
    public static final MessagingTr ACCOUNT_INFO = new MessagingTr() {
        private final Fee ACCOUNT_INFO_FEE = new SizeBasedFee(Constants.ONE_NXT, 2 * Constants.ONE_NXT, 32) {
            @Override
            public int getSize(Transaction transaction, Appendix appendage) {
                MessagingAccountInfo attachment = (MessagingAccountInfo) transaction.getAttachment();
                return attachment.getName().length() + attachment.getDescription().length();
            }
        };

        @Override
        public byte getSubtype() {
            return TransactionType.SUBTYPE_MESSAGING_ACCOUNT_INFO;
        }

        @Override
        public AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.ACCOUNT_INFO;
        }

        @Override
        public String getName() {
            return "AccountInfo";
        }

        @Override
        public Fee getBaselineFee(Transaction transaction) {
            return ACCOUNT_INFO_FEE;
        }

        @Override
        public MessagingAccountInfo parseAttachment(ByteBuffer buffer) throws FbcException.NotValidException {
            return new MessagingAccountInfo(buffer);
        }

        @Override
        public MessagingAccountInfo parseAttachment(JSONObject attachmentData) {
            return new MessagingAccountInfo(attachmentData);
        }

        @Override
        public void validateAttachment(Transaction transaction) throws FbcException.ValidationException {
            MessagingAccountInfo attachment = (MessagingAccountInfo) transaction.getAttachment();
            if (attachment.getName().length() > Constants.MAX_ACCOUNT_NAME_LENGTH || attachment.getDescription().length() > Constants.MAX_ACCOUNT_DESCRIPTION_LENGTH) {
                throw new FbcException.NotValidException("Invalid account info issuance: " + attachment.getJSONObject());
            }
        }

        @Override
        public void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {
            MessagingAccountInfo attachment = (MessagingAccountInfo) transaction.getAttachment();
            senderAccount.setAccountInfo(attachment.getName(), attachment.getDescription());
        }

        @Override
        public boolean isBlockDuplicate(Transaction transaction, Map<TransactionType, Map<String, Integer>> duplicates) {
            return isDuplicate(MessagingTr.ACCOUNT_INFO, getName(), duplicates, true);
        }

        @Override
        public boolean canHaveRecipient() {
            return false;
        }

        @Override
        public boolean isPhasingSafe() {
            return true;
        }
    };
    public static final MessagingTr ACCOUNT_PROPERTY = new MessagingTr() {
        private final Fee ACCOUNT_PROPERTY_FEE = new SizeBasedFee(Constants.ONE_NXT, Constants.ONE_NXT, 32) {
            @Override
            public int getSize(Transaction transaction, Appendix appendage) {
                MessagingAccountProperty attachment = (MessagingAccountProperty) transaction.getAttachment();
                return attachment.getValue().length();
            }
        };

        @Override
        public byte getSubtype() {
            return TransactionType.SUBTYPE_MESSAGING_ACCOUNT_PROPERTY;
        }

        @Override
        public AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.ACCOUNT_PROPERTY;
        }

        @Override
        public String getName() {
            return "AccountProperty";
        }

        @Override
        public Fee getBaselineFee(Transaction transaction) {
            return ACCOUNT_PROPERTY_FEE;
        }

        @Override
        public MessagingAccountProperty parseAttachment(ByteBuffer buffer) throws FbcException.NotValidException {
            return new MessagingAccountProperty(buffer);
        }

        @Override
        public MessagingAccountProperty parseAttachment(JSONObject attachmentData) {
            return new MessagingAccountProperty(attachmentData);
        }

        @Override
        public void validateAttachment(Transaction transaction) throws FbcException.ValidationException {
            MessagingAccountProperty attachment = (MessagingAccountProperty) transaction.getAttachment();
            if (attachment.getProperty().length() > Constants.MAX_ACCOUNT_PROPERTY_NAME_LENGTH || attachment.getProperty().length() == 0 || attachment.getValue().length() > Constants.MAX_ACCOUNT_PROPERTY_VALUE_LENGTH) {
                throw new FbcException.NotValidException("Invalid account property: " + attachment.getJSONObject());
            }
            if (transaction.getAmountNQT() != 0) {
                throw new FbcException.NotValidException("Account property transaction cannot be used to send " + Constants.COIN_SYMBOL);
            }
            if (transaction.getRecipientId() == Genesis.CREATOR_ID) {
                throw new FbcException.NotValidException("Setting Genesis account properties not allowed");
            }
        }

        @Override
        public void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {
            MessagingAccountProperty attachment = (MessagingAccountProperty) transaction.getAttachment();
            recipientAccount.setProperty(transaction, senderAccount, attachment.getProperty(), attachment.getValue());
        }

        @Override
        public boolean canHaveRecipient() {
            return true;
        }

        @Override
        public boolean isPhasingSafe() {
            return true;
        }
    };
    public static final MessagingTr ACCOUNT_PROPERTY_DELETE = new MessagingTr() {
        @Override
        public byte getSubtype() {
            return TransactionType.SUBTYPE_MESSAGING_ACCOUNT_PROPERTY_DELETE;
        }

        @Override
        public AccountLedger.LedgerEvent getLedgerEvent() {
            return AccountLedger.LedgerEvent.ACCOUNT_PROPERTY_DELETE;
        }

        @Override
        public String getName() {
            return "AccountPropertyDelete";
        }

        @Override
        public MessagingAccountPropertyDelete parseAttachment(ByteBuffer buffer) {
            return new MessagingAccountPropertyDelete(buffer);
        }

        @Override
        public MessagingAccountPropertyDelete parseAttachment(JSONObject attachmentData) {
            return new MessagingAccountPropertyDelete(attachmentData);
        }

        @Override
        public void validateAttachment(Transaction transaction) throws FbcException.ValidationException {
            MessagingAccountPropertyDelete attachment = (MessagingAccountPropertyDelete) transaction.getAttachment();
            Account.AccountProperty accountProperty = Account.getProperty(attachment.getPropertyId());
            if (accountProperty == null) {
                throw new FbcException.NotCurrentlyValidException("No such property " + Long.toUnsignedString(attachment.getPropertyId()));
            }
            if (accountProperty.getRecipientId() != transaction.getSenderId() && accountProperty.getSetterId() != transaction.getSenderId()) {
                throw new FbcException.NotValidException("Account " + Long.toUnsignedString(transaction.getSenderId()) + " cannot delete property " + Long.toUnsignedString(attachment.getPropertyId()));
            }
            if (accountProperty.getRecipientId() != transaction.getRecipientId()) {
                throw new FbcException.NotValidException("Account property " + Long.toUnsignedString(attachment.getPropertyId()) + " does not belong to " + Long.toUnsignedString(transaction.getRecipientId()));
            }
            if (transaction.getAmountNQT() != 0) {
                throw new FbcException.NotValidException("Account property transaction cannot be used to send " + Constants.COIN_SYMBOL);
            }
            if (transaction.getRecipientId() == Genesis.CREATOR_ID) {
                throw new FbcException.NotValidException("Deleting Genesis account properties not allowed");
            }
        }

        @Override
        public void applyAttachment(Transaction transaction, Account senderAccount, Account recipientAccount) {
            MessagingAccountPropertyDelete attachment = (MessagingAccountPropertyDelete) transaction.getAttachment();
            senderAccount.deleteProperty(attachment.getPropertyId());
        }

        @Override
        public boolean canHaveRecipient() {
            return true;
        }

        @Override
        public boolean isPhasingSafe() {
            return true;
        }
    };
    
}
