/*
 * Copyright © 2013-2016 The Nxt Core Developers.
 * Copyright © 2016-2018 Jelurida IP B.V.
 *
 * See the LICENSE.txt file at the top-level directory of this distribution
 * for licensing information.
 *
 * Unless otherwise agreed in a custom licensing agreement with Jelurida B.V.,
 * no part of the Nxt software, including this file, may be copied, modified,
 * propagated, or distributed except according to the terms contained in the
 * LICENSE.txt file.
 *
 * Removal or modification of this copyright notice is prohibited.
 *
 */

package io.firstbridge.fbc.http.rq;

import io.firstbridge.fbc.core.account.Account;
import io.firstbridge.fbc.core.messages.Attachment;
import io.firstbridge.fbc.core.Order;
import io.firstbridge.fbc.core.messages.ColoredCoinsAskOrderCancellation;
import io.firstbridge.fbc.http.APITag;
import io.firstbridge.fbc.http.ParameterParser;
import org.json.simple.JSONStreamAware;

import javax.servlet.http.HttpServletRequest;

import static io.firstbridge.fbc.http.JSONResponses.UNKNOWN_ORDER;
import io.firstbridge.fbc.util.FbcException;

public final class CancelAskOrder extends CreateTransaction {

    public CancelAskOrder() {
        super(new APITag[] {APITag.AE, APITag.CREATE_TRANSACTION}, "order");
    }

    @Override
    protected JSONStreamAware processRequest(HttpServletRequest req) throws FbcException {
        long orderId = ParameterParser.getUnsignedLong(req, "order", true);
        Account account = ParameterParser.getSenderAccount(req);
        Order.Ask orderData = Order.Ask.getAskOrder(orderId);
        if (orderData == null || orderData.getAccountId() != account.getId()) {
            return UNKNOWN_ORDER;
        }
        Attachment attachment = new ColoredCoinsAskOrderCancellation(orderId);
        return createTransaction(req, account, attachment);
    }

}
