/*
 * Copyright © 2013-2016 The Nxt Core Developers.
 * Copyright © 2016-2018 Jelurida IP B.V.
 *
 * See the LICENSE.txt file at the top-level directory of this distribution
 * for licensing information.
 *
 * Unless otherwise agreed in a custom licensing agreement with Jelurida B.V.,
 * no part of the Nxt software, including this file, may be copied, modified,
 * propagated, or distributed except according to the terms contained in the
 * LICENSE.txt file.
 *
 * Removal or modification of this copyright notice is prohibited.
 *
 */

package io.firstbridge.fbc.http;

import io.firstbridge.fbc.http.rq.CreateTransaction;
import io.firstbridge.fbc.core.DiH;
import io.firstbridge.fbc.core.account.Account;
import io.firstbridge.fbc.util.FbcException;
import io.firstbridge.fbc.core.TaggedData;
import io.firstbridge.fbc.core.interfaces.Transaction;
import io.firstbridge.fbc.core.messages.TaggedDataExtend;
import io.firstbridge.fbc.core.messages.TaggedDataUpload;
import io.firstbridge.fbc.transaction.DataTr;
import org.json.simple.JSONStreamAware;

import javax.servlet.http.HttpServletRequest;

import static io.firstbridge.fbc.http.JSONResponses.UNKNOWN_TRANSACTION;

public final class ExtendTaggedData extends CreateTransaction {

    public ExtendTaggedData() {
        super("file", new APITag[] {APITag.DATA, APITag.CREATE_TRANSACTION}, "transaction",
                "name", "description", "tags", "type", "channel", "isText", "filename", "data");
    }

    @Override
    protected JSONStreamAware processRequest(HttpServletRequest req) throws FbcException {

        Account account = ParameterParser.getSenderAccount(req);
        long transactionId = ParameterParser.getUnsignedLong(req, "transaction", true);
        TaggedData taggedData = TaggedData.getData(transactionId);
        if (taggedData == null) {
            Transaction transaction = DiH.getBlockchain().getTransaction(transactionId);
            if (transaction == null || transaction.getType() != DataTr.TAGGED_DATA_UPLOAD) {
                return UNKNOWN_TRANSACTION;
            }
            TaggedDataUpload taggedDataUpload = ParameterParser.getTaggedData(req);
            taggedData = new TaggedData(transaction, taggedDataUpload);
        }
        TaggedDataExtend taggedDataExtend = new TaggedDataExtend(taggedData);
        return createTransaction(req, account, taggedDataExtend);

    }

}
