/*
 * Copyright © 2013-2016 The Nxt Core Developers.
 * Copyright © 2016-2018 Jelurida IP B.V.
 *
 * See the LICENSE.txt file at the top-level directory of this distribution
 * for licensing information.
 *
 * Unless otherwise agreed in a custom licensing agreement with Jelurida B.V.,
 * no part of the Nxt software, including this file, may be copied, modified,
 * propagated, or distributed except according to the terms contained in the
 * LICENSE.txt file.
 *
 * Removal or modification of this copyright notice is prohibited.
 *
 */

package io.firstbridge.fbc.http.rq;

import io.firstbridge.fbc.core.account.Account;
import io.firstbridge.fbc.http.APIRequestHandler;
import io.firstbridge.fbc.http.APITag;
import io.firstbridge.fbc.http.ParameterParser;
import io.firstbridge.fbc.util.FbcException;
import io.firstbridge.fbc.crypto.util.Convert;
import io.firstbridge.fbc.util.JSON;
import org.json.simple.JSONObject;
import org.json.simple.JSONStreamAware;

import javax.servlet.http.HttpServletRequest;

public final class GetAccountPublicKey extends APIRequestHandler {

    public GetAccountPublicKey() {
        super(new APITag[] {APITag.ACCOUNTS}, "account");
    }

    @Override
    protected JSONStreamAware processRequest(HttpServletRequest req) throws FbcException {

        long accountId = ParameterParser.getAccountId(req, true);
        byte[] publicKey = Account.getPublicKey(accountId);
        if (publicKey != null) {
            JSONObject response = new JSONObject();
            response.put("publicKey", Convert.toHexString(publicKey));
            return response;
        } else {
            return JSON.emptyJSON;
        }
    }

}
