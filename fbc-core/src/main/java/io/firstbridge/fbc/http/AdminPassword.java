package io.firstbridge.fbc.http;

import io.firstbridge.fbc.core.DiH;
import static io.firstbridge.fbc.http.JSONResponses.INCORRECT_ADMIN_PASSWORD;
import static io.firstbridge.fbc.http.JSONResponses.LOCKED_ADMIN_PASSWORD;
import static io.firstbridge.fbc.http.JSONResponses.MISSING_ADMIN_PASSWORD;
import static io.firstbridge.fbc.http.JSONResponses.NO_PASSWORD_IN_CONFIG;
import io.firstbridge.fbc.util.ApiProperties;
import io.firstbridge.fbc.crypto.util.Convert;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Admin password checker
 *
 * @author al
 */
public class AdminPassword {
   static final Logger logger = LoggerFactory.getLogger(AdminPassword.class);

    private static final Map<String, PasswordCount> incorrectPasswords = new HashMap<>();

    public static void verifyPassword(HttpServletRequest req) throws ParameterException {
        if (ApiProperties.disableAdminPassword) {
            return;
        }
        if (ApiProperties.adminPassword.isEmpty()) {
            throw new ParameterException(NO_PASSWORD_IN_CONFIG);
        }
        checkOrLockPassword(req);
    }

    public static boolean checkPassword(HttpServletRequest req) {
        if (ApiProperties.disableAdminPassword) {
            return true;
        }
        if (ApiProperties.adminPassword.isEmpty()) {
            return false;
        }
        if (Convert.emptyToNull(req.getParameter("adminPassword")) == null) {
            return false;
        }
        try {
            checkOrLockPassword(req);
            return true;
        } catch (ParameterException e) {
            return false;
        }
    }

    private static class PasswordCount {

        private int count;
        private int time;
    }

    private static void checkOrLockPassword(HttpServletRequest req) throws ParameterException {
        int now = DiH.getEpochTime();
        String remoteHost = null;
        if (ApiProperties.forwardedForHeader != null) {
            remoteHost = req.getHeader(ApiProperties.forwardedForHeader);
        }
        if (remoteHost == null) {
            remoteHost = req.getRemoteHost();
        }
        synchronized (incorrectPasswords) {
            PasswordCount passwordCount = incorrectPasswords.get(remoteHost);
            if (passwordCount != null && passwordCount.count >= 25 && now - passwordCount.time < 60 * 60) {
                logger.warn("Too many incorrect admin password attempts from " + remoteHost);
                throw new ParameterException(LOCKED_ADMIN_PASSWORD);
            }
            String adminPassword = Convert.nullToEmpty(req.getParameter("adminPassword"));
            if (!ApiProperties.adminPassword.equals(adminPassword)) {
                if (adminPassword.length() > 0) {
                    if (passwordCount == null) {
                        passwordCount = new PasswordCount();
                        incorrectPasswords.put(remoteHost, passwordCount);
                        if (incorrectPasswords.size() > 1000) {
                            // Remove one of the locked hosts at random to prevent unlimited growth of the map
                            List<String> remoteHosts = new ArrayList<>(incorrectPasswords.keySet());
                            Random r = new Random();
                            incorrectPasswords.remove(remoteHosts.get(r.nextInt(remoteHosts.size())));
                        }
                    }
                    passwordCount.count++;
                    passwordCount.time = now;
                    logger.warn("Incorrect adminPassword from " + remoteHost);
                    throw new ParameterException(INCORRECT_ADMIN_PASSWORD);
                } else {
                    throw new ParameterException(MISSING_ADMIN_PASSWORD);
                }
            }
            if (passwordCount != null) {
                incorrectPasswords.remove(remoteHost);
            }
        }
    }

}
