/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.firstbridge.fbc.http;

import io.firstbridge.fbc.util.ApiProperties;
import io.firstbridge.fbc.util.FbcException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONStreamAware;

/**
 *
 * @author al
 */
public abstract class APIRequestHandler {
    
    private final List<String> parameters;
    private final String fileParameter;
    private final Set<APITag> apiTags;

    protected APIRequestHandler(APITag[] apiTags, String... parameters) {
        this(null, apiTags, parameters);
    }

    protected APIRequestHandler(String fileParameter, APITag[] apiTags, String... origParameters) {
        List<String> parameters = new ArrayList<>();
        Collections.addAll(parameters, origParameters);
        if ((requirePassword() || parameters.contains("lastIndex")) && !ApiProperties.disableAdminPassword) {
            parameters.add("adminPassword");
        }
        if (allowRequiredBlockParameters()) {
            parameters.add("requireBlock");
            parameters.add("requireLastBlock");
        }
        this.parameters = Collections.unmodifiableList(parameters);
        this.apiTags = Collections.unmodifiableSet(new HashSet<>(Arrays.asList(apiTags)));
        this.fileParameter = fileParameter;
    }

    public final List<String> getParameters() {
        return parameters;
    }

    public final Set<APITag> getAPITags() {
        return apiTags;
    }

    public final String getFileParameter() {
        return fileParameter;
    }

    protected abstract JSONStreamAware processRequest(HttpServletRequest request) throws FbcException;

    protected JSONStreamAware processRequest(HttpServletRequest request, HttpServletResponse response) throws FbcException {
        return processRequest(request);
    }

    protected boolean requirePost() {
        return false;
    }

    protected boolean startDbTransaction() {
        return false;
    }

    protected boolean requirePassword() {
        return false;
    }

    protected boolean allowRequiredBlockParameters() {
        return true;
    }

    public boolean requireBlockchain() {
        return true;
    }

    protected boolean requireFullClient() {
        return false;
    }

    protected boolean isTextArea(String parameter) {
        return false;
    }

    protected boolean isPassword(String parameter) {
        return false;
    }
    
}
