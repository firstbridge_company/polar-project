/*
 * Copyright © 2013-2016 The Nxt Core Developers.
 * Copyright © 2016-2018 Jelurida IP B.V.
 *
 * See the LICENSE.txt file at the top-level directory of this distribution
 * for licensing information.
 *
 * Unless otherwise agreed in a custom licensing agreement with Jelurida B.V.,
 * no part of the Nxt software, including this file, may be copied, modified,
 * propagated, or distributed except according to the terms contained in the
 * LICENSE.txt file.
 *
 * Removal or modification of this copyright notice is prohibited.
 *
 */

package io.firstbridge.fbc.http.rq;

import io.firstbridge.fbc.http.APIRequestHandler;
import io.firstbridge.fbc.http.APITag;
import io.firstbridge.fbc.http.EncodeQRCode;
import io.firstbridge.fbc.http.EncryptTo;
import io.firstbridge.fbc.http.EventRegister;
import io.firstbridge.fbc.http.EventWait;
import io.firstbridge.fbc.http.ExtendTaggedData;
import io.firstbridge.fbc.http.FullHashToId;
import io.firstbridge.fbc.http.FullReset;
import io.firstbridge.fbc.http.GetConstants;
import io.firstbridge.fbc.http.Hash;
import io.firstbridge.fbc.http.LuceneReindex;
import io.firstbridge.fbc.http.SetAPIProxyPeer;
import java.util.Base64;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum APIEnum {
    //To preserve compatibility, please add new APIs to the end of the enum.
    //When an API is deleted, set its name to empty string and handler to null.
    APPROVE_TRANSACTION("approveTransaction", new ApproveTransaction()),
    BROADCAST_TRANSACTION("broadcastTransaction", new BroadcastTransaction()),
    CALCULATE_FULL_HASH("calculateFullHash", new CalculateFullHash()),
    CANCEL_ASK_ORDER("cancelAskOrder", new CancelAskOrder()),
    CANCEL_BID_ORDER("cancelBidOrder", new CancelBidOrder()),
    CAST_VOTE("castVote", new CastVote()),
    CREATE_POLL("createPoll", new CreatePoll()),
    CURRENCY_BUY("currencyBuy",new  CurrencyBuy()),
    CURRENCY_SELL("currencySell", new CurrencySell()),
    CURRENCY_RESERVE_INCREASE("currencyReserveIncrease", new CurrencyReserveIncrease()),
    CURRENCY_RESERVE_CLAIM("currencyReserveClaim", new CurrencyReserveClaim()),
    CURRENCY_MINT("currencyMint", new CurrencyMint()),
    DECRYPT_FROM("decryptFrom", new DecryptFrom()),
    DELETE_ASSET_SHARES("deleteAssetShares",new  DeleteAssetShares()),
    DGS_LISTING("dgsListing", new DGSListing()),
    DGS_DELISTING("dgsDelisting",new  DGSDelisting()),
    DGS_DELIVERY("dgsDelivery", new DGSDelivery()),
    DGS_FEEDBACK("dgsFeedback", new DGSFeedback()),
    DGS_PRICE_CHANGE("dgsPriceChange", new DGSPriceChange()),
    DGS_PURCHASE("dgsPurchase", new DGSPurchase()),
    DGS_QUANTITY_CHANGE("dgsQuantityChange", new DGSQuantityChange()),
    DGS_REFUND("dgsRefund",new  DGSRefund()),
    DECODE_HALLMARK("decodeHallmark",new  DecodeHallmark()),
    DECODE_TOKEN("decodeToken", new DecodeToken()),
    DECODE_FILE_TOKEN("decodeFileToken",new  DecodeFileToken()),
    DECODE_Q_R_CODE("decodeQRCode", new DecodeQRCode()),
    ENCODE_Q_R_CODE("encodeQRCode",new  EncodeQRCode()),
    ENCRYPT_TO("encryptTo",new  EncryptTo()),
    EVENT_REGISTER("eventRegister",new  EventRegister()),
    EVENT_WAIT("eventWait",new  EventWait()),
    GENERATE_TOKEN("generateToken", new GenerateToken()),
    GENERATE_FILE_TOKEN("generateFileToken", new GenerateFileToken()),
    GET_ACCOUNT("getAccount", new GetAccount()),
    GET_ACCOUNT_BLOCK_COUNT("getAccountBlockCount",new  GetAccountBlockCount()),
    GET_ACCOUNT_BLOCK_IDS("getAccountBlockIds", new GetAccountBlockIds()),
    GET_ACCOUNT_BLOCKS("getAccountBlocks", new GetAccountBlocks()),
    GET_ACCOUNT_ID("getAccountId", new GetAccountId()),
    GET_ACCOUNT_LEDGER("getAccountLedger", new GetAccountLedger()),
    GET_ACCOUNT_LEDGER_ENTRY("getAccountLedgerEntry", new GetAccountLedgerEntry()),
    GET_VOTER_PHASED_TRANSACTIONS("getVoterPhasedTransactions", new GetVoterPhasedTransactions()),
    GET_LINKED_PHASED_TRANSACTIONS("getLinkedPhasedTransactions", new GetLinkedPhasedTransactions()),
    GET_POLLS("getPolls", new GetPolls()),
    GET_ACCOUNT_PHASED_TRANSACTIONS("getAccountPhasedTransactions", new GetAccountPhasedTransactions()),
    GET_ACCOUNT_PHASED_TRANSACTION_COUNT("getAccountPhasedTransactionCount", new GetAccountPhasedTransactionCount()),
    GET_ACCOUNT_PUBLIC_KEY("getAccountPublicKey", new GetAccountPublicKey()),
    GET_ACCOUNT_LESSORS("getAccountLessors",new  GetAccountLessors()),
    GET_ACCOUNT_ASSETS("getAccountAssets", new GetAccountAssets()),
    GET_ACCOUNT_CURRENCIES("getAccountCurrencies", new GetAccountCurrencies()),
    GET_ACCOUNT_CURRENCY_COUNT("getAccountCurrencyCount",new  GetAccountCurrencyCount()),
    GET_ACCOUNT_ASSET_COUNT("getAccountAssetCount", new GetAccountAssetCount()),
    GET_ACCOUNT_PROPERTIES("getAccountProperties",new  GetAccountProperties()),
    SELL_ALIAS("sellAlias", new SellAlias()),
    BUY_ALIAS("buyAlias",new  BuyAlias()),
    GET_ALIAS("getAlias", new GetAlias()),
    GET_ALIAS_COUNT("getAliasCount", new GetAliasCount()),
    GET_ALIASES("getAliases",new  GetAliases()),
    GET_ALIASES_LIKE("getAliasesLike", new GetAliasesLike()),
    GET_ALL_ASSETS("getAllAssets", new GetAllAssets()),
    GET_ALL_CURRENCIES("getAllCurrencies", new GetAllCurrencies()),
    GET_ASSET("getAsset", new GetAsset()),
    GET_ASSETS("getAssets", new GetAssets()),
    GET_ASSET_IDS("getAssetIds", new GetAssetIds()),
    GET_ASSETS_BY_ISSUER("getAssetsByIssuer",new  GetAssetsByIssuer()),
    GET_ASSET_ACCOUNTS("getAssetAccounts",new  GetAssetAccounts()),
    GET_ASSET_ACCOUNT_COUNT("getAssetAccountCount", new GetAssetAccountCount()),
    GET_ASSET_PHASED_TRANSACTIONS("getAssetPhasedTransactions", new GetAssetPhasedTransactions()),
    GET_BALANCE("getBalance",new  GetBalance()),
    GET_BLOCK("getBlock",new  GetBlock()),
    GET_BLOCK_ID("getBlockId", new GetBlockId()),
    GET_BLOCKS("getBlocks", new GetBlocks()),
    GET_BLOCKCHAIN_STATUS("getBlockchainStatus", new GetBlockchainStatus()),
    GET_BLOCKCHAIN_TRANSACTIONS("getBlockchainTransactions",new  GetBlockchainTransactions()),
    GET_REFERENCING_TRANSACTIONS("getReferencingTransactions",new  GetReferencingTransactions()),
    GET_CONSTANTS("getConstants",new  GetConstants()),
    GET_CURRENCY("getCurrency", new GetCurrency()),
    GET_CURRENCIES("getCurrencies", new GetCurrencies()),
    GET_CURRENCY_FOUNDERS("getCurrencyFounders",new  GetCurrencyFounders()),
    GET_CURRENCY_IDS("getCurrencyIds", new GetCurrencyIds()),
    GET_CURRENCIES_BY_ISSUER("getCurrenciesByIssuer",new  GetCurrenciesByIssuer()),
    GET_CURRENCY_ACCOUNTS("getCurrencyAccounts", new GetCurrencyAccounts()),
    GET_CURRENCY_ACCOUNT_COUNT("getCurrencyAccountCount", new GetCurrencyAccountCount()),
    GET_CURRENCY_PHASED_TRANSACTIONS("getCurrencyPhasedTransactions", new GetCurrencyPhasedTransactions()),
    GET_DGS_GOODS("getDGSGoods", new GetDGSGoods()),
    GET_DGS_GOODS_COUNT("getDGSGoodsCount", new GetDGSGoodsCount()),
    GET_DGS_GOOD("getDGSGood", new GetDGSGood()),
    GET_DGS_GOODS_PURCHASES("getDGSGoodsPurchases", new GetDGSGoodsPurchases()),
    GET_DGS_GOODS_PURCHASE_COUNT("getDGSGoodsPurchaseCount",new  GetDGSGoodsPurchaseCount()),
    GET_DGS_PURCHASES("getDGSPurchases", new GetDGSPurchases()),
    GET_DGS_PURCHASE("getDGSPurchase", new GetDGSPurchase()),
    GET_DGS_PURCHASE_COUNT("getDGSPurchaseCount", new GetDGSPurchaseCount()),
    GET_DGS_PENDING_PURCHASES("getDGSPendingPurchases",new  GetDGSPendingPurchases()),
    GET_DGS_EXPIRED_PURCHASES("getDGSExpiredPurchases", new GetDGSExpiredPurchases()),
    GET_DGS_TAGS("getDGSTags", new GetDGSTags()),
    GET_DGS_TAG_COUNT("getDGSTagCount",new  GetDGSTagCount()),
    GET_DGS_TAGS_LIKE("getDGSTagsLike", new GetDGSTagsLike()),
    GET_GUARANTEED_BALANCE("getGuaranteedBalance",new  GetGuaranteedBalance()),
    GET_E_C_BLOCK("getECBlock", new GetECBlock()),
    GET_INBOUND_PEERS("getInboundPeers", new GetInboundPeers()),
    GET_PLUGINS("getPlugins", new GetPlugins()),
    GET_MY_INFO("getMyInfo", new GetMyInfo()),
    GET_PEER("getPeer", new GetPeer()),
    GET_PEERS("getPeers", new GetPeers()),
    GET_PHASING_POLL("getPhasingPoll",new  GetPhasingPoll()),
    GET_PHASING_POLLS("getPhasingPolls", new GetPhasingPolls()),
    GET_PHASING_POLL_VOTES("getPhasingPollVotes", new GetPhasingPollVotes()),
    GET_PHASING_POLL_VOTE("getPhasingPollVote", new GetPhasingPollVote()),
    GET_POLL("getPoll", new GetPoll()),
    GET_POLL_RESULT("getPollResult", new GetPollResult()),
    GET_POLL_VOTES("getPollVotes", new GetPollVotes()),
    GET_POLL_VOTE("getPollVote", new GetPollVote()),
    GET_STATE("getState", new GetState()),
    GET_TIME("getTime", new GetTime()),
    GET_TRADES("getTrades", new GetTrades()),
    GET_LAST_TRADES("getLastTrades", new GetLastTrades()),
    GET_EXCHANGES("getExchanges", new GetExchanges()),
    GET_EXCHANGES_BY_EXCHANGE_REQUEST("getExchangesByExchangeRequest", new GetExchangesByExchangeRequest()),
    GET_EXCHANGES_BY_OFFER("getExchangesByOffer", new GetExchangesByOffer()),
    GET_LAST_EXCHANGES("getLastExchanges", new GetLastExchanges()),
    GET_ALL_TRADES("getAllTrades", new GetAllTrades()),
    GET_ALL_EXCHANGES("getAllExchanges", new GetAllExchanges()),
    GET_ASSET_TRANSFERS("getAssetTransfers", new GetAssetTransfers()),
    GET_ASSET_DELETES("getAssetDeletes",new  GetAssetDeletes()),
    GET_EXPECTED_ASSET_TRANSFERS("getExpectedAssetTransfers", new GetExpectedAssetTransfers()),
    GET_EXPECTED_ASSET_DELETES("getExpectedAssetDeletes", new  GetExpectedAssetDeletes()),
    GET_CURRENCY_TRANSFERS("getCurrencyTransfers", new GetCurrencyTransfers()),
    GET_EXPECTED_CURRENCY_TRANSFERS("getExpectedCurrencyTransfers", new GetExpectedCurrencyTransfers()),
    GET_TRANSACTION("getTransaction",new  GetTransaction()),
    GET_TRANSACTION_BYTES("getTransactionBytes", new GetTransactionBytes()),
    GET_UNCONFIRMED_TRANSACTION_IDS("getUnconfirmedTransactionIds", new GetUnconfirmedTransactionIds()),
    GET_UNCONFIRMED_TRANSACTIONS("getUnconfirmedTransactions", new GetUnconfirmedTransactions()),
    GET_EXPECTED_TRANSACTIONS("getExpectedTransactions", new GetExpectedTransactions()),
    GET_ACCOUNT_CURRENT_ASK_ORDER_IDS("getAccountCurrentAskOrderIds", new GetAccountCurrentAskOrderIds()),
    GET_ACCOUNT_CURRENT_BID_ORDER_IDS("getAccountCurrentBidOrderIds", new GetAccountCurrentBidOrderIds()),
    GET_ACCOUNT_CURRENT_ASK_ORDERS("getAccountCurrentAskOrders", new GetAccountCurrentAskOrders()),
    GET_ACCOUNT_CURRENT_BID_ORDERS("getAccountCurrentBidOrders", new GetAccountCurrentBidOrders()),
    GET_ALL_OPEN_ASK_ORDERS("getAllOpenAskOrders",new  GetAllOpenAskOrders()),
    GET_ALL_OPEN_BID_ORDERS("getAllOpenBidOrders", new GetAllOpenBidOrders()),
    GET_BUY_OFFERS("getBuyOffers", new GetBuyOffers()),
    GET_EXPECTED_BUY_OFFERS("getExpectedBuyOffers",new  GetExpectedBuyOffers()),
    GET_SELL_OFFERS("getSellOffers", new GetSellOffers()),
    GET_EXPECTED_SELL_OFFERS("getExpectedSellOffers", new GetExpectedSellOffers()),
    GET_OFFER("getOffer",new  GetOffer()),
    GET_AVAILABLE_TO_BUY("getAvailableToBuy", new GetAvailableToBuy()),
    GET_AVAILABLE_TO_SELL("getAvailableToSell", new GetAvailableToSell()),
    GET_ASK_ORDER("getAskOrder", new GetAskOrder()),
    GET_ASK_ORDER_IDS("getAskOrderIds", new GetAskOrderIds()),
    GET_ASK_ORDERS("getAskOrders", new GetAskOrders()),
    GET_BID_ORDER("getBidOrder", new GetBidOrder()),
    GET_BID_ORDER_IDS("getBidOrderIds", new GetBidOrderIds()),
    GET_BID_ORDERS("getBidOrders", new GetBidOrders()),
    GET_EXPECTED_ASK_ORDERS("getExpectedAskOrders",new  GetExpectedAskOrders()),
    GET_EXPECTED_BID_ORDERS("getExpectedBidOrders", new GetExpectedBidOrders()),
    GET_EXPECTED_ORDER_CANCELLATIONS("getExpectedOrderCancellations", new GetExpectedOrderCancellations()),
    GET_ORDER_TRADES("getOrderTrades",new  GetOrderTrades()),
    GET_ACCOUNT_EXCHANGE_REQUESTS("getAccountExchangeRequests", new GetAccountExchangeRequests()),
    GET_EXPECTED_EXCHANGE_REQUESTS("getExpectedExchangeRequests", new GetExpectedExchangeRequests()),
    GET_MINTING_TARGET("getMintingTarget", new GetMintingTarget()),
    GET_ALL_SHUFFLINGS("getAllShufflings", new GetAllShufflings()),
    GET_ACCOUNT_SHUFFLINGS("getAccountShufflings", new GetAccountShufflings()),
    GET_ASSIGNED_SHUFFLINGS("getAssignedShufflings", new GetAssignedShufflings()),
    GET_HOLDING_SHUFFLINGS("getHoldingShufflings", new GetHoldingShufflings()),
    GET_SHUFFLING("getShuffling", new GetShuffling()),
    GET_SHUFFLING_PARTICIPANTS("getShufflingParticipants", new GetShufflingParticipants()),
    GET_PRUNABLE_MESSAGE("getPrunableMessage", new GetPrunableMessage()),
    GET_PRUNABLE_MESSAGES("getPrunableMessages",new  GetPrunableMessages()),
    GET_ALL_PRUNABLE_MESSAGES("getAllPrunableMessages",new  GetAllPrunableMessages()),
    VERIFY_PRUNABLE_MESSAGE("verifyPrunableMessage", new VerifyPrunableMessage()),
    ISSUE_ASSET("issueAsset", new IssueAsset()),
    ISSUE_CURRENCY("issueCurrency", new IssueCurrency()),
    LEASE_BALANCE("leaseBalance", new LeaseBalance()),
    LONG_CONVERT("longConvert", new LongConvert()),
    HEX_CONVERT("hexConvert", new HexConvert()),
    MARK_HOST("markHost", new MarkHost()),
    PARSE_TRANSACTION("parseTransaction", new ParseTransaction()),
    PLACE_ASK_ORDER("placeAskOrder", new PlaceAskOrder()),
    PLACE_BID_ORDER("placeBidOrder", new PlaceBidOrder()),
    PUBLISH_EXCHANGE_OFFER("publishExchangeOffer", new PublishExchangeOffer()),
    RS_CONVERT("rsConvert", new RSConvert()),
    READ_MESSAGE("readMessage", new ReadMessage()),
    SEND_MESSAGE("sendMessage", new SendMessage()),
    SEND_MONEY("sendMoney", new SendMoney()),
    SET_ACCOUNT_INFO("setAccountInfo", new SetAccountInfo()),
    SET_ACCOUNT_PROPERTY("setAccountProperty", new SetAccountProperty()),
    DELETE_ACCOUNT_PROPERTY("deleteAccountProperty", new DeleteAccountProperty()),
    SET_ALIAS("setAlias", new SetAlias()),
    SHUFFLING_CREATE("shufflingCreate", new ShufflingCreate()),
    SHUFFLING_REGISTER("shufflingRegister", new ShufflingRegister()),
    SHUFFLING_PROCESS("shufflingProcess", new ShufflingProcess()),
    SHUFFLING_VERIFY("shufflingVerify", new ShufflingVerify()),
    SHUFFLING_CANCEL("shufflingCancel", new ShufflingCancel()),
    START_SHUFFLER("startShuffler", new StartShuffler()),
    STOP_SHUFFLER("stopShuffler", new StopShuffler()),
    GET_SHUFFLERS("getShufflers",new  GetShufflers()),
    DELETE_ALIAS("deleteAlias", new DeleteAlias()),
    SIGN_TRANSACTION("signTransaction", new SignTransaction()),
    START_FORGING("startForging", new StartForging()),
    STOP_FORGING("stopForging", new StopForging()),
    GET_FORGING("getForging", new GetForging()),
    TRANSFER_ASSET("transferAsset", new TransferAsset()),
    TRANSFER_CURRENCY("transferCurrency", new TransferCurrency()),
    CAN_DELETE_CURRENCY("canDeleteCurrency", new CanDeleteCurrency()),
    DELETE_CURRENCY("deleteCurrency", new DeleteCurrency()),
    DIVIDEND_PAYMENT("dividendPayment",new DividendPayment()),
    SEARCH_DGS_GOODS("searchDGSGoods", new SearchDGSGoods()),
    SEARCH_ASSETS("searchAssets", new SearchAssets()),
    SEARCH_CURRENCIES("searchCurrencies", new SearchCurrencies()),
    SEARCH_POLLS("searchPolls",new  SearchPolls()),
    SEARCH_ACCOUNTS("searchAccounts", new SearchAccounts()),
    SEARCH_TAGGED_DATA("searchTaggedData", new SearchTaggedData()),
    UPLOAD_TAGGED_DATA("uploadTaggedData", new UploadTaggedData()),
    EXTEND_TAGGED_DATA("extendTaggedData", new ExtendTaggedData()),
    GET_ACCOUNT_TAGGED_DATA("getAccountTaggedData", new GetAccountTaggedData()),
    GET_ALL_TAGGED_DATA("getAllTaggedData", new GetAllTaggedData()),
    GET_CHANNEL_TAGGED_DATA("getChannelTaggedData", new GetChannelTaggedData()),
    GET_TAGGED_DATA("getTaggedData",new  GetTaggedData()),
    DOWNLOAD_TAGGED_DATA("downloadTaggedData", new DownloadTaggedData()),
    GET_DATA_TAGS("getDataTags", new GetDataTags()),
    GET_DATA_TAG_COUNT("getDataTagCount", new GetDataTagCount()),
    GET_DATA_TAGS_LIKE("getDataTagsLike", new GetDataTagsLike()),
    VERIFY_TAGGED_DATA("verifyTaggedData", new VerifyTaggedData()),
    GET_TAGGED_DATA_EXTEND_TRANSACTIONS("getTaggedDataExtendTransactions", new GetTaggedDataExtendTransactions()),
    CLEAR_UNCONFIRMED_TRANSACTIONS("clearUnconfirmedTransactions",new  ClearUnconfirmedTransactions()),
    REQUEUE_UNCONFIRMED_TRANSACTIONS("requeueUnconfirmedTransactions", new RequeueUnconfirmedTransactions()),
    REBROADCAST_UNCONFIRMED_TRANSACTIONS("rebroadcastUnconfirmedTransactions",new  RebroadcastUnconfirmedTransactions()),
    GET_ALL_WAITING_TRANSACTIONS("getAllWaitingTransactions",new  GetAllWaitingTransactions()),
    GET_ALL_BROADCASTED_TRANSACTIONS("getAllBroadcastedTransactions", new GetAllBroadcastedTransactions()),
    FULL_RESET("fullReset", new FullReset()),
    POP_OFF("popOff", new PopOff()),
    SCAN("scan", new Scan()),
    LUCENE_REINDEX("luceneReindex", new LuceneReindex()),
    ADD_PEER("addPeer", new AddPeer()),
    BLACKLIST_PEER("blacklistPeer", new BlacklistPeer()),
    DUMP_PEERS("dumpPeers", new DumpPeers()),
    GET_LOG("getLog", new GetLog()),
    GET_STACK_TRACES("getStackTraces", new GetStackTraces()),
    RETRIEVE_PRUNED_DATA("retrievePrunedData", new RetrievePrunedData()),
    RETRIEVE_PRUNED_TRANSACTION("retrievePrunedTransaction", new RetrievePrunedTransaction()),
    SET_LOGGING("setLogging", new SetLogging()),
    SHUTDOWN("shutdown", new Shutdown()),
    TRIM_DERIVED_TABLES("trimDerivedTables", new TrimDerivedTables()),
    HASH("hash", new Hash()),
    FULL_HASH_TO_ID("fullHashToId", new FullHashToId()),
    SET_PHASING_ONLY_CONTROL("setPhasingOnlyControl", new SetPhasingOnlyControl()),
    GET_PHASING_ONLY_CONTROL("getPhasingOnlyControl", new GetPhasingOnlyControl()),
    GET_ALL_PHASING_ONLY_CONTROLS("getAllPhasingOnlyControls", new GetAllPhasingOnlyControls()),
    DETECT_MIME_TYPE("detectMimeType",new  DetectMimeType()),
    START_FUNDING_MONITOR("startFundingMonitor",new  StartFundingMonitor()),
    STOP_FUNDING_MONITOR("stopFundingMonitor", new StopFundingMonitor()),
    GET_FUNDING_MONITOR("getFundingMonitor", new GetFundingMonitor()),
    DOWNLOAD_PRUNABLE_MESSAGE("downloadPrunableMessage", new DownloadPrunableMessage()),
    GET_SHARED_KEY("getSharedKey", new GetSharedKey()),
    SET_API_PROXY_PEER("setAPIProxyPeer", new SetAPIProxyPeer()),
    SEND_TRANSACTION("sendTransaction", new SendTransaction()),
    GET_ASSET_DIVIDENDS("getAssetDividends", new GetAssetDividends()),
    BLACKLIST_API_PROXY_PEER("blacklistAPIProxyPeer", new BlacklistAPIProxyPeer()),
    GET_NEXT_BLOCK_GENERATORS("getNextBlockGenerators", new GetNextBlockGeneratorsTemp()),
    GET_SCHEDULED_TRANSACTIONS("getScheduledTransactions", new GetScheduledTransactions()),
    SCHEDULE_CURRENCY_BUY("scheduleCurrencyBuy", new ScheduleCurrencyBuy()),
    DELETE_SCHEDULED_TRANSACTION("deleteScheduledTransaction",new  DeleteScheduledTransaction());

    private static final Map<String, APIEnum> apiByName = new HashMap<>();

    static {
        final EnumSet<APITag> tagsNotRequiringBlockchain = EnumSet.of(APITag.UTILS);
        for (APIEnum api : values()) {
            if (apiByName.put(api.getName(), api) != null) {
                AssertionError assertionError = new AssertionError("Duplicate API name: " + api.getName());
                assertionError.printStackTrace();
                throw assertionError;
            }

            final APIRequestHandler handler = api.getHandler();
            if (!Collections.disjoint(handler.getAPITags(), tagsNotRequiringBlockchain)
                    && handler.requireBlockchain()) {
                AssertionError assertionError = new AssertionError("API " + api.getName()
                        + " is not supposed to require blockchain");
                assertionError.printStackTrace();
                throw assertionError;
            }
        }
    }

    public static APIEnum fromName(String name) {
        return apiByName.get(name);
    }

    private final String name;
    private final APIRequestHandler handler;

    APIEnum(String name, APIRequestHandler handler) {
        this.name = name;
        this.handler = handler;
    }

    public String getName() {
        return name;
    }

    public APIRequestHandler getHandler() {
        return handler;
    }

    public static EnumSet<APIEnum> base64StringToEnumSet(String apiSetBase64) {
        byte[] decoded = Base64.getDecoder().decode(apiSetBase64);
        BitSet bs = BitSet.valueOf(decoded);
        EnumSet<APIEnum> result = EnumSet.noneOf(APIEnum.class);
        for (int i = bs.nextSetBit(0); i >= 0; i = bs.nextSetBit(i+1)) {
            result.add(APIEnum.values()[i]);
            if (i == Integer.MAX_VALUE) {
                break; // or (i+1) would overflow
            }
        }
        return result;
    }

    public static String enumSetToBase64String(EnumSet<APIEnum> apiSet) {
        BitSet bitSet = new BitSet();
        for (APIEnum api: apiSet) {
            bitSet.set(api.ordinal());
        }
        return Base64.getEncoder().encodeToString(bitSet.toByteArray());
    }
}
