# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Polar projec GIT repository
* Version 0.0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* HYou need JDK 1.8.x and Apache Maven 3.5.x or later
* Configuration: nome
* Dependencies: focrypto from https://bitbucket.org/firstbridge_company/fbcrypto/src
  do "mvn install" in Java folder
* Database configuration: none
* How to run tests: mvn install
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* oleksiy.luikin@firstbridge.io